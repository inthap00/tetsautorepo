package com.im.wrapper.api;

public class ToolAPI {
	public static APIDriver getAPIDriver( String sTestMethodName, String sCountryCode) {
		APIDriver obj = new APIDriver( sTestMethodName, sCountryCode);
		return obj;
	}
	
	public static APIAssertion getAPIAssertion(APIDriver pDriver) {
		return new APIAssertion(pDriver);
	}

}
