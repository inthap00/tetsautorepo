package com.im.wrapper.api;
/**@author Sambodhan D. (Designer and Developer) */

import java.io.File;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.im.api.common.Constants;
import com.im.api.common.Util;
import com.im.b2b.api.business.AppUtil;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;



public class APIDriver{
	protected Logger logger = null;
	SoftAssert sa = new SoftAssert();
	String sTestCode=null;
	private boolean isCompleted=false;

	String sCountryCode=null;

    final String sWebBreak="<BR> ";
	
	final String BACKSLASH = File.separator;
	final String LOCAL_PATH_ADJUST =".." + File.separator; // WRT extent report
	
	
	
	public String getCountryCode() {
		return sCountryCode;
	}

	public APIDriver( String sTestMethodName, String psCountryCode) {
		logger = Logger.getLogger("APIDriver");
		setTestCode(sTestMethodName, psCountryCode);
		sCountryCode=psCountryCode;
	}
	
	public static String prepareTestCode(String testMethodName, String sCountry) {
		return testMethodName+":"+sCountry;
	}
	


	
	public SoftAssert getSoftAssert() {
		return sa;
	}

	
	
	public void resetSoftAssert() {
		sa = new SoftAssert();
	}

	

	public void setTestCode(String testMethodName, String sCountry) {
		sTestCode=APIDriver.prepareTestCode(testMethodName, sCountry);
		
	}


	public String getTestCode() {
		return sTestCode;
	}

	
	/*public String getAShotScreenshot(WebDriver driver, String screenshotName) throws Exception {
		return Util.getAShotScreenshot(driver, screenshotName);
	}
*/

	
	public boolean isTestCaseCompleted() {
		return isCompleted;
	}
	
	public void setTestCaseCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public synchronized void setHTTPRequest(HashMap<String, String> excelHMap, APIDriver objAPIDriver, String sMethodName) {
		RestAssured.baseURI = excelHMap.get(Constants.EXCELHEADEREndPointURL);
		RequestSpecification httpRequest=null;
		httpRequest = RestAssured.given();
		httpRequest.header("Content-Type", "application/json");
		httpRequest.auth().basic(excelHMap.get("UserName"), excelHMap.get("Password"));
		String sCountryCode = excelHMap.get(Constants.ExcelHeaderRunConfig);
		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = ""+Thread.currentThread().getId();
		
		oIContext.setAttribute(excelHMap.get(Constants.EXCELHEADEREndPointURL), httpRequest);
	}


	public synchronized RequestSpecification getHTTPRequest(APIDriver objAPIDriver, HashMap<String, String> excelHMap) {
		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = ""+Thread.currentThread().getId();
		
		String sCountryCode = (String) oIContext.getAttribute(AppUtil.COUNTRY+sThreadID);
		String sMethodName = (String) oIContext.getAttribute(AppUtil.METHOD+sThreadID);
		
		String sTestCode1 = APIDriver.prepareTestCode(sMethodName, sCountryCode);
		
		return (RequestSpecification) oIContext.getAttribute(excelHMap.get(Constants.EXCELHEADEREndPointURL));
	}

}
