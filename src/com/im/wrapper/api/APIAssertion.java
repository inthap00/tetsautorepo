package com.im.wrapper.api;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.asserts.SoftAssert;
import org.testng.Reporter;
import org.testng.SkipException;

import com.aventstack.extentreports.Status;
import com.im.api.common.Util;
import com.im.utility.extentreport.ExtentTestManager;

public class APIAssertion {

	APIDriver hDriver = null;
	final static String ReporterNewLine = "<BR>";
	public final static String PASS = "<font color=\"blue\">PASS</font>: ";
	public final static String FAIL = "<font color=\"red\">FAIL</font>: ";

	Logger logger = Logger.getLogger("HTMLWebElement");
	SoftAssert mysa = null;

	
	public APIAssertion(APIDriver pDriver) {
		hDriver = pDriver;
		mysa = hDriver.getSoftAssert();
	}

	
	public void setHardAssert_TCFailsAndStops(String sMessage, Exception e) throws Exception {
		assertTrue(false, sMessage);
		Assert.fail(sMessage + ReporterNewLine + e.getMessage());

	}
	

	public void setHardAssert_TCFailsAndStops(String sMessage) throws Exception {
		assertTrue(false, sMessage);
		Assert.fail(sMessage);	
	}

	public void assertTrue(boolean bBooleanCondition, String sMessage) throws Exception {
		String sStatus = bBooleanCondition == true ? PASS : FAIL;
		sMessage = sStatus + sMessage;
		mysa.assertTrue(bBooleanCondition, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}
	}

	
	public void setExtentInfo(String sMessage) throws Exception {
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = "" + Thread.currentThread().getId();
		String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
		ExtentTestManager.log(Status.INFO, "INFO:"+sMessage, sCountry);
	}
	
	/** Added by Prashant Navkudkar 
	 * **/
	public void setExtentSkip(String sMessage) throws Exception {
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = "" + Thread.currentThread().getId();
		String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
		ExtentTestManager.log(Status.SKIP, "Description:"+sMessage, sCountry);
		hDriver.setTestCaseCompleted(true);
		mysa.assertAll();
		}	
	
	/** Added by Prashant Navkudkar **/
	public void setExtentWarning(String sMessage) throws Exception {
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = "" + Thread.currentThread().getId();
		String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
		ExtentTestManager.log(Status.WARNING, "Warning:"+sMessage, sCountry);
	}


	
	public void assertFalse(boolean bBooleanCondition, String sMessage) throws Exception {
		assertTrue(!bBooleanCondition, sMessage);
	}

	
	public void setSoftAssert_TCFailsButContinuesExecution(String sMessage) throws Exception {
		System.out.println("Temp");
		mysa.fail(sMessage);
	}

	
	public void assertEquals(boolean bActual, boolean bExpected, String message) {
		String sStatus = bActual == bExpected ? PASS : FAIL;

		String sMessage = sStatus + " Validation for '" + message + "'.";
		mysa.assertEquals(bActual, bExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}
	}

	
	public void  assertEquals(String sActual, String sExpected, String message) {
		String sStatus = (sActual.equals(sExpected)) ? PASS : FAIL;

		String sMessage = sStatus + " Validation for '" + message + "'. Actual["+sActual+"] Expected["+sExpected+"]";
		mysa.assertEquals(sActual, sExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}

	}

	
	public void assertEquals(Float fActual, Float fExpected, String message) throws Exception {
		String sStatus = (fActual.equals(fExpected)) ? PASS : FAIL;

		String sMessage = sStatus + " Validation for '" + message + "'. Actual["+fActual+"] Expected["+fExpected+"]";
		mysa.assertEquals(fActual, fExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}
	}

	
	

	
	public void assertHardEquals(String sActual, String sExpected, String message) {
		String sStatus = (sActual.equals(sExpected)) ? PASS : FAIL;

		//String sMessage = sStatus + " Validation for '" + message + "'.";
		String sMessage = sStatus + " Validation for '" + message + "'. Actual:[" + sActual + "] Expected:[" + sExpected + "]";
		mysa.assertEquals(sActual, sExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
			Assert.fail(sMessage + "Actual:[" + sActual + "] Expected:[" + sExpected + "]");
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}
	}



	public void assertEqualsReportsOnlyOnFailure(String sActual, String sExpected, String message) {
		String sStatus = (sActual.equals(sExpected)) ? PASS : FAIL;

		String sMessage = sStatus + " Validation for '" + message + "'.";
		mysa.assertEquals(sActual, sExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
			Assert.fail(sMessage + "Actual:[" + sActual + "] Expected:[" + sExpected + "]");
		} 
		
	}

	/**********************************************************************************************
	 * Implemented on 9th dec 2019
	 * Hard fail and report thread specific 
	 * @author Sumeet Umalkar
	***************************************************************************************************/


	public void assertHardTrue(boolean bBooleanCondition, String sMessage) {
		String sStatus = bBooleanCondition == true ? PASS : FAIL;
		sMessage = sStatus + sMessage;
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
			Assert.fail(sMessage);
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}	
	}

	public void assertHardEquals(double dActual, double dExpected, String message) {
		String sStatus = (Double.compare(dActual,dExpected)==0) ? PASS : FAIL;

		String sMessage = sStatus + " Validation for '" + message + "'. Actual:[" + dActual + "] Expected:[" + dExpected + "]";
		mysa.assertEquals(dActual, dExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
			Assert.fail(sMessage + "Actual:[" + dActual + "] Expected:[" + dExpected + "]");
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}
	}



	public void assertEquals(double dActual, double dExpected, String message) {
		dActual = Util.convertTo2DecimanFormat(dActual);
		dExpected = Util.convertTo2DecimanFormat(dExpected);
		
		String sStatus = (Double.compare(dActual,dExpected)==0) ? PASS : FAIL;

		String sMessage = sStatus + " Validation for '" + message + "'. Actual["+dActual+"] Expected["+dExpected+"]";
		mysa.assertEquals(dActual, dExpected, sMessage);
		if (sStatus.contains("FAIL")) {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.FAIL, sMessage,sCountry);
		} else {
			ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
			String sThreadID = "" + Thread.currentThread().getId();
			String sCountry = (String) itestContext.getAttribute("Country" + sThreadID);
			ExtentTestManager.log(Status.PASS, sMessage,sCountry);
		}
	}


}// End of class
