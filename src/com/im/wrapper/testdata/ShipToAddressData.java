package com.im.wrapper.testdata;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import com.im.api.common.Constants;
import com.im.utility.ReadExcelFile;

public class ShipToAddressData {
	static Logger logger = Logger.getLogger("ShipToAddressData");
	static HashMap<String, HashMap<String, String>> hmShippingAddrOfCountries = new HashMap<String, HashMap<String, String>>(); 
	private static String psWorkBookName="Select Ship To Address";
	private static String TestDataFile="TestConfigDataForHermes.xlsx";
	final static String COUNTRY="COUNTRY";
	public static final String EXCEL_TestDataMisc=Constants.BASEPATH+"\\TestData\\"+TestDataFile;
	//public static final String EXCEL_TestDataMisc=Constants.BASEPATH+RunConfig.getProperty(Constants.EXCELNAME_MISCORDERDATA).trim();
	
	private static final String EXCEL_COUNTRY="COUNTRY", EXCEL_ATTENTION="attention",
			EXCEL_ADDR_LINE_1="addressline1", EXCEL_COUNTRY_CODE="countrycode",
					EXCEL_CITY="city", EXCEL_STATE="state", EXCEL_POSTCODE="postalcode";

	static {
			ReadExcelFile readExcel = new ReadExcelFile();
			List<HashMap<String, String>> listTestEntry=null;
			try {
				listTestEntry = readExcel.readExcelDataToListofHashMap(EXCEL_TestDataMisc, psWorkBookName);
				
				for(int i=0;i<listTestEntry.size();i++) {
					HashMap<String, String> countryRow=listTestEntry.get(i);
					String sKey = countryRow.get(COUNTRY);
					hmShippingAddrOfCountries.put(sKey, countryRow);					
				}
				
			} catch (Exception e) {
				logger.error("The excel to read the possible countrywise ship tp address is not available.["+e.getMessage()+"]");
				e.printStackTrace();
			}
	}	

	public static HashMap<String, String> getShippingAddressForCountry(String sCountryCode){
		HashMap<String, String> toReturn=null;
		toReturn=hmShippingAddrOfCountries.get(sCountryCode);
		return toReturn;
	}
	
	public static void main(String[] args) {
		String st = getShippingAddressForCountry("US").get("attention");
		System.out.println("rwerewrwe["+st+"]");
	}

}
