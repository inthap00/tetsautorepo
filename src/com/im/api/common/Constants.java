package com.im.api.common;
import java.io.File;

public class Constants {
	public static final String NEWLINE = "\n", IgnoreKeyword = "IGN", TRUE="TRUE", FALSE="FALSE", DEFAULT = "default";
	public static final String Separator = ":";
	public static final String SEPARATOR_BY_COMMA = ",";
	public static final String SEPARATOR_BY_SEMICOLON = ";";
	public static final String BASEPATH = System.getProperty("user.dir");
	public static final String ExtentReportOutputPath = System.getProperty("user.dir")+ File.separator  + "ExtentReports";
	
	// The Test data variables do not delete it
	//	public static final boolean COMMONJSONCREDENTIALENABLED = false;// Make it true if JSON file is part of the input
		//test															// data
	public static final String RUNCONFIGPROP = "runconfig.properties";
	public static final String EXCELNAME_TESTCASECOUNTRY_MAP = "EXCELNAME_TESTCASECOUNTRY_MAP";
	public static final String RUNFORCOUNTRIES = "RUNFORCOUNTRIES";
	public static final String COUNTRYGROUPPROP = "CountryGroup.properties";

	// Excel Headers
	public static final String ExcelHeaderRunConfig = "RunConfig";
	public static final String ExcelHeaderIsApplicable = "IsApplicable";

	public static final String EXCELHEADEREndPointURL = "EndPointURL";
	//public static final String EXCELHEADERSEARCHSTRINGL = "SearchString";
	
	// Parallel Execution section
	// Use for Jenkins configuration
	public static final String READ_FROM_PROPERTIES_FILE = "READ_FROM_PROPERTIES_FILE";	
	
	//Used for API Framework
	public static final String BLANKVALUE = "<<BLANK>>";
	public static final String HTTPREQUEST ="HTTPREQUEST";
	public static final String EXCELHEADER_TESTSCRIPT = "Test Script";
	public static final String EXCELHEADER_TESTDATACATEGORY="TestDataCategory";
	public static final String Environment="Environment";
	public static final String EXCELHEADER_SRNO="SR.NO";
	public static final String ConfigSheetName="ConfigSheetName";
	
	public static final String COUNTRY="country";
	public static final String EMPTY_VALUE_IN_DATA_SHEET="<<EMPTYSTRING>>";
	public static final String NULL_VALUE_IN_DATA_SHEET="<<NULL>>";

	public static final String DataSeperator = "|";
	public static final String NullValue = "NULL";
	
		
	public static String getCurrentProjectPath() {
		return System.getProperty("user.dir");
	}

	public static final String STATUS = "status";
	public static final String STATUS_CODE = "statusCode";
	public static final String ErrorMSG = "errorMessage";
	
	//////////DB
	public static final String DBUSERNAME = "EBSAPL";
	public static final String DBPASSWORD = "MJU76YHN!";
	public static final String JDBCORACLETHIN = "jdbc:oracle:thin";
	public static final String DBDEVIP = "10.22.144.25";
	public static final String PORTNUMBER = "1531";
	public static final String DBQACONNECTIONSTRING = "jdbc:tibcosoftwareinc:oracle//uschlordev4102d.corporate.ingrammicro.com:1531/ODSD01";
	public static final String DBDEVCONNECTIONSTRING = "uschlordev4102d.corporate.ingrammicro.com:1531";
	
	//api parameters
	public static final String KEY_ISIML = "isIml";
	public static final String KEY_INGRAMORDERDATE = "ingramOrderDate";
	public static final String KEY_SIMULATESTATUS = "simulateStatus";

	//api headers
	public static final String KEY_IMCOUNTRYCODE = "IM-CountryCode";
	public static final String KEY_IMCUSTOMERNUM = "IM-CustomerNumber";
	public static final String KEY_IMSENDERID = "IM-SenderID";
	public static final String KEY_IMCORRELATIONID = "IM-CorrelationID";
	
	public static final String REMOVE_KEY_FROM_DATA_SHEET="<<REMOVEKEY>>";
	public static final String FIELD_SEPARATOR_DATA_SHEET = "~";

	public static final String KEY_INGRAMORDERNUM = "ingramOrderNumber";
	public static final String KEY_CUSTLINENUM = "customerLineNumber";
	public static final String KEY_VENDORPARTNUM = "vendorPartNumber";

	public static final String ERRORCODE_FORINVALIDFIELD = "FieldName is not valid";
	public static final String ERRORCODE_FORINVALIDORDER = "Order Not Found";
	public static final String ERRORCODE_FORBLANKFIELD = "FieldName cannot be blank";
	public static final String ERRORCODE_INCORRECTFORMATCUSTNO = "IM-CustomerNumber must be in the format XX-XXXXXX";
	public static final String ERRORCODE_INCORRECTFORMATORDERNO = "OrderNumber must be in the format xx-xxxxx or xx-xxxxx-xx";
	public static final String KEY_FIELDFORVALIDATION = "Validation_Field";	
	public static final String KEY_DATAFORRESPECTIVEFIELDFORVALIDATION = "Validation_Data";
	public static final String KEY_ORDERTYPE = "orderType";
	public static final String KEY_ORDERSTATUS = "orderStatus";
	public static final String KEY_TESTDATACATEGORY = "TestDataCategory";
}
