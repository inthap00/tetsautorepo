package com.im.api.common;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.im.b2b.api.business.DateTimeUtil;


public class Util {
	static String arrCurrencyStorage[]= {"$"};
	static Logger logger = Logger.getLogger("Util");

	public static float convertStringToFloat(String psFloatValue) {
		String sFloatValue = new DecimalFormat("0.00").format(Float.parseFloat(psFloatValue));	//Changes done by Bivas to overcome double issue
		Float fFloatActual = Float.parseFloat(sFloatValue);
		return fFloatActual;
	}

	public static Float convertStrToNumber(String sNumber) {		
		return Float.parseFloat(sNumber);
	}

	public static double convertTo2DecimanFormat(double pdInputValue) {
		DecimalFormat df2 = new DecimalFormat(".##");
		return Double.parseDouble(df2.format(pdInputValue));
	}

	public static int getDotCount(String sNumber) {
		Pattern p = Pattern.compile("[.]");
		Matcher matcher = p.matcher(sNumber);
		int count = 0;
		while(matcher.find()) {
			count++;
		}
		return count;
	}

	public static synchronized <E> boolean compareListValuesWithExpectedValue(List<E> sActual, String expectedvaule) throws Exception {

		boolean bResult=true;	
		if(sActual.size()==0) 
			throw new Exception("Actual List is Empty");

		for(E strActual : sActual) {
			String sActualwithSpace=strActual+"";
			String s=sActualwithSpace.replaceAll("\\s+", "").toUpperCase().replaceAll("[^a-zA-Z0-9]", "");
			if(!(s.contains(expectedvaule.replaceAll("\\s+", "").replaceAll("[^a-zA-Z0-9]", "").toUpperCase()))) {
				bResult=false;
				break;
			}}


		return bResult;	
	}

	public static synchronized <E> boolean compareListOfValuesWithExpectedValue(List<E> lstActual, String expectedvalue) throws Exception {

		boolean bResult=false;
		if(lstActual.size()==0) 
			throw new Exception("Actual List is Empty");

		for(E strActual : lstActual) {
			String sActual = strActual+"";
			if(sActual.equalsIgnoreCase(expectedvalue)) {
				bResult=true;
				break;
			}}

		return bResult;	
	}

	public static synchronized <E> boolean compareListOfValuesWithExpectedValueMoreThanOne(List<E> lstActual, String expectedvalue) throws Exception {

		boolean bResult=false;	
		if(lstActual.size()==0) 
			throw new Exception("Actual List is Empty");
		for(E strActual : lstActual) {
			String sActual = strActual+"";
			if(sActual.toLowerCase().contains(expectedvalue.toLowerCase())) { 
				bResult=true;
				break;
			}}

		return bResult;	
	}

	public static synchronized <E> boolean compareListOfValuesWithExpectedValueMoreThanOneIgnoreCase(List<E> lstActual, String expectedvalue) {

		boolean bResult=false;	
		for(E strActual : lstActual) {
			String sActual = strActual+"";
			if(sActual.toLowerCase().contains(expectedvalue.toLowerCase())) {
				bResult=true;
				break;
			}}

		return bResult;	
	}

	public static synchronized <E> boolean compareListOfValuesWithExpectedValueList(List<E> lstActual, List<E> sExpectedStatusList) throws Exception {

		boolean bResult=true;	
		if(lstActual.size()==0) 
			throw new Exception("Actual List is Empty");
		for(E strActual : lstActual) {
		//	String sActual = strActual+"";
			if(sExpectedStatusList.contains(strActual)) { 
				bResult=false;
				break;
			}}

		return bResult;	
	}

	public static int getRandomNumberInRange(int min, int max) {

		if (min > max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	/*
	 * 
	 * Method name: getRandomNumber
	 * generates an random number with below format:
	 * "SelAuto + date(ddmmsss) +randomnumber(9000)"
	 * 
	 * returns random number as string
	 */

	public static String getRandomNumber() throws Exception {
		String sName = "";
		SimpleDateFormat sdf = new SimpleDateFormat("ddmmSSS");
		Random r = new Random();
		int i = r.nextInt(9000);
		sName =  sdf.format(new Date()) + i;

		return sName;
	}

	public static HashMap<String, String> returnNewCopyOfHashMap(HashMap<String, String> oExistingHMap) {
		if (null==oExistingHMap) return null;
		HashMap<String, String> newHMap = new HashMap<String, String>();
		for (String sKey : oExistingHMap.values()) { 
			newHMap.put(sKey, oExistingHMap.get(sKey));
		}
		return newHMap;
	}

	public static String removeCurrrencySymbol(String text) {
		String sToReturn=null;
		for(int i=0;i<arrCurrencyStorage.length;i++) {
			if(text.contains(arrCurrencyStorage[i])) {
				text=text.replace(arrCurrencyStorage[i], "").trim();
			}
		}
		sToReturn=text;
		return sToReturn;
	}

	public static ArrayList<String> tokenizeString(String sTokenizeString, String sSeparator) {
		ArrayList<String> aList = new ArrayList<String> ();
		StringTokenizer stk = new StringTokenizer(sTokenizeString, sSeparator);
		while(stk.hasMoreTokens()) {
			aList.add(stk.nextToken());
		}
		return aList;
	}

	public static String getCurrentYear() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		return sdf.format(new Date());
	}

	public static String getTodaysDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
		return sdf.format(new Date());
	}	
	
	public static String getTimeStamp() {
		return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

	}	
	

	public static String getTodaysDateISOFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	
	public static String getScreenTagForTestNGReporter(String sFileToStringAsScreenShot, String sMessageName) {
		//String sHeight="34", sWidth="50";	
		//"<a href=\""+filePath+"\"><img src=\""+filePath+"\" alt=\"CapturedScreen\" height=\""+sHeight+"\" width=\""+sWidth+"  border=\"0\"></a><BR>";
		String sLink = "<a href=\"" + sFileToStringAsScreenShot+"\"  target=\"_blank\">"+sMessageName+"</a>";
		// "<a href=\""+sFolderForScreenShot+"/"+getTimeStamp()+Constants.SCREENSHOTEXTN+"\" alt=\"Click here for screenshot\" ></a><BR>";
		return sLink;
	}

	public static String getExceptionDesc(Exception e) {
		int exceptionLength = e.toString().length()>300?300:e.toString().length();
		String strExceptionDesc = e.toString().substring(0, exceptionLength);
		return strExceptionDesc;
	}

	public static String getExceptionDesc(String sExceptionDesc) {
		int iLen=sExceptionDesc.length();
		int exceptionLength = iLen>300?300:iLen;
		String strExceptionDesc = sExceptionDesc.substring(0, exceptionLength);
		return strExceptionDesc;
	}

	public static Float getFormatedPrice(String sNumber) throws Exception {
		float returnValue = 0;
		if(null==sNumber) throw new Exception ("The value received at getPrice is  ["+sNumber+"]");

		String sValue=sNumber.replaceAll("[^0-9.,-]", "");
		// price contains comma as well as dot 
		if(sValue.contains(".") && sValue.contains(",")) {
			if(sValue.lastIndexOf(".")> sValue.lastIndexOf(",")) {
				returnValue = getDotTypeValue(sValue);
			}
			else if (sValue.lastIndexOf(".") < sValue.lastIndexOf(",")) {	
				returnValue = getCommaTypeValue(sValue);
			}
		}else if(sValue.contains(".")) {
			returnValue = getDotTypeValue(sValue);

		}else if(sValue.contains(",")) {
			returnValue = getCommaTypeValue(sValue);
		}else {
			System.out.println("ERROR no comma or Dots in the received");
			throw new Exception ("The value received at getPrice is incorrect ["+sNumber+"]");
		}
		return returnValue;
	}

	private static String convertMultipleDots(String sUpdate) {
		int iLastIndexDot=sUpdate.lastIndexOf(".");
		String sFirstH = sUpdate.substring(0,iLastIndexDot);
		String sSecoH = sUpdate.substring(iLastIndexDot+1);
		sFirstH=sFirstH.replaceAll("[^0-9]", "");
		String sNewStr = sFirstH+"."+sSecoH;		
		return sNewStr;		
	}

	private static float getDotTypeValue(String sNumber) {
		//4343,343,435.90 OR 23.22.33.44
		sNumber = sNumber.replace(",", "");
		if(getDotCount(sNumber)>1)
			sNumber =convertMultipleDots(sNumber);
		return convertStrToNumber(sNumber);	
	}

	private static float getCommaTypeValue(String sNumber) {
		//444.443.33,00
		sNumber = sNumber.replace(".", "");
		sNumber = sNumber.replace(",", ".");
		if(getDotCount(sNumber)>1)
			sNumber =convertMultipleDots(sNumber);
		return convertStrToNumber(sNumber);		
	}

	public synchronized static String count(String source, String sub) {
		int count = 0;

		for (int i = 0; (i = source.indexOf(sub, i)) != -1; i += sub.length()) {
			++count;
		}
		return count+"";
	}
	
	/**
	 * @param lstDataToBeSorted List of data to be sorted
	 * @param sCountryCode Country code
	 * @param isAscendingOrder  If expecting data to be sort in Ascending order then pass 'true' otherwise 'false'
	 * @return This function returns sorted date in given order
	 * @throws Exception
	 */
	public static synchronized List<String> getSortedDate(List<String> lstDataToBeSorted, String sCountryCode, boolean isAscendingOrder) throws Exception {

		List<String> lstActualRawData = lstDataToBeSorted;
		List<String> lstSortedData= new ArrayList<>();	

		List<Date> lstFormattedDates = new ArrayList<Date>();
		DateFormat  dateFormatter = DateTimeUtil.getDefaultCountryDateFormat(sCountryCode);

		// creating list according to country format
		for(String sData: lstActualRawData) {
			lstFormattedDates.add(dateFormatter.parse(sData));
		}		

		// sorting a list
		if(isAscendingOrder)
			Collections.sort(lstFormattedDates);
		else
			Collections.sort(lstFormattedDates, Collections.reverseOrder());		

		// converting into string array list
		for (Date date : lstFormattedDates) {
			String dateStr = dateFormatter.format(date);
			lstSortedData.add(dateStr);
		}

		return lstSortedData;		
	}

	/** 
	 * @param sExponentialNumber
	 * @return This function will returns conversion of exponential number to decimal (string fromat)
	 */
	public synchronized static String geStringofExponentialToNumber(String sExponentialNumber) {
		return new BigDecimal(sExponentialNumber).stripTrailingZeros().toString();
	}

	//Vinita: return sorted list in specified order
	public static synchronized List<String> getSortedListInRequiredOrder(List<String> sActual, String sortOrder) {
		List<String> sExpected=null;
		if(sortOrder.equals("asc"))
			sExpected=sActual.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
		else if(sortOrder.equals("desc"))
			sExpected=sActual.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		return sExpected;
	}

	public static synchronized <E> boolean checkValuesWithinGivenRange(List<E> lstActual, float minValue, float maxValue) {
		boolean bResult = true;
		for(E strActual : lstActual) {
			float sCurrentValue = Float.valueOf(strActual.toString());
			if(!(minValue <= sCurrentValue && sCurrentValue <= maxValue)) {
				bResult = false;
				break;
			}
		}
		return bResult;   
	}

	public static List<? extends Number> getSortedNumberListInRequiredOrder(List<? extends Number> Actual, String sortOrder) {
		List<? extends Number> sExpected=null;
		sExpected=Actual;
		Collections.sort(sExpected, Collections.reverseOrder());
		if(sortOrder.equals("asc"))
			Collections.reverse(sExpected);
		return sExpected;
	}

	public static List<? extends Double> getSortedDoubleListInRequiredOrder(List<Double> lActual, String sSortDirection) {
		List<? extends Double> sExpected=null;
		sExpected=lActual;
		Collections.sort(sExpected, Collections.reverseOrder());
		if(sSortDirection.equals("asc"))
			Collections.reverse(sExpected);
		return sExpected;
	}
	
	/**
	 * @param pCountry: Country code
	 * @returnThis function returns the country specific code used in database
	 * @author prashant navkudkar
	 */
	public synchronized static String getCountryCodeForDatabase(String pCountry) {
		
		String sCountryCode = null;
		
		switch(pCountry.toUpperCase()) {
		
		case "US": sCountryCode = "MD"; break;
		
		case "FR": sCountryCode = "FR"; break;
		
		case "DE": sCountryCode = "DE"; break;
		
		case "AT": sCountryCode = "AT"; break;
		
		case "CA": sCountryCode = "FT"; break;
		
		default:
			System.out.println("Please enter correct choice");		
		}
		
		return sCountryCode;
		
	}

	public static String getSalesOrganizationForDatabase(String pCountry) {
		
		String salesOrgID = null;
		
		switch(pCountry.toUpperCase()) {
		
		case "US": salesOrgID = "9500"; break;
		
		case "CA": salesOrgID = "8030"; break;
		
		default:
			System.out.println("Please enter correct choice");		
		}
		
		return salesOrgID;
		
	}
}