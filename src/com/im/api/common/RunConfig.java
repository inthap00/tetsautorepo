package com.im.api.common;
/**Design and Developer: Sambodhan D.**/

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class RunConfig {
	private static Properties properties = new Properties();
	Logger logger = Logger.getLogger("RunConfig");
	private static boolean bPropertiesLoaded=false;

	synchronized static void loadProperties() {
		if(!bPropertiesLoaded) {
			try {
				String FilePath= Constants.RUNCONFIGPROP;
				properties.load(RunConfig.class.getResourceAsStream("/"+FilePath));
				bPropertiesLoaded=true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	static {
		loadProperties();
	}
	public static synchronized String getProperty(String psPropertyName) {
		return properties.getProperty(psPropertyName);
	}
	
	public static synchronized boolean isEnabled(String psPropertyName) {
		String sAns=properties.getProperty(psPropertyName);
		
		return (null!=sAns&&sAns.contains(Constants.TRUE));
	}
	

}
