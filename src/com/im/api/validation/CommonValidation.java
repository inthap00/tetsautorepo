package com.im.api.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.aventstack.extentreports.Status;
import com.im.api.common.Constants;
import com.im.api.common.Util;
import com.im.b2b.api.business.AppUtil;
import com.im.b2b.api.business.JsonUtil;
import com.im.utility.extentreport.ExtentTestManager;
import com.im.wrapper.api.APIAssertion;

public class CommonValidation {
	APIAssertion objAPIAssertion=null;
	Logger logger = Logger.getLogger("CommonValidation"); 
	public CommonValidation(APIAssertion pAPIAssertion) {
		objAPIAssertion = pAPIAssertion;
	}

	public synchronized void validateMultiSetDetailsForREST(List<HashMap<String,String>> multisetDataDrivenMap, List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse,
			String[] sKeysToBeVerify) throws Exception {

		try {

			for(int i=0; i<multisetDataDrivenMap.size(); i++)
			{
				if(lstOfDataToBeVerifiedFromResponse.get(i)==null)
				{
					String sActual = String.valueOf(multisetDataDrivenMap.get(i).get(sKeysToBeVerify[0])).toUpperCase();
					objAPIAssertion.assertHardEquals(Constants.NULL_VALUE_IN_DATA_SHEET, sActual, "Status response expected: [No Values],  Actual: [No Values]");
				}
				else {
					for(int j=0; j<sKeysToBeVerify.length; j++) {
						String sActual = String.valueOf(lstOfDataToBeVerifiedFromResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();
						String sExpected = String.valueOf(multisetDataDrivenMap.get(i).get(sKeysToBeVerify[j])).toUpperCase();		
						if(sExpected.equalsIgnoreCase(Constants.EMPTY_VALUE_IN_DATA_SHEET))
							sExpected = "";
						objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
					}
				}
			}

		}
		catch(Exception ex) {
			String sErrMessage="FAIL: validateMultiSetDetailsForREST() method: "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
	}


	/** This is the function where we  manage single to multiple set responses
	 * 
	 * @param expectedMultisetDataDrivenMap expected data map from test data sheet
	 * @param lstOfDataToBeVerifiedFromResponse data map from the response
	 * @param sKeysToBeVerify Name of the parameters to verify
	 * @param sKeyForReference Name of column from test data excel against which user wish to see the details into the report 
	 * @throws Exception if data is not configured correctly in test data sheet.
	 * @author prashant navkudkar
	 */
	public synchronized  void validateSingleToMultipleSetRequest(HashMap<String,String> expectedMultisetDataDrivenMap, List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse,
			String[] sKeysToBeVerify, String sKeyForReference) throws Exception {


		List<String> listOfExpectedDataKeys = Arrays.asList(sKeysToBeVerify);

		List<HashMap<String,String>> reqDataForSingleToMultResponse = new ArrayList<HashMap<String, String>>();
		System.out.println("  ");
		try {
			// Fetch data from Excel and create new data set as per require validation column
			for(int j=0; j<lstOfDataToBeVerifiedFromResponse.size(); j++) {
				HashMap<String,String> mapOfTestData = new HashMap<>();
				for(String sKeydata: listOfExpectedDataKeys) {						
					String sValue[] =StringUtils.split(expectedMultisetDataDrivenMap.get(sKeydata),Constants.DataSeperator);
					mapOfTestData.put(sKeydata, sValue[j]);
				}
				reqDataForSingleToMultResponse.add(mapOfTestData);
			}

			// Validate with actual response
			
			objAPIAssertion.setExtentInfo("=+=+=+=+=+=+=+==+=+=+=+= Executing for "+ sKeyForReference +":"+expectedMultisetDataDrivenMap.get(sKeyForReference)+
					" =+=+=+=+=+=+=+=+=+=+=+=");	
			for(int i=0; i<reqDataForSingleToMultResponse.size(); i++)
			{
				if(reqDataForSingleToMultResponse.get(i)==null)
				{
					String sActual = String.valueOf(reqDataForSingleToMultResponse.get(i).get(sKeysToBeVerify[0])).toUpperCase();
					objAPIAssertion.assertHardEquals(Constants.NULL_VALUE_IN_DATA_SHEET, sActual, "Status response expected: [No Values],  Actual: [No Values]");
				}
				else {

					for(int j=0; j<sKeysToBeVerify.length; j++) {
						String sActual = String.valueOf(lstOfDataToBeVerifiedFromResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();
						String sExpected = String.valueOf(reqDataForSingleToMultResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();		
						if(sExpected.equalsIgnoreCase(Constants.EMPTY_VALUE_IN_DATA_SHEET))
							sExpected = "";	
						objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
					}
				}
			}	

		}
		catch(ArrayIndexOutOfBoundsException | NullPointerException ex) {
			String sErrMessage="FAIL: validateSingleToMultipleSetRequest() method: Test data maintained in the sheet has been"
					+ " misconfigured. "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}

		catch(Exception ex) {
			String sErrMessage="FAIL: validateSingleToMultipleSetRequest() method: "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
	}

	public synchronized void validateMultiSetDetails(HashMap<String, String> hSingleSetData, List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse, String[] sToBeVerify, String sTestDataFilenm) throws Exception {
		try {
			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hSingleSetData, sTestDataFilenm);

			for(int i=0; i<multisetDataDrivenMap.size(); i++)
			{
				String sCountry = multisetDataDrivenMap.get(i).get(Constants.ExcelHeaderRunConfig);
				ExtentTestManager.log(Status.INFO, "Running for Multi set Test Data SR.NO ["+multisetDataDrivenMap.get(i).get(Constants.EXCELHEADER_SRNO) +"]", sCountry);
				for(int j=0; j<sToBeVerify.length; j++) {
					String sActual = lstOfDataToBeVerifiedFromResponse.get(i).get(sToBeVerify[j]).toUpperCase();
					String sExpected = multisetDataDrivenMap.get(i).get(sToBeVerify[j]).toUpperCase();
					if(sExpected.equalsIgnoreCase(Constants.EMPTY_VALUE_IN_DATA_SHEET))
						sExpected = "";	
					objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sToBeVerify[j].toUpperCase());
				}
			}

		}	catch(Exception ex) {
			String sErrMessage="FAIL: validateMultiSetDetails() method: "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
	}
	
	
	/**
	 * This function check for valid response received from end point, test case execution will end if invalid response receive
	 * @param pObjJSONUtil JsonUtil object
	 * @param expectedStatusString expected status from response
	 * @throws Exception throws exception if any unexpected condition
	 * @author prashant navkudkar
	 */
	public void verifyEndptResponseStatus(JsonUtil pObjJSONUtil, String expectedStatusString) throws Exception {
		try 
		{
			String sEndptStatus = pObjJSONUtil.getActualValueFromJSONResponseWithoutModify(Constants.STATUS);
			String sEndStatusCode = pObjJSONUtil.getActualValueFromJSONResponseWithoutModify(Constants.STATUS_CODE);
			String sEndptErrorCode = pObjJSONUtil.getActualValueFromJSONResponseWithoutModify(Constants.ErrorMSG);

			if (!(sEndStatusCode.equalsIgnoreCase(expectedStatusString)))
				objAPIAssertion.setHardAssert_TCFailsAndStops("Invalid response: Status Code is: ["+ sEndStatusCode +"]. Response Status: ["+ sEndptStatus +"]."
						+ " Error Message Displayed as: ["+ sEndptErrorCode +"]");

		}catch(Exception e)	{
			String sErrMessage="FAIL: verifyEndptResponseStatus method of CommonValidations: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	protected void performResponseCodeAndStatusCodeValidation(JsonUtil objJSONUtil, String APITemplateName) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Response Code Validation for "+APITemplateName+" API Response");

			String sActualStatusCode=objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode");
			objAPIAssertion.assertHardEquals(sActualStatusCode, "200", "Status field Validation for "+APITemplateName+" serviceresponse.");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
}
