package com.im.api.validation;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.im.api.common.Constants;
import com.im.b2b.api.business.JsonUtil;
import com.im.wrapper.api.APIAssertion;
import io.restassured.response.Response;

public class OrderDetailsAPIValidation {
	APIAssertion objAPIAssertion = null;
	Logger logger = Logger.getLogger("OrderDetailsAPIValidation");

	public OrderDetailsAPIValidation(APIAssertion pAPIAssertion) {
		objAPIAssertion = pAPIAssertion;
	}

	/**
	 * Function to validate data from two API responses(Order Create and Order
	 * Details)
	 * 
	 * @param response1        - API response of first service
	 * @param xpaths_response1 array of field xpaths for respective response
	 * @param response2        - API response of second service
	 * @param xpaths_response2 array of field xpaths for respective response
	 * 
	 * @author Priyesh Thakur
	 */
	public synchronized void validateAPI_APIData(Response response1, String[] xpaths_response1, Response response2,
			String[] xpaths_response2) throws Exception {

		JsonUtil jsonUtil = new JsonUtil();

		if (xpaths_response1.length == xpaths_response2.length) {
			for (int i = 0; i <= xpaths_response1.length - 1; i++) {
				String response1Value = jsonUtil.getValueFromJSONResponse(response1, xpaths_response1[i]);
				// Some logics to be added
				if (xpaths_response1[i].contains(Constants.KEY_CUSTLINENUM)) {
					response1Value = String.format("%03d",
							Integer.parseInt(jsonUtil.getValueFromJSONResponse(xpaths_response1[i])));
				}
				String response2Value = jsonUtil.getValueFromJSONResponse(response2, xpaths_response2[i]);

				// Some logics to be added
				if (xpaths_response1[i].contains(Constants.KEY_INGRAMORDERDATE)
						|| xpaths_response1[i].contains(Constants.KEY_VENDORPARTNUM)) {
					objAPIAssertion.assertTrue(response2Value.contains(response1Value),
							"Values are not matching for xpaths" + xpaths_response1[i] + " " + xpaths_response2[i]);
				} else {
					objAPIAssertion.assertEquals(response1Value, response2Value,
							"Values are not matching for xpaths" + xpaths_response1[i] + " " + xpaths_response2[i]);
				}
			}

		} else {
			objAPIAssertion.setHardAssert_TCFailsAndStops("Count for Validation fields is not matching");

		}
	}

	/**
	 * Function to validate data from API response with DB
	 * 
	 * @param response1              - API response of first service
	 * @param xpaths_response1       array of field xpaths for respective response
	 * @param orderDetailsDataFromDB - hashmap of order details column/field name
	 *                               from db and it's value
	 * @param columnNames_DB         array of field xpaths for respective response
	 * 
	 * @author Priyesh Thakur
	 */
	public synchronized void validateData(Response apiResponse, String[] xpaths_response,
			HashMap<String, String> orderDetailsDataFromDB, String[] columnNames_DB) throws Exception {

		JsonUtil jsonUtil = new JsonUtil();

		if (xpaths_response.length == columnNames_DB.length) {
			for (int i = 0; i <= xpaths_response.length - 1; i++) {
				String apiResponseValue = jsonUtil.getValueFromJSONResponse(apiResponse, xpaths_response[i]);
				String dbValue = orderDetailsDataFromDB.get(columnNames_DB[i]);

				objAPIAssertion.assertEquals(apiResponseValue, dbValue,
						"Values are not matching for xpath" + xpaths_response[i] + " and DB column " + columnNames_DB[i]);
			}
		} else {
			objAPIAssertion.setHardAssert_TCFailsAndStops("Count for Validation fields is not matching");

		}
	}
}
