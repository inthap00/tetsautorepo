package com.im.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.im.api.common.Constants;
import com.im.api.common.Util;
import com.im.b2b.api.business.AppUtil;
import com.im.b2b.api.business.MapTCForNations;
import com.im.b2b.api.control.CountryGroupBuilder;

public class ReadExcelFile {
	InputStream inpStream = null;
	int iColumnCountOfTwoDimArray = 1;
	String fileName = null, sSheetName = null;
	private static final Logger logger = Logger.getLogger(ReadExcelFile.class.getName());
	// private static CountryCredentialBucket objCountryCredentialBucket=null;

	private Sheet getSheetName() throws Exception {
		Sheet sheet = null;
		try {
			Workbook workbook = WorkbookFactory.create(new File(fileName));
			sheet = workbook.getSheet(sSheetName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return sheet;
	}

	public List<HashMap<String, String>> readExcelDataToListofHashMap(String pFileName, String psSheetName)
			throws Exception {

		List<HashMap<String, String>> testDataList = null;
		fileName = pFileName;
		sSheetName = psSheetName;
		Sheet sheet = getSheetName();
		testDataList = storeValuesInHashMap(sheet);
		logger.debug(testDataList);
		// logger.info("Total row count for file["+fileName+"] sheet["+sSheetName+"] is
		// '"+testDataList.size()+"'");
		return testDataList;
	}

	public synchronized Object[][] readExcelDataTo2DimArrayWithJasonObject(String fileName, String sSheetName)
			throws Exception {
		Object[][] arrayTwodimObject = null;
		arrayTwodimObject = convertListToObjectArray(readExcelDataToListofHashMap(fileName, sSheetName));
		return arrayTwodimObject;
	}

	/*
	 * Created By : Paresh Save. sample test case for order staus API just read the
	 * test data file and store in 2 dimensional array
	 */
	public synchronized Object[][] readExcelDataTo2DimArrayWithJasonObjectDefault(String fileName, String sSheetName)
			throws Exception {
		Object[][] arrayTwodimObject = null;
		arrayTwodimObject = convertListToObjectArrayDefault(readExcelDataToListofHashMap(fileName, sSheetName));
		return arrayTwodimObject;
	}

	/**
	 * This is generic function to create test data at runtime
	 * 
	 * @author Prashant Navkudkar
	 */
	public <T> Object[][] readExcelDataTo2DimArrayWithTestDataUtilObject(String pTestDataFileName,
			String pSheetNameInApplicability, HashMap<String, MapTCForNations> plistTCMappingToCountry,
			Class<T> pNameOfTestDataUtilClass) throws Exception {

		Object[][] arrayTwodimObject = null;
		arrayTwodimObject = convertListToObjectArrayForTestDataUtilData(
				readExcelDataToListofHashMap(pTestDataFileName, pSheetNameInApplicability), plistTCMappingToCountry,
				pNameOfTestDataUtilClass);
		return arrayTwodimObject;
	}

	private Object[][] convertListToObjectArray(List<HashMap<String, String>> listTestDataList) {
		if (null == listTestDataList || listTestDataList.size() == 0) {
			logger.fatal("Received null value to the convertListToObjectArray");
			return null;
		}
		// Filter out the countries which are not given in the RUNTIMECOUNTRY
		// runconfig.properties
		iColumnCountOfTwoDimArray++;
		CountryGroupBuilder cgb = new CountryGroupBuilder();
		List<HashMap<String, String>> testDataList = cgb.processRunCountryEnvVariable(listTestDataList);

		// Added By Sumeet to remove the IsApplicable countries which are set to N.
		testDataList = processDataForIsApplicableList(testDataList);

		Object[][] arrayTwodimObject = new Object[testDataList.size()][iColumnCountOfTwoDimArray];
		if (null == testDataList || testDataList.size() == 0) {
			logger.fatal("ERROR: The given test data list (from excel) is null!");
			return null;
		}
		int i = 0;
		// Store the HashMaps into the Object array
		for (HashMap<String, String> hmInstance : testDataList) {
			arrayTwodimObject[i][0] = hmInstance;
			i++;
		}

		return arrayTwodimObject;
	}

	/*
	 * Created By : Paresh Save. sample test case for order staus API just read the
	 * test data file and store in 2 dimensional array
	 */
	private Object[][] convertListToObjectArrayDefault(List<HashMap<String, String>> listTestDataList) {
		if (null == listTestDataList || listTestDataList.size() == 0) {
			logger.fatal("Received null value to the convertListToObjectArray");
			return null;
		}
		// Filter out the countries which are not given in the RUNTIMECOUNTRY
		// runconfig.properties
		iColumnCountOfTwoDimArray++;
		CountryGroupBuilder cgb = new CountryGroupBuilder();
		List<HashMap<String, String>> testDataList = cgb.processRunCountryEnvVariable(listTestDataList);

		Object[][] arrayTwodimObject = new Object[testDataList.size()][iColumnCountOfTwoDimArray];
		if (null == testDataList || testDataList.size() == 0) {
			logger.fatal("ERROR: The given test data list (from excel) is null!");
			return null;
		}
		int i = 0;
		// Store the HashMaps into the Object array
		for (HashMap<String, String> hmInstance : testDataList) {
			arrayTwodimObject[i][0] = hmInstance;
			i++;
		}

		return arrayTwodimObject;
	}

	/**
	 * This function Store the data for testdata hashmap and also TestDataUtil
	 * object
	 * 
	 * @author Prashant Navkudkar
	 */
	private <T> Object[][] convertListToObjectArrayForTestDataUtilData(List<HashMap<String, String>> listTestDataList,
			HashMap<String, MapTCForNations> pListTCMappingToCountry, Class<T> pNameOfTestDataUtilClass)
			throws Exception {

		if (null == listTestDataList || listTestDataList.size() == 0) {
			logger.fatal("Received null value to the convertListToObjectArrayForElasticSearchData method");
			return null;
		}

		// Filter out the countries which are not given in the RUNTIMECOUNTRY
		// runconfig.properties
		iColumnCountOfTwoDimArray++;
		CountryGroupBuilder cgb = new CountryGroupBuilder();
		List<HashMap<String, String>> testDataList = cgb
				.processRunCountryEnvVariableWithApplicabilityTrueCountries(listTestDataList, pListTCMappingToCountry);

		Object[][] arrayTwodimObject = new Object[testDataList.size()][iColumnCountOfTwoDimArray];
		if (null == testDataList || testDataList.size() == 0) {
			logger.fatal("ERROR: The given test data list (from excel) is null!");
			return null;
		}

		/**
		 * Here will add implementation to store TestDataUtil object for each applicable
		 * country
		 */
		// if(!RunConfig.getProperty("ExecuteWithOfflineData").equalsIgnoreCase("Yes"))
		// { Sumeet: This decides whether to pull data dynamically or not
		System.out.println("+++++++++++++++++ Data generation started at [" + new Timestamp(new Date().getTime())
				+ "] +++++++++++++++++");
		storeDataObjectRetrivedFromWS(testDataList, arrayTwodimObject, pNameOfTestDataUtilClass);
		System.out.println("+++++++++++++++++ Data generation Completed at [" + new Timestamp(new Date().getTime())
				+ "] +++++++++++++++++");
		// }
		int i = 0;
		// Store the HashMaps into the Object array
		for (HashMap<String, String> hmInstance : testDataList) {
			arrayTwodimObject[i][0] = hmInstance;
			i++;
		}

		return arrayTwodimObject;
	}

	/**
	 * Store test data object in two dimensional array object
	 * 
	 * @author Prashant Navkudkar
	 */
	private <T> void storeDataObjectRetrivedFromWS(List<HashMap<String, String>> testDataList,
			Object[][] arrayTwodimObject, Class<T> pNameOfTestDataUtilClass) {
		try {

			int iRow = 0;
			for (HashMap<String, String> hmInstance : testDataList) {
				try {
					System.out.println("============== Capturing data for "
							+ hmInstance.get(Constants.ExcelHeaderRunConfig) + " ==============");
					arrayTwodimObject[iRow][1] = pNameOfTestDataUtilClass.getDeclaredConstructor(HashMap.class)
							.newInstance(hmInstance);
				} catch (Exception e) {
					logger.error("Data util related file issue : " + e.getMessage() + "]");
				}
				iRow++;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/**
	 * Store test data object to Hashmap and Json file
	 * 
	 * @author Sumeet Umalkar
	 * @param testDataList:            store initial Excel test data
	 * @param pNameOfTestDataUtilClass : Name of the TestDataUtil class which data
	 *                                 needs to be fetch
	 */
	private <T> void storeDataObjectRetrivedFromWS_Dynamic(List<HashMap<String, String>> testDataList,
			Object[][] arrayTwodimObject, Class<T> pNameOfTestDataUtilClass) {
		try {
			HashMap<String, Object> objHM = new HashMap<>();
			for (HashMap<String, String> hmInstance : testDataList) {
				try {
					System.out.println("============== Capturing data for "
							+ hmInstance.get(Constants.ExcelHeaderRunConfig) + " ==============");
					// arrayTwodimObject[iRow][1]=
					// pNameOfTestDataUtilClass.getDeclaredConstructor(HashMap.class).newInstance(hmInstance);
					objHM.put(hmInstance.get(Constants.ExcelHeaderRunConfig),
							pNameOfTestDataUtilClass.getDeclaredConstructor(HashMap.class).newInstance(hmInstance));
				} catch (Exception e) {
					logger.error("Data util related file issue : " + e.getMessage() + "]");
				}
			}

			String sFileNameWithLocation = Constants.BASEPATH + "\\DynamicTestData\\"
					+ pNameOfTestDataUtilClass.getSimpleName() + ".json";
			writeDataToFile(sFileNameWithLocation, objHM, pNameOfTestDataUtilClass);

		} catch (Exception e) {
			logger.error("Error in function storeDataObjectRetrivedFromWS_Dynamic of ReadExcelFile.java");
			e.printStackTrace();
		}

	}

	/***
	 * Stores the test data to json file with Key as Country and value as
	 * TestDataUtil Object, Override existing data if not present creates a new one
	 * 
	 * @author Sumeet Umalkar
	 * @param sFileNameWithLocation              : Testdata Json file location
	 * @param HM<CountryCode,TestDataUtilObject>
	 */
	private <T> void writeDataToFile(String sFileNameWithLocation, HashMap<String, Object> objHM,
			Class<T> pNameOfTestDataUtilClass) {
		FileWriter writer;
		Gson gson;
		try {
			HashMap<String, Object> hmExistingData = AppUtil.getEntireTestdataForFile(sFileNameWithLocation,
					pNameOfTestDataUtilClass);

			hmExistingData.putAll(objHM);

			writer = new FileWriter(sFileNameWithLocation);
			gson = new GsonBuilder().setPrettyPrinting().create();
			gson.toJson(hmExistingData, writer);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			logger.error("Error in function writeDataToFile of ReadExcelFile.java");
			e.printStackTrace();
		}

	}

	private List<HashMap<String, String>> processDataForIsApplicableList(List<HashMap<String, String>> testDataList) {
		List<HashMap<String, String>> listFilteredExcelRow = new ArrayList<HashMap<String, String>>();

		for (HashMap<String, String> hmExcelRow : testDataList) {
			String sIsApplicable = hmExcelRow.get(Constants.ExcelHeaderIsApplicable).trim();
			if (sIsApplicable.equalsIgnoreCase("Y")) {
				listFilteredExcelRow.add(hmExcelRow);
			}
		}

		if (listFilteredExcelRow.isEmpty())
			System.out.println(
					"In Test Data Is Applicable is marked as N for all the test data, Make sure atleast one Data should be marked as Y");

		return listFilteredExcelRow;
	}

	// This function is not in use now since the '[AdminCred:SelAutoDN1~SelAutoDN1]'
	// format is replaced by CountryCredentialBucket now.
	/*
	 * private void storeMultiCredential(List<HashMap<String, String>> testDataList,
	 * Object[][] arrayTwodimObject, int iMultiCredenialColumnNum) { int i=0;
	 * //Store the HashMaps into the Object array for(HashMap<String, String>
	 * hmInstance : testDataList ) { String
	 * sMultiCredentialPattern=hmInstance.get(Constants.EXCELHEADERCREDENTIAL);
	 * CredentialBucket oCB=null; try { oCB =
	 * CredentialBucket.decodeTheCredentialString(sMultiCredentialPattern);
	 * logger.debug("The Credential of encoded values\""+oCB+"\""); } catch
	 * (Exception e) { logger.error("The value of "+Constants.
	 * EXCELHEADERCREDENTIAL+" is null in test data's one of the row!"); }
	 * arrayTwodimObject[i][iMultiCredenialColumnNum]=oCB; i++; }
	 * 
	 * }
	 */

	/*
	 * private void
	 * storeJsonObjectFromFileForEachCountrysCredential(List<HashMap<String,
	 * String>> testDataList,Object [][] arrayTwodimObject, int
	 * piColumnNameForJason) { // if(null==objCountryCredentialBucket) {
	 * synchronized (this) { String sSheetName = null; sSheetName =
	 * System.getProperty("Environment").trim().toUpperCase(); String
	 * sFileToReadForCredential=Constants.BASEPATH+RunConfig.getProperty(
	 * "SmokeTestsCredential");
	 * 
	 * if (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) {
	 * sFileToReadForCredential = Constants.BASEPATH+ File.separator +"TestData" +
	 * File.separator + "Credential"+sSheetName+".json"; }
	 * 
	 * System.out.println("********* Credential file path["+sFileToReadForCredential
	 * +"]**********"); //String sCredentialsFileName=(!sSheetName.equalsIgnoreCase(
	 * "READ_FROM_PROPERTIES_FILE"))?"":Util.getProperty("SmokeWorksheet");
	 * 
	 * // String sFileToReadForCredential =
	 * Constants.BASEPATH+Util.getProperty("SmokeTestsCredential"); //
	 * objCountryCredentialBucket =
	 * CountryCredentialBucket.gSonReader(sFileToReadForCredential); } }
	 * CredentialBucket pCB = null; int iRow=0; //Store the HashMaps into the Object
	 * array for(HashMap<String, String> hmInstance : testDataList ) { try { String
	 * sCountryName=hmInstance.get(Constants.ExcelHeaderCountryExcelHeader); // pCB
	 * = objCountryCredentialBucket.getCredentials(sCountryName);
	 * arrayTwodimObject[iRow][piColumnNameForJason]=pCB; } catch (Exception e) {
	 * logger.error("Jackson or file related issue : "+e.getMessage()+"]"); }
	 * iRow++; } }
	 */

	private ArrayList<HashMap<String, String>> storeValuesInHashMap(Sheet sheet) {
		ArrayList<HashMap<String, String>> testDataList = new ArrayList<HashMap<String, String>>();
		int iRowCount = 0;

		// every sheet has rows, iterate over them
		Iterator<Row> rowIterator = sheet.iterator();
		ArrayList<String> headerColumns = null;
		while (rowIterator.hasNext()) {
			// Get the row object
			Row row = rowIterator.next();

			// Every row has columns, get the column iterator and iterate over them
			Iterator<Cell> cellIterator = row.cellIterator();

			if (iRowCount == 0)
				headerColumns = readTheStringsFromTheRowIterator(cellIterator);
			else {
				HashMap<String, String> rowMap = new HashMap<String, String>();

				ArrayList<String> rowData = readTheStringsFromTheRowIteratorNew(cellIterator);
				for (int i = 0; i < headerColumns.size(); i++) {
					String sHeaderName = headerColumns.get(i);
					String sHeaderValue = rowData.size() > i ? rowData.get(i) : null;
					rowMap.put(sHeaderName, sHeaderValue);
				}
				testDataList.add(rowMap);
			}
			iRowCount++;
		} // end of rows iterator

		return testDataList;

	}

	private ArrayList<String> readTheStringsFromTheRowIterator(Iterator<Cell> cellIterator) {

		ArrayList<String> rowData = new ArrayList<String>();

		if (null == cellIterator)
			return null;
		else {
			while (cellIterator.hasNext()) {
				// Get the Cell object
				Cell cell = cellIterator.next();

				// check the cell type and process accordingly
				switch (cell.getCellType()) {
				case STRING:
				case NUMERIC:
					DataFormatter dataFormatter = new DataFormatter();
					String sCellVal = dataFormatter.formatCellValue(cell);
					rowData.add(sCellVal);
					break;
				default:
					logger.error("Could not find the data!");
				}
			}
			return rowData;
		}
	}

	private ArrayList<String> readTheStringsFromTheRowIteratorNew(Iterator<Cell> cellIterator) {
		int previousCell = -1;
		int currentCell = 0;
		ArrayList<String> rowData = new ArrayList<String>();

		if (null == cellIterator)
			return null;
		else {

			while (cellIterator.hasNext()) {
				// Get the Cell object
				Cell cell = cellIterator.next();
				currentCell = cell.getColumnIndex();
				if (previousCell != currentCell - 1) {
					int iP = previousCell;
					int iC = currentCell;
					for (int k = iP + 1; k < iC; k++) {
						rowData.add(null);
					}
				}
				previousCell = currentCell;

				// check the cell type and process accordingly
				switch (cell.getCellType()) {
				case BLANK:
					rowData.add(null);
					break;
				case STRING:
				case NUMERIC:
					DataFormatter dataFormatter = new DataFormatter();
					String sCellVal = dataFormatter.formatCellValue(cell);
					rowData.add(sCellVal);
					break;
				default:
					System.out.println("Given cell type is not supported:" + cell.getCellType());

				}
			}
			return rowData;
		}
	}

	public static synchronized void createWorkbook(String sfilePath) throws Exception {
		try {
			FileOutputStream fos = new FileOutputStream(new File(sfilePath));
			XSSFWorkbook workbook = new XSSFWorkbook();

			@SuppressWarnings("unused")
			XSSFSheet sheet = workbook.createSheet("result");

			workbook.write(fos);
			fos.flush();
			fos.close();
			System.out.println("Workbook created successfully");
		} catch (Exception e) {
			System.out.println("##############################################################################");
			System.out.println("Unable to Create [" + sfilePath + "] : " + Util.getExceptionDesc(e));
			throw new Exception();
		}
	}

	public static void main(String args[]) throws Exception {

		String path = "C:\\Users\\usumas00\\eclipse-workspace\\IngramAutoAPITest\\ExtentReports\\APIResults\\Test.xlsx";

		createWorkbook(path);
		List<Object[]> empinfo = Collections.synchronizedList(new ArrayList<Object[]>());
		List<Object[]> empinfo1 = Collections.synchronizedList(new ArrayList<Object[]>());
		empinfo.add(new Object[] { "EMPID", "NAME", "DESIGNATION", "" });
		empinfo.add(new Object[] { "tp01", "Gopal", "Technical Manager" });
		empinfo.add(new Object[] { "tp02", "Manisha", "Proof Reader" });

		/*
		 * Map < String, Object[] > empinfo = new TreeMap < String, Object[] >();
		 * empinfo.put( "abc", new Object[] { "EMPID", "NAME", "DESIGNATION" });
		 * empinfo.put( "abc", new Object[] { "tp01", "Gopal", "Technical Manager" });
		 * empinfo.put( "abc", new Object[] { "tp02", "Manisha", "Proof Reader" });
		 * empinfo.put( "abc", new Object[] { "tp03", "Masthan", "Technical Writer" });
		 * empinfo.put( "abc", new Object[] { "tp04", "Satish", "Technical Writer" });
		 * empinfo.put( "abc", new Object[] { "tp05", "Krishna", "Technical Writer" });
		 * 
		 * Map <String,String> hmResults = new HashMap<String, String>();
		 */

		writeResult(path, empinfo);
		System.out.println("First done");
		empinfo1.add(new Object[] { "EMPID", "NAME", "DESIGNATION", "Second" });
		writeResult(path, empinfo1);
		System.out.println("Second done");

	}

	public static synchronized void writeResult(String sFilePath, List<Object[]> lstResultLog) throws IOException {
		String path = sFilePath;

		try {
			FileInputStream myxls = new FileInputStream(sFilePath);
			XSSFWorkbook workbook = new XSSFWorkbook(myxls);

			// get the first sheet
			XSSFSheet spreadsheet = workbook.getSheetAt(0);

			// Create row object
			XSSFRow row;

			// This data needs to be written (Object[])
			int rowid = spreadsheet.getLastRowNum();
			/*
			 * if(rowid != 0) rowid++;
			 */
			for (Object[] objArr : lstResultLog) {
				row = spreadsheet.createRow(++rowid);
				int cellid = 0;
				for (Object obj : objArr) {
					Cell cell = row.createCell(cellid++);
					cell.setCellValue((String) obj);
				}

			}

			// Write the workbook in file system
			// FileOutputStream out = new FileOutputStream(new File(path));
			FileOutputStream out = new FileOutputStream(new File(path));
			workbook.write(out);
			out.close();
		} catch (Exception e) {
			System.out.println(
					"File[" + sFilePath + "] may be open hence unable to write to file : " + Util.getExceptionDesc(e));
		}

	}

	public static void append(String sPath, Map<String, Object[]> empinfo) throws IOException {
		String path = sPath;

		FileInputStream myxls = new FileInputStream(sPath);

		XSSFWorkbook workbook = new XSSFWorkbook(myxls);

		// Picks the first sheet
		XSSFSheet spreadsheet = workbook.getSheetAt(0);

		// Create row object
		XSSFRow row;

		// This data needs to be written (Object[])

		// Iterate over data and write to sheet
		Set<String> keyid = empinfo.keySet();
		int rowid = spreadsheet.getLastRowNum();

		for (String key : keyid) {
			row = spreadsheet.createRow(rowid++);
			Object[] objectArr = empinfo.get(key);
			int cellid = 0;

			for (Object obj : objectArr) {
				Cell cell = row.createCell(cellid++);
				cell.setCellValue((String) obj);
			}
		}

		// Write the workbook in file system
		FileOutputStream out = new FileOutputStream(new File(path));
		workbook.write(out);
		out.close();
		System.out.println("Writesheet.xlsx written successfully");

	}

	// Design by Sumeet to get the entire row in hashMap from Config sheet for
	// specified country
	public HashMap<String, String> readConfigSheetforRunConfig(String fileName, String sSheetName, String sCountry)
			throws Exception {
		HashMap<String, String> configHMap = new HashMap<String, String>();
		List<HashMap<String, String>> lstConfigHMap = readExcelDataToListofHashMap(fileName, sSheetName);

		for (HashMap<String, String> hmInstance : lstConfigHMap) {
			if (hmInstance.get(Constants.ExcelHeaderRunConfig).equalsIgnoreCase(sCountry.trim())) {
				configHMap = hmInstance;
			}
		}
		return configHMap;
	}
}
