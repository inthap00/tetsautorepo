package com.im.utility;
import java.sql.*;
import java.util.Properties;

import javax.sql.DataSource;

import com.im.b2b.api.business.APIEnumerations.ConnectToDatabase;

import oracle.jdbc.pool.OracleDataSource;



/**
 * Purpose of this class is to Setup Database Connection
 * @author Prashant Navkudkar
 */

public class DatabaseConnect {

	Connection conn = null;
	DataSource ds = null;
	public DatabaseConnect(ConnectToDatabase databaseType, String sDatabaseServiceName, String sUsername, String sPassword) {
		connectToDatabase(databaseType, sDatabaseServiceName, sUsername, sPassword);
	}
	
	/**
	 * This function is to connect the database
	 * @param dataBaseType: Provide the type of the database
	 * @param sDatabaseName: Provide name of the database
	 * @author Prashant Navkudkar
	 */
	private void connectToDatabase(ConnectToDatabase dataBaseType, String sDatabaseName, String sUsername, String sPassword) {
		
		try {
			switch(dataBaseType){

			case ODS: 
				ds =  getOracleDataSource(sDatabaseName, sUsername, sPassword);
			break;

			default:System.out.println("========= Please Select Appropriate database type ==============");
			break;
			}

		}
		catch(Exception e) {			
			e.printStackTrace();
		}
	}

	public DataSource getDatsSource() {
		return ds;
	}

	/**
	 * To get the connection
	 * 
	 * @return Returns the DB connection string
	 * @throws SQLException 
	 */
	public Connection getConnection() throws SQLException {
		return getDatsSource().getConnection();
	}
	
	public static DataSource getOracleDataSource(String sDatabaseName, String sUsername, String sPassword){
		String sConnString = "jdbc:oracle:thin:@10.22.144.25:1531:"+sDatabaseName+"";
		OracleDataSource oracleDS = null;
		try {
			oracleDS = new OracleDataSource();
			oracleDS.setURL(sConnString);
			oracleDS.setUser(sUsername);
			oracleDS.setPassword(sPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return oracleDS;
	}

	public void closeConnection() {
		try {
			if(conn!=null) {
				conn.close();
				System.out.println("<============= SUCCESS: Connection closed successfully ===============>");
			}else
				System.out.println("<============= FAILURE: Not Connected to the Database  ===============>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}