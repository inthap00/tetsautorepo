package com.im.utility.extentreport;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D.*/


import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JFileChooser;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.model.AbstractStructure;
import com.aventstack.extentreports.model.Log;
import com.im.b2b.api.business.AppUtil;

public class ExtentTestManager {
	private static final String ACTUAL_FAILED_STEPS = "<font color=\"RED\"><B>(ACTUAL_FAILED_STEP) </B></font>" ;

	private ExtentTestManager() {}
	private static HashMap<String, ExtentTest> extentTestMap = null;
	private static HashMap<String, ExtentCountryTestManager> extentCountryTestMap = null;
	private static Logger logger = Logger.getLogger("ExtentTestManager");
	static ExtentReports extent = null;


	static {
		try {
			extentTestMap = new HashMap<String, ExtentTest> ();
			extentCountryTestMap=new HashMap<String, ExtentCountryTestManager> ();
			extent = ExtentManager.getReporter();
			logger.debug("ExtentTestManager (Static block) parallel execution!!");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized void initializeExentReporter() {
		logger.debug("ExtentTestManager initializing the ExentReporter");
	}
	public static synchronized void log(Status pPolStatus, String pDesc, String sCountry) {
		long tID=Thread.currentThread().getId();
		String sId = ""+(Thread.currentThread().getId());
		//Generic Report
		((ExtentTest)extentTestMap.get(sId+sCountry)).log(pPolStatus, pDesc);

		//Country Specific report                              
		ExtentCountryTestManager objECTM = extentCountryTestMap.get(tID+sCountry);
		if(null==objECTM) {
			logger.error("I cant find the key for ["+tID+sCountry+"]");
			logger.error("ERROR:(Function log) The ExtentCountryTestManager is null for the country["+sCountry+"] Missed to log desc ["+pDesc+"] Thread["+sId+"]");
		}else {
			objECTM.log(pPolStatus, pDesc);
		}
	}

	public static void log(Status pPolStatus,String sCountry, Markup createScreenshotURL) {
		long tID=Thread.currentThread().getId();
		String sId = ""+(Thread.currentThread().getId());
		//Generic Report
		ExtentTest et = (ExtentTest)extentTestMap.get(sId+sCountry);
		(et).log(pPolStatus, createScreenshotURL);

		//Country Specific report                              
		ExtentCountryTestManager objECTM = extentCountryTestMap.get(tID+sCountry);
		if(null==objECTM) {
			logger.error("I cant find the key for ["+tID+sCountry+"]");
			logger.error("ERROR:(Function log) The ExtentCountryTestManager is null for the country["+sCountry+"] Missed to log Thread["+sId+"]");
		}else {
			objECTM.log(pPolStatus, createScreenshotURL);
		}

	}
	//            public static synchronized void removeTest( String sCountry, Status pTestStatus) {
	//                            long tID=Thread.currentThread().getId();
	//                            //Generic Report
	//                            ExtentTest test =extentTestMap.remove(""+ (tID));
	//                            test.getModel().setStatus(pTestStatus);
	//                            String sLogMesg="Starting test for thread["+tID+"] for testName["+test.getStatus()+"] test["+test+"]";
	//                            logger.debug(sLogMesg);
	//                            System.out.println(sLogMesg);
	//            }

	private static synchronized void resetFAILTestSteps(Status pTestsLatestStatus) {
		String sKey=""+ Thread.currentThread().getId();


		ITestContext oIContext =              Reporter.getCurrentTestResult().getTestContext();
		String sCountry = (String) oIContext.getAttribute(AppUtil.COUNTRY+sKey);

		ExtentTest test =extentTestMap.get(""+ (sKey+sCountry));
		//Commented by SUmeet as it gives the Actual failed step text in the extent report which is not in use for API
		 AbstractStructure<Log> abv = test.getModel().getLogContext();

		 for(int i=0;i<abv.size();i++) {
                                                Status stepsStatus = abv.get(i).getStatus();
                                                if(stepsStatus==Status.FAIL||stepsStatus==Status.ERROR) {
                                                                abv.get(i).setStatus(Status.INFO);
                                                                String sOriginalFailedStr = abv.get(i).getDetails();
                                                                abv.get(i).setDetails(ACTUAL_FAILED_STEPS+sOriginalFailedStr);
                                                }
                                }
		test.getModel().setStatus(pTestsLatestStatus);
		extentTestMap.put(""+ (sKey+sCountry), test);
	}

	public static synchronized void endTest( String sCountry, Status pTestsLatestStatus) {
		String sKey=""+ Thread.currentThread().getId();
		//Generic Reporting

		if(Status.PASS==pTestsLatestStatus) {
			resetFAILTestSteps(pTestsLatestStatus);
		}
		extent.flush();

		//Country Specific Reporting
		ExtentCountryTestManager objECTM = extentCountryTestMap.get(sKey+sCountry);
		if(null==objECTM) {
			logger.error("ERROR:(Function endTest) The ExtentCountryTestManager is null for the country["+sCountry+"]");
		}else {
			logger.debug("The test ends for ["+sCountry+"]["+sKey+"]");
			objECTM.endTestAndFlush();
		}
	}

	public static synchronized void setCategorMeansMethodNameToCountry( String sCountry, String sCategory,String testName) {
		String sKey=""+ Thread.currentThread().getId();


		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();           

		String sKey1 = testName+sCountry;                         

		String sCheck =  (String) oIContext.getAttribute(sKey1);

		if(null==sCheck || sCheck.equalsIgnoreCase("false")) {

			oIContext.setAttribute(sKey1, "true");

			//Generic Reporting
			Object obj = extentTestMap.get(sKey+sCountry);
			((ExtentTest)obj).assignCategory(sCategory);

			//Country Specific Reporting
			ExtentCountryTestManager objECTM = extentCountryTestMap.get(sKey+sCountry);
			if(null==objECTM) {
				logger.error("ERROR:(Function setCategory) The ExtentCountryTestManager is null for the country["+sCountry+"]");
			}else {
				//objECTM.setCategory(sCategory);
			}
		}
	}

	public static synchronized ExtentTest createTest(String testName, String desc, String Country) {
		long tID=Thread.currentThread().getId();
		ExtentTest test=null;
		String sKey = testName+Country;

		ITestContext oIContext =  Reporter.getCurrentTestResult().getTestContext();
		String sCheck =  (String) oIContext.getAttribute(sKey);

		if(null==sCheck || sCheck.equalsIgnoreCase("false")) {

			oIContext.setAttribute(sKey, "true");

			//Generic Report
			test=extent.createTest(testName, desc);
			extentTestMap.put(""+ (tID)+Country, test);
			String sLogMesg="Starting test for thread["+tID+"] for testName["+testName+"] desc["+desc+"] Country["+Country+"]";
			logger.debug(sLogMesg);
			System.out.println(sLogMesg);

			//Country Specific report
			ExtentCountryTestManager objECTM=  extentCountryTestMap.get(Country);
			if(null==objECTM) {
				objECTM=new ExtentCountryTestManager(Country);
				extentCountryTestMap.put(""+ (tID)+Country, objECTM);
			}
			objECTM.createTest(testName, desc);

		}
		return test;
	}


	public static synchronized HashMap<String, ExtentTest> getExtentTestMap() {
		return extentTestMap;
	}
	
	
}
