package com.im.utility.extentreport;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D.*/
import java.util.HashMap;

import org.apache.log4j.Logger;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;


public class ExtentCountryTestManager{
	private String sCountryname=null; 
	private HashMap<String, ExtentTest> extentCountryTestMap = null;
	ExtentReports extReport =null;
	private Logger logger = Logger.getLogger("ExtentCountryTestManager");
	
	public ExtentCountryTestManager(String sCountryName){
		extentCountryTestMap = new HashMap<String, ExtentTest> ();
		extReport =ExtentManager.getReporterForCountry(sCountryName);
		sCountryname=sCountryName;
	}
	public void log(Status pPolStatus, String pDesc) {
		String sId = ""+(Thread.currentThread().getId());
		((ExtentTest)extentCountryTestMap.get(sId+sCountryname)).log(pPolStatus, pDesc);
	}
	
	
	public void log(Status pPolStatus, Markup createScreenshotURL) {
		String sId = ""+(Thread.currentThread().getId());
		((ExtentTest)extentCountryTestMap.get(sId+sCountryname)).log(pPolStatus, createScreenshotURL);
		
	}
	
	public ExtentTest createTest(String testName, String desc) {
		long tID=Thread.currentThread().getId();
		ExtentTest test = extReport.createTest(testName, desc);
		logger.debug("ExtentCountryTestManager-startTest testName["+testName+"] desc["+desc+"] test["+test+"] key["+(tID)+sCountryname+"]");
		extentCountryTestMap.put(""+ (tID)+sCountryname, test);
		return test;
	}
	public void endTestAndFlush() {
		String sKey=""+ (Thread.currentThread().getId())+sCountryname;
		extReport.flush();
		logger.debug("EndTestAndFlush test for sKey["+sKey+"] for extReport["+extReport+"] extentCountryTestMap["+extentCountryTestMap+"] Country["+sCountryname+"]");
	}
	public void setCategory(String sCategory) {
		String sKey=""+ (Thread.currentThread().getId())+sCountryname;
		Object obj = extentCountryTestMap.get(sKey);
		((ExtentTest)obj).assignCategory(sCategory);
	}

	
	
}
