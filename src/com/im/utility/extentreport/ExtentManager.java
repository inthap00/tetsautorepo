package com.im.utility.extentreport;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D.,*/
import java.io.File;
import java.util.HashMap;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.b2b.api.business.AppUtil;

public class ExtentManager {
	private static ExtentReports extent;
	final static String sHTMLReportName="index.html"; 
	private static HashMap<String , ExtentReports> hmapCountryExtentReports = new HashMap<String , ExtentReports>  ();
	public static final String STG_ENV= "Environment", BROWSER_NAME="Browser Name";
	
	

	public synchronized static ExtentReports getReporter(){
		if(extent == null){
			String sFileName =Constants.ExtentReportOutputPath+ File.separator +sHTMLReportName;
			System.out.println("Generating the Extent report at["+sFileName+"]");
			ExtentHtmlReporter htmlReport = new ExtentHtmlReporter(sFileName);
			htmlReport.loadXMLConfig(new File(System.getProperty("user.dir")+ File.separator + "extent-config.xml"));
			
			/*String dateName = new SimpleDateFormat("ddMMMyy_hhmmss").format(new Date());
			String sFileName=Constants.ExtentReportOutputPath+"\\IMOnline_"+Util.getProperty("SmokeWorksheet")+"_"+dateName+".html"*/; 
			System.out.println("Path of the ExtentReports["+sFileName+"]");
			extent = new ExtentReports ();
//			extent.setSystemInfo("Host Name", "Ingram Online");
//			extent.setSystemInfo("Environment", "Automation Testing");
//			extent.setSystemInfo("User Name", "AutoTestUser");
//			extent.setSystemInfo(STG_ENV, sSTGenv); we will delete code after 30 nov 2018
//			extent.setSystemInfo("OS", System.getProperty("os.name"));
			setEnvironmentInfo(extent);
			extent.attachReporter(htmlReport);
			

		}
		return extent;
	}
	
	public synchronized static ExtentReports getReporterForCountry(String sCountryName){
		System.out.println("ExtentManager.getReporterForCountry Being called for the country["+sCountryName+"]");
		ExtentReports extentCountry=hmapCountryExtentReports.get(sCountryName);

		if(null==extentCountry) {
			String sCountryFileName =Constants.ExtentReportOutputPath+ File.separator +sCountryName.trim()+sHTMLReportName;
			ExtentHtmlReporter htmlCountryReport = new ExtentHtmlReporter(sCountryFileName);
			htmlCountryReport.loadXMLConfig(new File(System.getProperty("user.dir")+ File.separator + "extent-config.xml"));
			System.out.println("Path of the ExtentReports sCountryFileName["+sCountryFileName+"]");
			extentCountry = new ExtentReports ();
			System.out.println("ExtentManager.getReporterForCountry ***************getReporterForCountry sCountryName["+sCountryName+"] extentCountry["+extentCountry+"] ");
//			extentCountry.setSystemInfo(STG_ENV, sSTGenv); We will delete the code after 30 nov 2018
			setEnvironmentInfo(extentCountry);
			extentCountry.attachReporter(htmlCountryReport);

			hmapCountryExtentReports.put(sCountryName, extentCountry);
		}else System.out.println("The country already exist for country["+sCountryName+"]");
		
		return extentCountry;
	}
	
	private static void setEnvironmentInfo(ExtentReports ex) {	
		String sEnvironment=System.getProperty("Environment").trim();
		sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sEnvironment:RunConfig.getProperty(Constants.Environment);
		ex.setSystemInfo(STG_ENV, sEnvironment);		
	}
}