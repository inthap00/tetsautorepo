package com.im.utility.listener;
//**@author Sambodhan D. (Designer and Developer) *//*
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {
	final static int iRetryLimit = 1, iDefaultValue=0;
	private static HashMap<String , Integer> testMethodCountryRetryCounter  = new HashMap<String , Integer>();
	protected static Logger logger = Logger.getLogger("RetryAnalyzer");
	private static int iRetryGlobalCount = 0;
	
	
	public static synchronized String prepareUniqueKey(String sTestName, String Country) {
		String sCurrentThre= ""+Thread.currentThread().getId();
		String toReturn=sCurrentThre+sTestName+Country;
		return toReturn;
	}
	public synchronized static void setCounterToDefault(String sTestName, String Country) {
		String sUniqueKey=prepareUniqueKey(sTestName, Country);
		if(null==testMethodCountryRetryCounter.get(sUniqueKey))
		testMethodCountryRetryCounter.put(sUniqueKey, iDefaultValue);
	}
	
	public synchronized static int getCounterValue(String sTestName, String Country) {
		int iStoredCounter=testMethodCountryRetryCounter.get(prepareUniqueKey(sTestName, Country));
		return iStoredCounter;
	}
	
	private static void incrementCounterValue(String sTestName, String Country) {
		String sUniqueKey=prepareUniqueKey(sTestName, Country);
		int iStoredCounter=testMethodCountryRetryCounter.get(sUniqueKey);
		int iNEwCounter=(1+iStoredCounter);
		testMethodCountryRetryCounter.put(sUniqueKey, iNEwCounter);
	}
	public static synchronized boolean isRetryPossiblewithoutCounterChange(String sTestName, String Country) {
		logger.debug("Is Retry isRetryPossiblewithoutCounterChange?? sTestName["+sTestName+"] Country["+Country+"]");
		boolean status = false;
		try {
			int iCurrentCounterVal = getCounterValue(sTestName, Country);
		status=(iCurrentCounterVal<(iRetryLimit+1));
		}catch(Exception e) {}//pl dont do anything		
		return status;
	}
	
	public static int getGlobalRetryCount() {
		return iRetryGlobalCount;
	}
	
	private synchronized boolean isRetryPossible(String sTestName, String Country) {
		boolean status = false;
		int iCV = getCounterValue(sTestName, Country);
		logger.info("Is Retry possible?? sTestName["+sTestName+"] Country["+Country+"]iCV["+iCV+"]");
		
		if(iCV<iRetryLimit) {
			status=true;
			incrementCounterValue(sTestName, Country);
			iRetryGlobalCount++;
		}else {
			System.out.println("isRetryPossible iCV["+iCV+"] iRetryLimit["+iRetryLimit+"]");
			testMethodCountryRetryCounter.remove(prepareUniqueKey(sTestName, Country));
		}
		return status;
	}

	@Override
	public synchronized boolean retry(ITestResult result) {
		ITestContext itestContext = result.getTestContext();
		String sMethod = result.getMethod().getMethodName();
		String sThreadID = ""+Thread.currentThread().getId();
		String sCountry = (String) itestContext.getAttribute("Country"+sThreadID);
		boolean status = isRetryPossible(sMethod, sCountry);
		return status;
	}

}
