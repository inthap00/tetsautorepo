package com.im.utility.listener;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D.*/
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.im.api.common.Util;
import com.im.b2b.api.business.AppUtil;
import com.im.b2b.api.tests.BaseTest;
import com.im.utility.extentreport.ExtentManager;
import com.im.utility.extentreport.ExtentTestManager;
import com.im.wrapper.api.APIDriver;

public class TestngListener implements ITestListener	{
	protected static Logger logger = Logger.getLogger("TestngListener");
	private final String PASSED="Test passed", FAILED="Test Failed",SKIPPED= "Test Skipped"; 
	public static final String sTagSt = "<Mark>", sTagEn = "</Mark>";;
	private static int iPassCountAfterRetry = 0;
	private final String GlobalRetryCount = "Global Retry Count", PassCountAfterRetry = "Count of TC's passed after Retry";
	
	@SuppressWarnings("unused")
	private static String getClassName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();

	}

	@Override		
	public void onStart(ITestContext iTestContext) {
		ExtentTestManager.initializeExentReporter();
	}		
	//After ending all tests, below method runs.
	@Override
	public void onFinish(ITestContext iTestContext) {
		System.out.println("inside TEstNGListener onFinish");
		ExtentReports extent = ExtentManager.getReporter();
		extent.setSystemInfo(GlobalRetryCount, String.valueOf(RetryAnalyzer.getGlobalRetryCount()));
		extent.setSystemInfo(PassCountAfterRetry, String.valueOf(iPassCountAfterRetry));
		extent.flush();
	}

	@Override
	public void onTestStart(ITestResult iTestResult) {
		//Not adding anything for Extent as it needs additional info and handled at the TestSuite
		//System.out.println("TestngListener starting test case:"+iTestResult.getName()+"]");

	}
	protected boolean isItExecutableForCountry(ITestContext context, String sMethod, String sCountryCode) {
		String sCode=BaseTest.getTCMapCountryCode(sMethod, sCountryCode );

		Object obj=context.getAttribute(sCode);
	//	System.out.println("checking EXE - BaseTest.setContextForExeCountrySelection sCode["+sCode+"] obj["+obj+"] context["+context+"]");

		if(null==obj) return false;
		return (boolean) obj;

	}
	
	  private synchronized void  testCompleted(ITestResult iTestResult, Status pTestStatus) {
		boolean bTestSkipped=false;
		String sMethod = iTestResult.getMethod().getMethodName();
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = ""+Thread.currentThread().getId();
		String sCountry = (String) itestContext.getAttribute(AppUtil.COUNTRY+sThreadID);
		if(null==sCountry) {
			//System.out.println("+++++++++++++++++Smart man Sambodhan !");
			return ;
		}

		if(!isItExecutableForCountry(itestContext, sMethod, sCountry)) return;

		if(Status.ERROR==pTestStatus) 
			pTestStatus=Status.FAIL;

		String sStrStatus=PASSED;
		if(Status.FAIL==pTestStatus) sStrStatus=FAILED;
		else if(Status.SKIP==pTestStatus) {
			sStrStatus=SKIPPED;
			bTestSkipped=true;
		}

		logger.debug("TestngListener onFinish=======sCountry["+sCountry+"] sThreadID["+sThreadID+"] TestName["+sMethod+"]");
		boolean bRetryPossible=false;

		if(Status.PASS!=pTestStatus)
			bRetryPossible=RetryAnalyzer.isRetryPossiblewithoutCounterChange(sMethod, sCountry);

		if(Status.FAIL==pTestStatus||Status.PASS==pTestStatus||(bTestSkipped&&!bRetryPossible)) {
			if(Status.PASS==pTestStatus && BaseTest.getRetryCountRemover().containsKey(RetryAnalyzer.prepareUniqueKey(sMethod, sCountry)))
				iPassCountAfterRetry++;
			ExtentTestManager.log(pTestStatus, sStrStatus + " ["+sThreadID+"]["+sCountry+"]", sCountry); 
			ExtentTestManager.endTest(sCountry, pTestStatus); 
			BaseTest.removeRetryCounter(sMethod, sCountry);
			itestContext.removeAttribute(AppUtil.COUNTRY+sThreadID); 
		}else if(bTestSkipped&&bRetryPossible) {
			String sMessage="Re-executing with the RetryAnalyzer!! Please wait! Round=";
			ExtentTestManager.log(Status.INFO, sTagSt+sMessage+(RetryAnalyzer.getCounterValue(sMethod, sCountry)+1)+sTagEn, sCountry);
			BaseTest.setTestSkipped(sMethod, sCountry);
		}else {
			String sError="TestNGListener: ERROR your logic is incomplete...rework";
			logger.error(sError);
			ExtentTestManager.log(Status.FAIL, sTagSt+sError+sTagEn, sCountry);
			System.out.println(sError);
		}
	}
	
	
	
	@Override 
	public synchronized void onTestSuccess(ITestResult iTestResult) {
		//System.out.println("inside TEstNGListener onTestSuccess: "+iTestResult.getName()+"]");
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = ""+Thread.currentThread().getId();
		APIDriver objAPIDriver =  (APIDriver) itestContext.getAttribute(AppUtil.HDRIVER+sThreadID);

		if(objAPIDriver==null) { 
			//This is because the country is skipped in the TestApplicabilityMatrix. So relax
		}else{
			if(!objAPIDriver.isTestCaseCompleted())
				onTestFailure(iTestResult);
			else
				testCompleted(iTestResult, Status.PASS);
		}
	}
	
	
/*Updated to test Reporting flush Right now in progress
 * 	@Override
	public synchronized void onTestSuccess(ITestResult iTestResult) {
		System.out.println("inside TEstNGListener onTestSuccess"+iTestResult.getName()+"]");
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		String sThreadID = ""+Thread.currentThread().getId();
		APIDriver hDriver =  (APIDriver) itestContext.getAttribute(AppUtil.HDRIVER+sThreadID);

		System.out.println("################## Yes I am In #####################s");
		
		
			 testCompleted(iTestResult, Status.PASS);
		
	}
	*/

	@Override
	public synchronized void onTestFailure(ITestResult iTestResult) {
		String sMethod = iTestResult.getMethod().getMethodName();
		System.out.println("inside TEstNGListener onTestFailure sMethod="+sMethod+"]");
		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		//		String sAssertionErrorCode="java.lang.AssertionError";
		String sThreadID = ""+Thread.currentThread().getId();
		String sCountry = (String) itestContext.getAttribute("Country"+sThreadID);
		String sExceptionReceived=iTestResult.getThrowable().getMessage();
		logger.error("The complete Exception for the Debugging: ["+sExceptionReceived+"]");
		if(null!=sExceptionReceived) sExceptionReceived=Util.getExceptionDesc(sExceptionReceived);
		boolean bScreenshotTaken=false;

		//This condition is added to take screenshot to analuyze the cause of the failure
		APIDriver hDriver= (APIDriver) itestContext.getAttribute(AppUtil.HDRIVER+sThreadID);
	//	hDriver.logFailureAndTakeScreensot("Unexpected Failure!["+sExceptionReceived+"]");
		bScreenshotTaken=true;
		//}
		//Dont delete below print statement
		System.out.println("EXCEPTION DEBUG ON FAILURE----for country["+sCountry+"] CheckExcception["+sExceptionReceived+"]\r\r ");		
		testCompleted(iTestResult, Status.FAIL); 
	}

	@Override
	public synchronized void onTestSkipped(ITestResult iTestResult) {
		String sMethod = iTestResult.getMethod().getMethodName();
		String sThreadID = ""+Thread.currentThread().getId();
		String sCountry = (String) iTestResult.getTestContext().getAttribute("Country"+sThreadID);

		System.out.println("inside TEstNGListener onTestSkipped sMethod="+sMethod+"] Country["+sCountry+"]");

		ITestContext itestContext = Reporter.getCurrentTestResult().getTestContext();
		boolean bScreenshotTaken=false;
		String sExceptionReceived=iTestResult.getThrowable().getMessage();
		if(null!=sExceptionReceived) sExceptionReceived=Util.getExceptionDesc(sExceptionReceived);
		APIDriver hDriver= (APIDriver) itestContext.getAttribute(AppUtil.HDRIVER+sThreadID);
	//	hDriver.logFailureAndTakeScreensot("Unexpected Skipped!["+sExceptionReceived+"]");
		bScreenshotTaken=true;
		System.out.println("EXCEPTION DEBUG ON SKIPPED----for country["+sCountry+"] bScreenshotTaken["+bScreenshotTaken+"] CheckExcception["+sExceptionReceived+"]\r\r ");
		//}
		//Dont delete below print statement

		testCompleted(iTestResult, Status.SKIP);
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
		System.out.println("inside TEstNGListener onTestFailedButWithinSuccessPercentage");
	}

}



