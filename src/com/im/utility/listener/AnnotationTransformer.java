package com.im.utility.listener;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D.*/
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.ITestAnnotation;

import com.im.b2b.api.tests.BaseTest;

public class AnnotationTransformer implements IAnnotationTransformer {

//	@Override
//	public synchronized void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
//		if(null==Reporter.getCurrentTestResult()) return;
//		ITestContext context=Reporter.getCurrentTestResult().getTestContext();
//
//		String sThreadID = ""+Thread.currentThread().getId();
//		String sCountryCode = (String) context.getAttribute("Country"+sThreadID);
//
//		String sCode=BaseTest.getTCMapCountryCode(testMethod.getName(), sCountryCode );
//		Object obj=context.getAttribute(sCode);
//
//
//
//		IRetryAnalyzer retry = annotation.getRetryAnalyzer();
//		if (retry == null) {
//			annotation.setRetryAnalyzer(RetryAnalyzer.class);
//		}
//	}
		

		@Override
		public synchronized void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
			IRetryAnalyzer retry = annotation.getRetryAnalyzer();
			if (retry == null) {
			annotation.setRetryAnalyzer(RetryAnalyzer.class);
			}
		}
}
