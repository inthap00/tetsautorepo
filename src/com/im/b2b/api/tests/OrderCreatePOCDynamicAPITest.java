package com.im.b2b.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.b2b.api.action.OrderCreatePOCDynamicAPIActionBucket;
import com.im.b2b.api.business.AppUtil;
import com.im.b2b.api.business.MapTCForNations;
import com.im.utility.ReadExcelFile;
import com.im.utility.extentreport.ExtentTestManager;
import com.im.wrapper.api.APIDriver;

public class OrderCreatePOCDynamicAPITest extends BaseTest {

	Logger logger = Logger.getLogger("OrderCreatePOCDynamicAPITest");
	public final static String ModuleNameInTestApplicability = "OrderCreateDynamicPOC";
	public final static String TestDataFile = "TestDataForOrderCreatePOCDynamic.xlsx";
	public final static boolean isMultiSet = false;
	public final static String sFileName="OrderCreatePOCDynamic";
	public static List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());

	public OrderCreatePOCDynamicAPITest(HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap)
			throws Exception {
		super(pExcelHMap, pConfigHMap, ModuleNameInTestApplicability);
	}

	//Adding text for Git Check-in 
	@Test(description = "<<Jira ID for Tracking>>TC1: Validating Order Create Workflow", priority = 1)
	public void createAndValidateOrderDetails() throws Exception {
		logger.info("Entering OrderCreatePOCDynamicAPITest.createAndValidateOrderDetails");

		if (!(isTestMappedToCountry(new Object() {
		}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)))
			return;
		ExtentTestManager.log(Status.INFO, "Running for Test Data SR.NO[" + hmTestData.get(Constants.EXCELHEADER_SRNO) + "]",
				hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver = AppUtil.preparePrequisites(oIContext, hmTestData);

		OrderCreatePOCDynamicAPIActionBucket orderDetailsAPIActionBucket = new OrderCreatePOCDynamicAPIActionBucket(hmTestData,
				hmConfig, objAPIDriver);
		orderDetailsAPIActionBucket.createAndValidateOrder();
		orderDetailsAPIActionBucket.validateOrderDetailsWithDB();

		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	// Get test data from excel file
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim = null;
		Object[] data1Dim = null;
		String sExcelFileName = null, sTabName = null;

		String sSheetName = System.getProperty("Environment").trim();
		sExcelFileName = Constants.BASEPATH + "\\TestData\\" + TestDataFile;

		HashMap<String, MapTCForNations> listTCMappingToCountry = MapTCForNations
				.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) ? sSheetName
				: RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObject(sExcelFileName, sTabName);

			if (null != myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim = new Object[iTotalCountryGiven];
				for (int i = 0; i < iTotalCountryGiven; i++) {

					@SuppressWarnings("unchecked")
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					HashMap<String, String> pConfigHMap = new ReadExcelFile().readConfigSheetforRunConfig(
							sExcelFileName, RunConfig.getProperty(Constants.ConfigSheetName),
							pExcelHMap.get(Constants.ExcelHeaderRunConfig));

					// ChangeClass
					OrderCreatePOCDynamicAPITest newInstance = new OrderCreatePOCDynamicAPITest(pExcelHMap, pConfigHMap);

					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i] = newInstance;
				}
			} else
				System.out.println(
						"Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}

	// Update status in report
	@AfterTest(alwaysRun = true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath() + "\\ExtentReports\\APIResults\\" + sCallingClassName
				+ ".xlsx";
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData", "TEST CASE", "REQUEST", "RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}
