package com.im.b2b.api.tests;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.b2b.api.business.AppUtil;
import com.im.b2b.api.business.MapTCForNations;
import com.im.b2b.api.control.CountryGroupBuilder;
import com.im.b2b.api.control.CountryGroups;
import com.im.utility.ReadExcelFile;
import com.im.utility.extentreport.ExtentTestManager;
import com.im.utility.listener.RetryAnalyzer;

public class BaseTest {
	Logger logger = Logger.getLogger("BaseTest");
	protected String sCountryCode = null;
	protected HashMap<String, String> hmTestData = null;
	protected String sModuleName = null, sMethodName = null;
	protected CountryGroups countryGroup = null;
	private static HashMap<String, Boolean> retryCountRemover = new HashMap<String, Boolean>();
	protected HashMap<String, MapTCForNations> listTCMappingToCountry = null;
	protected HashMap<String, String> hmConfig = null;

	public BaseTest(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig, String pModuleName)
			throws Exception {
		hmTestData = new HashMap<>();
		hmTestData.putAll(phmTestData);
		this.sCountryCode = hmTestData.get(Constants.ExcelHeaderRunConfig);
		countryGroup = new CountryGroupBuilder().getCountryGroupsForCountry(sCountryCode);
		hmConfig = phmConfig;
		sModuleName = pModuleName;
	}

	public BaseTest(HashMap<String, String> phmTestData, String pModuleName) throws Exception {
		hmTestData = new HashMap<>();
		sModuleName = pModuleName;
		hmTestData.putAll(phmTestData);
		hmConfig = AppUtil.getCredentialData(hmTestData);
		this.sCountryCode = hmTestData.get(Constants.ExcelHeaderRunConfig);
		countryGroup = new CountryGroupBuilder().getCountryGroupsForCountry(sCountryCode);

	}

	public synchronized static void setTestSkipped(String sMethod, String Country) {
		String sKey = RetryAnalyzer.prepareUniqueKey(sMethod, Country);
		retryCountRemover.put(sKey, true);
	}

	private synchronized static boolean isTestSkipped(String sTestName, String Country) {
		String sKey = RetryAnalyzer.prepareUniqueKey(sTestName, Country);
		boolean bStatus = false;
		Object obj = retryCountRemover.get(sKey);
		if (null != obj)
			bStatus = (boolean) obj;
		return bStatus;
	}

	public synchronized static void removeRetryCounter(String sTestName, String Country) {
		String sKey = RetryAnalyzer.prepareUniqueKey(sTestName, Country);
		retryCountRemover.remove(sKey);
	}
	// ----------------Ends the TC of retry analyzer

	// =======starts the methods of the test case execution as per the countries
	protected void setTheCountriesForTheTC(HashMap<String, MapTCForNations> pListTCMappingToCountry) {
		listTCMappingToCountry = pListTCMappingToCountry;

	}

	protected synchronized boolean isItExecutableForCountry(ITestContext context, String sMethod) {
		String sCode = getTCMapCountryCode(sMethod, sCountryCode);
		Object obj = context.getAttribute(sCode);
		if (null == obj)
			return false;
		return (boolean) obj;
	}

	protected synchronized boolean isTestMappedToCountry(String sMethod) {
		boolean bAnswer = false;
		MapTCForNations oMap = listTCMappingToCountry.get(sMethod);
		if (null != oMap) {
			bAnswer = oMap.isCountryEabledForExecution(sCountryCode);
		}
		return bAnswer;
	}

	@BeforeMethod(alwaysRun = true)
	protected synchronized void setPlatform(Method method, ITestContext context) throws Exception {
		Test test = method.getAnnotation(Test.class);
		String sMethod = method.getName();

		sMethodName = sMethod;

		boolean bIsTestCaseMappedToCountry = isTestMappedToCountry(sMethod);
		if (!bIsTestCaseMappedToCountry)
			return;

		boolean bisTestCategoryMapped = isTestCategoryMapped(sModuleName, context);
		if (!bisTestCategoryMapped)
			return;

		String sDesc = "[Empty]";
		if (test != null) {
			sDesc = test.description();
		}

		String sCode = getTCMapCountryCode(sMethod, sCountryCode);

		setContextForExeCountrySelection(sCode, context, bIsTestCaseMappedToCountry);

		if (!isTestSkipped(sMethod, sCountryCode)) {
			String sEnvironment = System.getProperty("Environment").trim();
			sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) ? sEnvironment
					: RunConfig.getProperty(Constants.Environment);

			String sCountryName = CountryGroups.getCountryName(sCountryCode);

			String sURL = hmConfig.get(Constants.EXCELHEADEREndPointURL + "_" + sEnvironment);
			String sUserName = hmConfig.get("UserName" + "_" + sEnvironment);
			String sPassword = hmConfig.get("Password" + "_" + sEnvironment);

			if (null == sUserName && null == sPassword)
				sUserName = sPassword = "Sent from request Body";

			String sCredential = "<b>Username: </b> " + sUserName + "<br>" + "<b>Password: </b>" + sPassword;

			ExtentTestManager.createTest(sMethod + "[" + sCountryName + "]", sDesc + "<font color=\"red\">["
					+ sCountryCode + "]</font><br>" + "<b>URL: </b>" + sURL + "<br>" + sCredential, sCountryCode);
			RetryAnalyzer.setCounterToDefault(sMethod, sCountryCode);
			String sClassName = method.getDeclaringClass().getSimpleName();
			ExtentTestManager.setCategorMeansMethodNameToCountry(sCountryCode, sClassName, sMethod);
		} else {
			System.out.println(" In Extent the tc not created ====================sMethod[" + sMethod
					+ "] sCountryCode[" + sCountryCode + "] ");
		}
	}

	public static String getTCMapCountryCode(String sMethod, String sCountryCode) {
		String sThreadID = "" + Thread.currentThread().getId();
		return sThreadID + sMethod + sCountryCode;
	}

	public static HashMap<String, Boolean> getRetryCountRemover() {
		return retryCountRemover;
	}

	private synchronized void setContextForExeCountrySelection(String sCode, ITestContext context,
			boolean bIsTestCaseMappedToCountry) {
		context.setAttribute(sCode, bIsTestCaseMappedToCountry);
	}

	protected synchronized boolean isTestCategoryMapped(String psSheetName) throws Exception {
		boolean bCheck = false;
		String sTestAplicability = Constants.BASEPATH
				+ RunConfig.getProperty(Constants.EXCELNAME_TESTCASECOUNTRY_MAP).trim();
		List<HashMap<String, String>> lstTestAppHMap = new ReadExcelFile()
				.readExcelDataToListofHashMap(sTestAplicability, psSheetName);

		String sTestName = sMethodName;

		for (HashMap<String, String> objHMap : lstTestAppHMap) {
			if (objHMap.get("Test Script").equalsIgnoreCase(sTestName)) {
				List<String> arr = Arrays.asList(objHMap.get("TestDataCategory").split("~"));
				if (arr.contains(hmTestData.get("TestDataCategory")) || arr.contains("ALL")) {
					bCheck = true;
					break;
				}
			}
		}

		return bCheck;
	}

	private boolean isTestCategoryMapped(String psSheetName, ITestContext context) throws Exception {
		boolean bCheck = false;
		String sTestAplicability = Constants.BASEPATH
				+ RunConfig.getProperty(Constants.EXCELNAME_TESTCASECOUNTRY_MAP).trim();
		List<HashMap<String, String>> lstTestAppHMap = new ReadExcelFile()
				.readExcelDataToListofHashMap(sTestAplicability, psSheetName);

		String sTestName = sMethodName;
		/**
		 * Handling for TestDataRepository without TestDataCategory Updated by Prashant
		 * Navkudkar
		 */
		boolean fCheckCategoryColumn = lstTestAppHMap.get(0).containsKey("TestDataCategory");
		if (!fCheckCategoryColumn)
			return true;

		for (HashMap<String, String> objHMap : lstTestAppHMap) {
			if (objHMap.get("Test Script").equalsIgnoreCase(sTestName)) {
				List<String> arr = Arrays.asList(objHMap.get("TestDataCategory").split("~"));
				if (arr.contains(hmTestData.get("TestDataCategory")) || arr.contains("ALL")) {
					bCheck = true;
					context.setAttribute(
							Thread.currentThread().getId() + sTestName + hmTestData.get("TestDataCategory"), bCheck);
					break;
				}
			}
		}
		return bCheck;
	}

	@AfterMethod(alwaysRun = true)
	synchronized void afterMethod(Method method, ITestResult result, ITestContext oContext) {
		System.gc();
	}
}
