package com.im.b2b.api.testdata.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.im.api.common.Constants;
import com.im.b2b.api.business.APIEnumerations.ConnectToDatabase;
import com.im.b2b.api.business.APIEnumerations.dataBaseName;
import com.im.b2b.api.business.ExecuteSQLscript;

public class GetOrderDetailsFromDB {
	LinkedHashMap<String,String> mapData;
	Logger logger = Logger.getLogger("GetOrderDetailsFromDB");
	ExecuteSQLscript es;
	
	public GetOrderDetailsFromDB(String dbName, String sheetName) throws FileNotFoundException, IOException, SQLException{
		mapData = getExcelDataAsMap(sheetName);
		es = new ExecuteSQLscript(ConnectToDatabase.ODS, dataBaseName.ODS, Constants.DBUSERNAME, Constants.DBPASSWORD);
	}
	
	/**
	 * Enum refering to excel sheet name where the query is placed
	 * 
	 * @author Priyesh Thakur
	 */
	public static enum sheetName {
		ORDERDETAILS("OrderDetails");

		public String value;

		private sheetName(String value) {
			this.value = value;
		}
	}

	/**
	 * Enum for Order Status of Impulse 
	 * 
	 * @author Priyesh Thakur
	 */
	public static enum OrderStatusImpulse {
		INVOICED("I"), VOIDED ("V"), OPENORDER("B"), RELEASED("R"),
		SHIPPED("4");

		public String value;

		private OrderStatusImpulse(String value) {
			this.value = value;
		}
	}
	
	/**
	 * Enum for Fields available in DB for order details
	 * 
	 * @author Priyesh Thakur
	 */
	public static enum OrderDetailsDBFields {
		ORDERBRANCH("ORDERBRANCH"), ORDERNUMBER ("ORDERNUMBER"), BILLTOCOUNTRYCODE("BILLTOCOUNTRYCODE"), 
		SHIPTOCOUNTRYCODE("SHIPTOCOUNTRYCODE"), SHIPMENTSTATUSDESC("SHIPMENTSTATUSDESC"), BILLTOCITY("BILLTOCITY"), 
		SHIPTOSTATE("SHIPTOSTATE"), CUSTOMERNUMBER("BILL_TO_CUST_NBR");

		public String value;

		private OrderDetailsDBFields(String value) {
			this.value = value;
		}
	}
	
	/**
	 * Function to get query for fetching all the orders for provided country and order status
	 * @param countryCode country code
	 * @statusOfOrder Order status
	 * @return query to get orders as per country and status entered
	 * 
	 * @author Priyesh Thakur
	 */
	public String getAllOrdersAsPerStatusQuery(String countryCode, String statusOfOrder) {
		return mapData.get("getAllOrdersAsPerStatusQuery").replace("@countryCode", countryCode).replace("@statusOfOrder", statusOfOrder);
	}
	
	/**
	 * Function to hashmap of queries from the excel sheet
	 * @param sheetName sheet name to fetch queries
	 * @return Hashmap of queries with respective keys
	 * 
	 * @author Priyesh Thakur
	 */
	public LinkedHashMap<String, String> getExcelDataAsMap(String sheetName) throws EncryptedDocumentException, IOException {
		// Create a Workbook
		Workbook wb = WorkbookFactory.create(new File("src\\DBQueries.xlsx"));
		// Get sheet with the given name "Sheet1"
		Sheet s = wb.getSheet(sheetName);
		// Initialized an empty LinkedHashMap which retain order
		LinkedHashMap<String, String> data = new LinkedHashMap<>();
		// Get total row count
		int rowCount = s.getPhysicalNumberOfRows();
		// Skipping first row as it contains headers
		for (int i = 1; i < rowCount; i++) {
			// Get the row
			Row r = s.getRow(i);
			// Since every row has two cells, first is field name and another is value.
			String fieldName = r.getCell(0).getStringCellValue();
			String fieldValue = r.getCell(1).getStringCellValue();
			data.put(fieldName, fieldValue);
		}
		return data;
	}
	
	/**
	 * Function to get order details from db
	 * @param countryCode country code
	 * @statusOfOrder Order status
	 * @return Hashmap of order details
	 * 
	 * @author Priyesh Thakur
	 */
	public HashMap<String, String> getOrderDetailsData(String countryCode, String statusOfOrder) throws SQLException{
		
		HashMap<String, String> inputData = new HashMap<>();
		ResultSet rs = es.executeQuery(getAllOrdersAsPerStatusQuery(countryCode, statusOfOrder));
		List<String> colList = es.returnAllColumns(rs);
		
		for (int i=0; i<=colList.size()-1; i++) {
			String colValue = es.returnColData(rs, colList.get(i)).get(0);
			inputData.put(colList.get(i), colValue);
		}
		return inputData;
	}
	
	/**
	 * Function to get country code to be passed to db query
	 * 
	 * @param countryCode country code
	 * @return country code for DB
	 * 
	 * @author Priyesh Thakur
	 */
	public String getCountryCodeForDB(String countryCode) {
		String countryCodeForDB = null;
		switch (countryCode) {
		case "US":
			countryCodeForDB = "MD";
			break;
		default:
			logger.assertLog(false, "Invalid parameter passed to getCountryCode function. Parameter: " + countryCode);
		}
		return countryCodeForDB;
	}
}
