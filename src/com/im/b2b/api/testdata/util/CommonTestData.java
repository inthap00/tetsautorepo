package com.im.b2b.api.testdata.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.wrapper.api.APIAssertion;

public class CommonTestData {

	APIAssertion objAPIAssertion = null;
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	Logger logger = Logger.getLogger("CommonValidation");
	
	public static final List<String> headerColumns = new ArrayList<String>(Arrays.asList(Constants.KEY_IMCUSTOMERNUM,
	 		Constants.KEY_IMCOUNTRYCODE, Constants.KEY_IMCORRELATIONID, Constants.KEY_IMSENDERID));
	public static final List<String> requestParamColumns = new ArrayList<String>(
			Arrays.asList(Constants.KEY_INGRAMORDERDATE, Constants.KEY_ISIML, Constants.KEY_SIMULATESTATUS));

	public CommonTestData(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,
			APIAssertion pAPIAssertion) {
		hmTestData = phmTestData;
		hmConfig = phmConfig;
		objAPIAssertion = pAPIAssertion;
	}
	
	/**
	 * Function to get all headers from excel sheet to add to a request
	 * 
	 * @param hmTestData Hashmap of test data required for running a test case
	 * @return Hashmap contains all the headers require to pass for a request
	 * 
	 * @author Priyesh Thakur
	 */
	public HashMap<String, String> getHeaders() {
		logger.info("Entering OrderDetailsAPIActionBucket.getHeaders");

		HashMap<String, String> header = new HashMap<>();
		header.put("Content-Type", "application/json");
		for (String key : headerColumns) {
			if (hmTestData.get(key) != null) {
				if (!hmTestData.get(key).equals("<<BLANK>>")) {
					header.put(key, hmTestData.get(key));
				}
			} else {
				header.put(key, "");
			}
		}
		return header;
	}

	/**
	 * Function to get all parameters from excel sheet to add to a request
	 * 
	 * @param hmTestData Hashmap of test data required for running a test case
	 * @return Hashmap contains all the parameters require to pass for a request
	 * 
	 * @author Priyesh Thakur
	 */
	public HashMap<String, String> getParameters() {
		logger.info("Entering OrderDetailsAPIActionBucket.getParameters");

		HashMap<String, String> params = new HashMap<>();
		for (String key : requestParamColumns) {
			if (hmTestData.get(key) != null) {
				if (!hmTestData.get(key).equals("<<BLANK>>")) {
					params.put(key, hmTestData.get(key));
				}
			} else {
				params.put(key, "");
			}
		}
		return params;
	}

	
	/**
	 * Function to get api endpoint with orderID(Passed dynamically)
	 * 
	 * @param orderId Order id for which you require the details
	 * @return endpoint for order details with the specific orderId
	 * 
	 * @author Priyesh Thakur
	 */
	public String getAPIEndPointWithOrderId(String orderId) {
		logger.info("Entering OrderDetailsAPIActionBucket.getAPIEndPointWithOrderId");

		String sEnvironment = System.getProperty("Environment").trim();
		sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) ? sEnvironment
				: RunConfig.getProperty(Constants.Environment);
		String URI = hmConfig.get(Constants.EXCELHEADEREndPointURL + "_" + sEnvironment);
		return URI.replace("orderId", orderId);
	}
}
