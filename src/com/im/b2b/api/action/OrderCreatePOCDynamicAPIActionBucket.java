package com.im.b2b.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.testng.Reporter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.api.common.Util;
import com.im.api.validation.OrderDetailsAPIValidation;
import com.im.b2b.api.business.AppUtil;
import com.im.b2b.api.business.JsonUtil;
import com.im.b2b.api.testdata.util.CommonTestData;
import com.im.b2b.api.testdata.util.GetOrderDetailsFromDB;
import com.im.b2b.api.testdata.util.GetOrderDetailsFromDB.OrderDetailsDBFields;
import com.im.b2b.api.testdata.util.GetOrderDetailsFromDB.OrderStatusImpulse;
import com.im.b2b.api.testdata.util.GetOrderDetailsFromDB.sheetName;
import com.im.b2b.api.tests.OrderCreatePOCDynamicAPITest;

import io.restassured.response.Response;

import com.im.wrapper.api.APIAssertion;
import com.im.wrapper.api.APIDriver;
import com.im.wrapper.api.ToolAPI;
import java.lang.reflect.Type;
import com.google.common.reflect.TypeToken;
//hgvhff
public class OrderCreatePOCDynamicAPIActionBucket {
	Logger logger = Logger.getLogger("OrderDetailsAPIActionBucket");
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	HashMap<String, String> orderDetailsDataFromDB;
	String ingramOrderNumber;

	public OrderCreatePOCDynamicAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,
			APIDriver pAPIDriver) {
		hmTestData = phmTestData;
		hmConfig = phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	/**
	 * Function to validate order details
	 * 
	 * @author Priyesh Thakur
	 */
	public void createAndValidateOrder() throws Exception {
		logger.info("Entering OrderCreatePOCAPIActionBucket.createAndValidateOrder");

		String sEnvironment = System.getProperty("Environment").trim();
		sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) ? sEnvironment
				: RunConfig.getProperty(Constants.Environment);
		hmConfig.get(Constants.EXCELHEADEREndPointURL + "_" + sEnvironment);

		String requestBody = null;
		JsonUtil jsonUtil = null;
		try {
			requestBody = AppUtil.getRequestBodyForRestRequest(OrderCreatePOCDynamicAPITest.sFileName, hmTestData,
					OrderCreatePOCDynamicAPITest.isMultiSet, OrderCreatePOCDynamicAPITest.TestDataFile);
			requestBody = AppUtil.getRequestBodyWithoutNullValues(requestBody);
			jsonUtil = new JsonUtil(sEnvironment, hmTestData, hmConfig);

			// Provide Endpoint for Order Create
			String[] endPoints = hmConfig.get(Constants.EXCELHEADEREndPointURL + "_" + sEnvironment).split("~");
			String endpointHost_post = endPoints[0];
			String endpointHost_get = endPoints[1];

			String arrUpdateParam[] = { endpointHost_post, "" };

			// String requestBody1 = getPayload();

			JSONObject objJSON = new JSONObject(requestBody);
			
			CommonTestData ctd = new CommonTestData(hmTestData, hmConfig, objAPIAssertion);
			HashMap<String, String> reqHeaders = ctd.getHeaders();
			HashMap<String, String> reqParameters = ctd.getParameters();

			// Get response for the details sent to httpRequest
			Response postResponse = jsonUtil.call("post", true, arrUpdateParam, objJSON, reqHeaders, reqParameters);

			// Validate response
			objAPIAssertion.assertHardEquals(String.valueOf(postResponse.getStatusCode()), "201",
					"Status Code Validation");
			String ingramOrderNumber = jsonUtil.getValueFromJSONResponse("orders[0].ingramOrderNumber");

			// Provide Endpoint for Order Details
			String endpointHostOrderDetails = endpointHost_get.replace("orderId", ingramOrderNumber);
			String arrUpdateParamOrderDetails[] = { endpointHostOrderDetails, " " };

			JSONObject objJSONOrderDetails = new JSONObject();

			// Get response for the details sent to httpRequest
			Response responseOrderDetails = jsonUtil.call("get", true, arrUpdateParamOrderDetails, objJSONOrderDetails,
					ctd.getHeaders(), ctd.getParameters());

			// Validate response
			objAPIAssertion.assertHardEquals(String.valueOf(responseOrderDetails.getStatusCode()), "200",
					"Status Code Validation");

			// Validate test data
			String[] orderCreateXpaths = hmTestData.get("Validation_APIVsAPI_OrderCreateAPI").split("~");
			String[] orderDetailsXpaths = hmTestData.get("Validation_APIVsAPI_OrderDetailsAPI").split("~");

			OrderDetailsAPIValidation cv = new OrderDetailsAPIValidation(objAPIAssertion);
			cv.validateAPI_APIData(postResponse, orderCreateXpaths, responseOrderDetails, orderDetailsXpaths);

		} catch (Exception e) {
			String sErrMessage = "FAIL: validateOrderDetails() method of OrderDetailsAPIActionBucket: "
					+ Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage, e);
		} finally {
			String sTestCaseName = (String) Reporter.getCurrentTestResult().getTestContext()
					.getAttribute(AppUtil.METHOD + Thread.currentThread().getId());
			OrderCreatePOCDynamicAPITest.lstResultSet.add(new Object[] { hmTestData.get(Constants.EXCELHEADER_SRNO),
					sTestCaseName, hmTestData.get(Constants.KEY_INGRAMORDERNUM), jsonUtil.getResponse().asString() });
		}
	}	

	public String getRequestBodyWithoutNullValuesLogic(String requestBody) {
		Type type = new TypeToken<Map<String, Object>>() {
		}.getType();
		Map<String, Object> data = new Gson().fromJson(requestBody, type);

		for (Iterator<Map.Entry<String, Object>> it = data.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Object> entry = it.next();
			if (entry.getValue().equals("<<REMOVEKEY>>")) {
				it.remove();
			} else if (entry.getValue() instanceof ArrayList) {
				if (((ArrayList<?>) entry.getValue()).equals("<<REMOVEKEY>>")) {
					it.remove();
				}
			}
		}

		requestBody = new GsonBuilder().setPrettyPrinting().create().toJson(data);
		return requestBody;
	}

	private String getRequestBodyWithoutNullValues(String text) {
		// JSONObject objJson = new JSONObject(text);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(text);
		String prettyJsonString = gson.toJson(je);
		String[] arr = prettyJsonString.toString().split("\n"); // every arr items is a line now.
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= arr.length - 1; i++) {
			if (arr[i].contains("REMOVEKEY")) {
				if (arr[i + 1].contains("}")) {
					if (String.valueOf(sb.toString().charAt(sb.toString().length() - 1)).equals(",")) {
						String newStr = sb.toString().substring(0, sb.toString().length() - 1);
						sb = new StringBuilder(newStr);
					}
				}
			} else {
				sb.append(arr[i]);
			}
		}
		return sb.toString(); // new file that does not contains that lines.
	}

	

	public HashMap<String, Object> getValuesForPayloadObject(String payloadValues) {
		HashMap<String, Object> payloadObject = new HashMap<String, Object>();
		String objectValue = payloadValues.replace("\n", "");
		String[] arrayPayloadValues = objectValue.split(",");
		for (int i = 0; i <= arrayPayloadValues.length - 1; i++) {
			String key = arrayPayloadValues[i].split(":")[0].replace("\"", "").trim();
			String value = arrayPayloadValues[i].split(":")[1].trim();
			payloadObject.put(key, value);
		}
		return payloadObject;
	}

	public List<HashMap<String, Object>> getValuesForPayloadArray(String payloadValues) {
		List<HashMap<String, Object>> payloadArray = new ArrayList<HashMap<String, Object>>();
		String[] objectValueArray = payloadValues.split("~");
		for (int i = 0; i <= objectValueArray.length - 1; i++) {
			HashMap<String, Object> payloadObject = new HashMap<String, Object>();
			String objectValue = objectValueArray[i].replace("\n", "");
			String[] arrayPayloadValues = objectValue.split(",");
			for (int j = 0; j <= arrayPayloadValues.length - 1; j++) {
				String key = arrayPayloadValues[j].split(":")[0].replace("\"", "").trim();
				String value = arrayPayloadValues[j].split(":")[1].trim();
				payloadObject.put(key, value);
			}
			payloadArray.add(payloadObject);
		}
		return payloadArray;
	}

	/**
	 * Function to validate order details-------------Test Case Description.
	 * 
	 * @param --------In case if we are sending any parameters in test file(eg. any
	 *                   constant)
	 * 
	 * @author Priyesh Thakur
	 */
	public void validateOrderDetailsWithDB() throws Exception {
		logger.info("Entering OrderDetailsAPIActionBucket.validateOrderDetailsWithDB");

		String sEnvironment = System.getProperty("Environment").trim();
		sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) ? sEnvironment
				: RunConfig.getProperty(Constants.Environment);
		hmConfig.get(Constants.EXCELHEADEREndPointURL + "_" + sEnvironment);

		JsonUtil jsonUtil = null;
		try {
			GetOrderDetailsFromDB gdb = new GetOrderDetailsFromDB("ODSD01", sheetName.ORDERDETAILS.value);

			CommonTestData ctd = new CommonTestData(hmTestData, hmConfig, objAPIAssertion);

			String countryCodeForDB = gdb.getCountryCodeForDB(hmTestData.get(Constants.KEY_IMCOUNTRYCODE));
			orderDetailsDataFromDB = gdb.getOrderDetailsData(countryCodeForDB, OrderStatusImpulse.SHIPPED.value);
			ingramOrderNumber = orderDetailsDataFromDB.get(OrderDetailsDBFields.ORDERBRANCH.value) + "-"
					+ orderDetailsDataFromDB.get(OrderDetailsDBFields.ORDERNUMBER.value);

			jsonUtil = new JsonUtil(sEnvironment, hmTestData, hmConfig);

			// Provide Endpoint
			String endpointHost = ctd.getAPIEndPointWithOrderId(ingramOrderNumber);
			String arrUpdateParam[] = { endpointHost, " " };

			JSONObject objJSON = new JSONObject();


			// Get response for the details sent to httpRequest
			Response response = jsonUtil.call("get", true, arrUpdateParam, objJSON, ctd.getHeaders(), ctd.getParameters());

			// Validate response
			objAPIAssertion.assertHardEquals(String.valueOf(response.getStatusCode()),
					hmTestData.get(Constants.STATUS_CODE), "Status Code Validation");

			// Validate data from response
			// Create a class to return values from json response using jsonpath
			String orderStatusAPI = jsonUtil.getValueFromJSONResponse("lines[0].lineStatus");
			String orderStatusDB = orderDetailsDataFromDB.get(OrderDetailsDBFields.SHIPMENTSTATUSDESC.value).trim();
			objAPIAssertion.assertEquals(orderStatusAPI, orderStatusDB, "Order Status validation");

			String billToCityAPI = jsonUtil.getValueFromJSONResponse("billToInfo.city");
			String billToCityDB = orderDetailsDataFromDB.get(OrderDetailsDBFields.BILLTOCITY.value).trim();
			objAPIAssertion.assertEquals(billToCityAPI, billToCityDB, "Order Type validation");

		} catch (Exception e) {
			String sErrMessage = "FAIL: validateOrderDetails() method of OrderDetailsAPIActionBucket: "
					+ Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage, e);
		} finally {
			String sTestCaseName = (String) Reporter.getCurrentTestResult().getTestContext()
					.getAttribute(AppUtil.METHOD + Thread.currentThread().getId());
			OrderCreatePOCDynamicAPITest.lstResultSet.add(
					new Object[] { hmTestData.get(Constants.EXCELHEADER_SRNO), sTestCaseName, ingramOrderNumber, "" });
		}
	}

	// String customerNumber =
	// orderDetailsDataFromDB.get(OrderDetailsDBFields.ORDERBRANCH.value)+"-"+orderDetailsDataFromDB.get(OrderDetailsDBFields.CUSTOMERNUMBER.value);

	/**
	 * Function to get expected error message as per invalid/blank data or incorrect
	 * format
	 * 
	 * @return expected error message for validation
	 * 
	 * @author Priyesh Thakur
	 */
	public String getExpectedErrorMessageAsPerInput() {
		logger.info("Entering OrderDetailsAPIActionBucket.getExpectedErrorMessageAsPerInput");
		String expectedMessage = "";

		if (hmTestData.get(Constants.KEY_TESTDATACATEGORY).contains("Invalid")) {
			if (hmTestData.get(Constants.KEY_FIELDFORVALIDATION).equals(Constants.KEY_IMCOUNTRYCODE)) {
				expectedMessage = Constants.ERRORCODE_FORINVALIDFIELD.replace("FieldName",
						hmTestData.get(Constants.KEY_FIELDFORVALIDATION));
			} else {
				expectedMessage = Constants.ERRORCODE_FORINVALIDORDER;
			}
		}

		if (hmTestData.get(Constants.KEY_TESTDATACATEGORY).contains("Blank")) {
			expectedMessage = Constants.ERRORCODE_FORBLANKFIELD.replace("FieldName",
					hmTestData.get(Constants.KEY_FIELDFORVALIDATION));
		}

		if (hmTestData.get(Constants.KEY_TESTDATACATEGORY).contains("IncorrectFormat")) {
			if (hmTestData.get(Constants.KEY_FIELDFORVALIDATION).equals(Constants.KEY_INGRAMORDERNUM)) {
				expectedMessage = Constants.ERRORCODE_INCORRECTFORMATORDERNO;
			} else if (hmTestData.get(Constants.KEY_FIELDFORVALIDATION).equals(Constants.KEY_IMCUSTOMERNUM)) {
				expectedMessage = Constants.ERRORCODE_INCORRECTFORMATCUSTNO;
			}
		}

		return expectedMessage;
	}
}