package com.im.b2b.api.business;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D.*/
import org.apache.log4j.Logger;

import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.utility.ReadExcelFile;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class MapTCForNations {
	static Logger logger = Logger.getLogger("MapTCForNations");
	public static final String EXH_TC="Test Script", EXH_TC_ID="TC", EXH_EXE_YES="Y", EXH_EXE_NO="N";
	public static final int COUNTRYCODELENGTH=2;
	public static final String EXCEL_TestCaseVSCoutnry=Constants.BASEPATH+RunConfig.getProperty(Constants.EXCELNAME_TESTCASECOUNTRY_MAP).trim();

	private String TC_ID=null, TCName=null;
	HashMap<String,Boolean> hmCountryAvailability= null;

	public MapTCForNations(String pTCName, HashMap<String,Boolean> pCountryAvailability) {
		TCName=pTCName;
		hmCountryAvailability=pCountryAvailability;
	}

	
	@Override
	public String toString() {
		return "MapTCForNations [TC_ID=" + TC_ID + ", TCName=" + TCName + ", hmCountryAvailability="
				+ hmCountryAvailability + "]";
	}


	public void setCountryAvailability(String sCountryCode, boolean pBValue) {
		hmCountryAvailability.put(sCountryCode, pBValue);
	}

	public boolean isCountryEabledForExecution(String sCountryCode) {
		if(hmCountryAvailability.containsKey(sCountryCode))
			return hmCountryAvailability.get(sCountryCode);
		else return false;
	}


	public synchronized static MapTCForNations prepareCountryApplicabilityForTestcase(HashMap<String,String> pHMSingleTestEntry) {
		Set<String> ExcelHeaders=pHMSingleTestEntry.keySet();
		MapTCForNations mapTC = null;mapTC = new MapTCForNations(null, new HashMap<String,Boolean>());

		for(String sKey : ExcelHeaders) {
		//Sumeet changed from false to true for API Testing	
			boolean bCountryValue=true;
			final String CountryCodes="COUNTRYCODES"  ;
			//Sumeet for API bCountryValue= sKey.length()==COUNTRYCODELENGTH;
			switch(sKey) {
			case EXH_TC: mapTC.setTCName(pHMSingleTestEntry.get(sKey)); break;
			case EXH_TC_ID: mapTC.setTC_ID(pHMSingleTestEntry.get(sKey)); break;
			default: if(bCountryValue) {
				String sVal = pHMSingleTestEntry.get(sKey);
				if(null!=sVal) {
					boolean bCountryNeedToExecute= sVal.startsWith(EXH_EXE_YES);

					mapTC.setCountryAvailability(sKey, bCountryNeedToExecute);

				}else {
					String sErr="ERROR: The vlue for the givwn key["+sKey+"] is null.";
					mapTC.setCountryAvailability(sKey, false);
					logger.error(sErr);
					System.out.println(sErr);
				}
			}

			}
		}

		return mapTC;
	}

	public String getTC_ID() {
		return TC_ID;
	}

	public void setTC_ID(String tC_ID) {
		TC_ID = tC_ID;
	}

	public String getTCName() {
		return TCName;
	}

	public void setTCName(String tCName) {
		TCName = tCName;
	}

	public static HashMap<String,MapTCForNations> getListOfTestvsCountryMap(String psWorkBookName) throws Exception {
		ReadExcelFile readExcel = new ReadExcelFile();
		List<HashMap<String,String>> listTestEntry = readExcel.readExcelDataToListofHashMap(EXCEL_TestCaseVSCoutnry, psWorkBookName);
		HashMap<String,MapTCForNations> hmTCMappingToCountry = new HashMap<String,MapTCForNations>  ();

		for(HashMap<String,String> kp : listTestEntry) {
			MapTCForNations objTCEntry = MapTCForNations.prepareCountryApplicabilityForTestcase(kp);
			hmTCMappingToCountry.put(objTCEntry.getTCName(), objTCEntry);
			System.out.println("The data["+objTCEntry+"]");
		}	


		return hmTCMappingToCountry;

	}

	public static void main(String args[]) throws Exception {	
		String sWorkBook="Smoke";
		getListOfTestvsCountryMap(sWorkBook);
	}



}
