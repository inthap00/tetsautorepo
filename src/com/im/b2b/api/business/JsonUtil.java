package com.im.b2b.api.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.wrapper.api.APIAssertion;
import com.jayway.jsonpath.JsonPath;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JsonUtil implements Cloneable{

	Logger logger = Logger.getLogger("JsonUtil");
	APIAssertion objAPIAssertion = null;
	RequestSpecification httpRequest=null;
	Response objResponse = null;
	HashMap<String, String> excelHMap=null;
	HashMap<String, String> configHMap=null;
	String sRequestBody = null;
	int iStatusCode=0;
	
	public Object clone() throws CloneNotSupportedException 
	{ 
		return super.clone(); 
	} 
	
	public JsonUtil(String pRequestBody, HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap) {		
		sRequestBody=pRequestBody;
		excelHMap=pExcelHMap;
		configHMap = pConfigHMap;		
	}
	
	public String getsRequestBody() {
		return sRequestBody;
	}

	public JsonUtil(String pRequestBody, HashMap<String, String> pExcelHMap) {		
		sRequestBody=pRequestBody;
		excelHMap=pExcelHMap;
		configHMap = AppUtil.getCredentialData(pExcelHMap);		
	}
	
	
	public JsonUtil() {
		// TODO Auto-generated constructor stub
	}
	
	public int getStatusCode() {
		return iStatusCode;
	}
	
	public Response getResponse() {
		return objResponse;
	}
	
	/*
	 * sXpath will be like as follow:
	 * Example: 
	 * For Single set: 
	 * For multi set: responses.hits.hits[0]._source.ordersubtotalvalue[1]
	 * 
	 * 
	 */
	public synchronized String getActualValueFromJSONResponse(String sXpath) throws Exception {
		String sActualValue="";
		if(objResponse==null || sXpath==null)
			throw new Exception("Inputs to getActualValueFromJSONResponse having null values for Response or Xpath"); 
		sActualValue = objResponse.jsonPath().getString(sXpath).trim();

		if(!(null==sActualValue || sActualValue.equalsIgnoreCase(""))) {
			int len = sActualValue.length();
			sActualValue=sActualValue.substring(1, len-1);
		}else {
			sActualValue="Actual Value is Null";
		}
		return sActualValue;
	}
	
	public synchronized String getActualValueFromJSONResponseWithoutModify(String sXpath) throws Exception {
		String sActualValue="";
		
		try {
		if(objResponse==null || sXpath==null)
			throw new Exception("Inputs to getActualValueFromJSONResponse having null values for Response or Xpath"); 
		
		sActualValue = objResponse.jsonPath().getString(sXpath).trim();

		}catch(NullPointerException e) {
			sActualValue = "NULL";
			// Apply check for NULL value subsequent method using this // added by Prashant N
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return sActualValue;
	}
	
	/*
	 * sXpath will be like as follow:
	 * Example: responses.hits.hits._source[0]
	 * 
	 */
	
	public synchronized  List<HashMap<String, String>> getListofHashMapFromJSONResponse(String sXpath) throws Exception{
		List<HashMap<String, String>> lstOfDataFromResponse =new ArrayList<HashMap<String,String>>();
		try{
		
		if(objResponse==null || sXpath==null)
			throw new Exception("Inputs to getActualValueFromJSONResponse having null values for Response or Xpath");
		
		lstOfDataFromResponse = objResponse.jsonPath().getList(sXpath.trim());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return lstOfDataFromResponse;
	}
	
	public synchronized  <E> List<E> getListofStringsFromJSONResponse(String sXpath) throws Exception{
		
		List<E> lstOfDataFromResponse =new ArrayList<E>();
		if(objResponse==null || sXpath==null)			
			throw new Exception("Inputs to getActualValueFromJSONResponse having null values for Response or Xpath");
		
		lstOfDataFromResponse = objResponse.jsonPath().getList(sXpath.trim());
				
		return lstOfDataFromResponse;		
		
	}
	
	public synchronized  HashMap<Object, Object> getMapFromJSONResponse(String sXpath) throws Exception{
		
		if(objResponse==null || sXpath==null)
			throw new Exception("Inputs to getMapFromJSONResponse having null values for Response or Xpath");
		
		HashMap<Object, Object> lstOfDataFromResponse = (HashMap<Object, Object>) objResponse.jsonPath().getMap(sXpath.trim());
		
		return lstOfDataFromResponse;
	}
	
	
	public String countTheOccuranceOfSpecifiedField(String sub) {
		int count = 0;
		String source = objResponse.asString();

		for (int i = 0; (i = source.indexOf(sub, i)) != -1; i += sub.length()) {
			++count;
		}
		return count+"";

	}
	
	public static void main(String[] args) throws Exception {		
		
	}

	public List<Object> getResponseFromJayway(JsonUtil objJSONUtil, String xpathvalue) {
		return JsonPath.read(objJSONUtil.getResponse().asString(), xpathvalue);
	}
	
	
	/**
	 * Function to add headers to httpRequest
	 * 
	 * @param header HashMap<String, String> - Having key value pair of headers
	 * @author Priyesh Thakur
	 */
	@SuppressWarnings("rawtypes")
	public void addHeader(HashMap<String, String> header) {
		// Add a header
		if (!header.isEmpty()) {
			Iterator it = header.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				httpRequest.header(pair.getKey().toString(), pair.getValue().toString());
				it.remove(); // avoids a ConcurrentModificationException
			}
		}
	}

	/**
	 * Function to add parameters to httpRequest
	 * 
	 * @param formParams HashMap<String, String> - Having key value pair of Form
	 *                   Parameters
	 * @return true if form parameters set successfully, else return false
	 * @author Priyesh Thakur
	 */
	@SuppressWarnings("rawtypes")
	public boolean addBodyFormParams(HashMap<String, String> formParams) {
		// Add form params
		boolean formParamSet = false;
		if (!formParams.isEmpty()) {
			Iterator it = formParams.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				httpRequest.formParam(pair.getKey().toString(), pair.getValue().toString());
				it.remove(); // avoids a ConcurrentModificationException
			}
			formParamSet = true;
		}
		return formParamSet;
	}

	/**
	 * Function to make an REST api call and get the response and status code.
	 * To be used when we need to pass JSON object as request body or Form Parameters as body with headers
	 * 
	 * @param strVerb    API method or action to be performed
	 * 					 Value can be "put","post","get","delete", "head",
	 *                   "options", "patch"
	 * @param arrParam   Array of Strings: 1st array element - baseURI E.g.,
	 *                   "http://www.test.com/api/myproject/asset", 2nd array
	 *                   element would be query parameters - Method E.g., "?search=testOrder", "?env=QA"
	 * @param objJSON    JSON body object of the type JSONObject
	 * @param header     HashMap<String, String> - Having key value pair of headers
	 * @param formParams HashMap<String, String> - Having key value pair of form
	 *                   parameters
	 * @return Response Body of the type Response
	 * @author Priyesh Thakur
	 */
	public Response call(String strVerb, boolean auth, String arrParam[], JSONObject objJSON, HashMap<String, String> header,
			HashMap<String, String> formParams) {

		try {
			RestAssured.baseURI = arrParam[0];
			httpRequest = RestAssured.given().relaxedHTTPSValidation().given();
			if(auth) {
				String sEnvironment = System.getProperty("Environment").trim();
				sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE)) ? sEnvironment
						: RunConfig.getProperty(Constants.Environment);
				String sUserName = configHMap.get("UserName" + "_" + sEnvironment);
				String sPassword = configHMap.get("Password" + "_" + sEnvironment);
				httpRequest.auth().preemptive().basic(sUserName, sPassword);
			} else {
				httpRequest.auth().none();
			}
				
			JSONObject jsonBody = objJSON;
			
			addHeader(header); 
			addBodyFormParams(formParams);
			
			if(!(jsonBody.length()==0)) {
				httpRequest.body(jsonBody.toString());
			}
			
			// Post the request and return the response
			Response response = makeCall(strVerb, arrParam[1]);

			logger.info(strVerb + " call - " + arrParam[0] + arrParam[1] + ", JSON: " + objJSON + ", Status: "
					+ response.getStatusLine() + ". Response Time - " + response.time());
			
			objResponse = response; 
			iStatusCode = response.getStatusCode();
			 
			return response;

		} catch (Exception e) {
			logger.info(strVerb + " call - " + arrParam[0] + arrParam[1] + ", JSON: " + objJSON + ", Exception: "
					+ e.getMessage());
		}
		return null;
	}

	/**
	 * Function to return response based on the inputs provided for the request
	 * @param strVerb        Value can be "put","post","get","delete", "head",
	 *                       "options", "patch"
	 * @param strQueryParams query parameters - Method E.g., "?search=testOrder", "?env=QA"
	 * @return Response Body of the type Response
	 * @author Priyesh Thakur
	 */
	public Response makeCall(String strVerb, String strQueryParams) {

		Response response = null;
		switch (strVerb) {
		case "get":
			response = httpRequest.get(strQueryParams);
			break;
		case "put":
			response = httpRequest.put(strQueryParams);
			break;
		case "delete":
			response = httpRequest.delete(strQueryParams);
			break;
		case "post":
			response = httpRequest.post(strQueryParams);
			break;
		case "head":
			response = httpRequest.head(strQueryParams);
			break;
		case "options":
			response = httpRequest.options(strQueryParams);
			break;
		case "patch":
			response = httpRequest.patch(strQueryParams);
			break;
		default:
			logger.assertLog(false, "Invalid parameter passed to RestAPI-call function. Parameter: " + strVerb);
		}
		return response;
	}
	
	/** Function to get a value from a response using json path
	 * @param sXpath json path to get the exact data
	 * Example: 
	 * For Single set: billToInfo.companyName
	 * For multi set: lines[0].partDescription
	 * @author Priyesh Thakur
	 */
	public synchronized String getValueFromJSONResponse(String sXpath) throws Exception {
		String sActualValue="";
		if(objResponse==null || sXpath==null) {
			throw new Exception("Inputs to getActualValueFromJSONResponse having null values for Response or Xpath"); 
		}else {
			sActualValue = objResponse.jsonPath().getString(sXpath);
		}
		return sActualValue;
	}
	
	/** Function to get a value from a response using json path
	 * @param sXpath json path to get the exact data
	 * Example: 
	 * For Single set: billToInfo.companyName
	 * For multi set: lines[0].partDescription
	 * @author Priyesh Thakur
	 */
	public synchronized String getValueFromJSONResponse(Response response, String sXpath) throws Exception {
		String sActualValue="";
		if(response==null || sXpath==null) {
			throw new Exception("Inputs to getActualValueFromJSONResponse having null values for Response or Xpath"); 
		}else {
			sActualValue = response.jsonPath().getString(sXpath);
		}
		return sActualValue;
	}
}

