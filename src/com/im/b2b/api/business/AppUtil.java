package com.im.b2b.api.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.testng.ITestContext;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.api.common.Util;
import com.im.utility.ReadExcelFile;
import com.im.wrapper.api.APIDriver;

import com.im.wrapper.api.ToolAPI;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.json.*;
import java.io.*;
import java.text.SimpleDateFormat;

public class AppUtil {
	static Logger logger = Logger.getLogger("AppUtil");
	public final static String HDRIVER="HDriver", COUNTRY="Country", BROWSER="BROWSER",METHOD="Method",HTTPREQUEST="HTTPRequest";
	private static final String MUTISET="_MultiSet",START="@start", END="@end";


	public static  APIDriver preparePrequisites(ITestContext oIContext, HashMap<String, String> configHMap) throws InterruptedException {

		String sCountryCode = configHMap.get(Constants.ExcelHeaderRunConfig);
		String sMethodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		APIDriver objAPIDriver=ToolAPI.getAPIDriver( sMethodName, sCountryCode);
		String sThreadID = ""+Thread.currentThread().getId();
		oIContext.setAttribute(HDRIVER+sThreadID, objAPIDriver);
		oIContext.setAttribute(COUNTRY+sThreadID, sCountryCode);		
		oIContext.setAttribute(METHOD+sThreadID, sMethodName);

		return objAPIDriver;
	}

	/*****************
	 * 
		@author Sumeet Umalkar
		Added : "\\b" on 16th dec 2019: to handle the exact replacement on key on contains
		Eg: @branch_nbr and @branch_nbr_ship_from here it will replace only @branch_nbr  
	 */

	public  static String getFormatedContentStringForRestRequest(HashMap<String, String> phmTestData, String sContent) {
		for(Map.Entry<String, String> entry : phmTestData.entrySet()) {
			String sKey = entry.getKey();
			String sValue=phmTestData.get(sKey);
			if(!(null==sValue||sValue.equalsIgnoreCase(Constants.BLANKVALUE) || sValue.equals("")))
				sContent=sContent.replaceAll("@\\b"+sKey+"\\b", phmTestData.get(sKey));
			else
				//sContent=sContent.replaceFirst("@"+sKey, "\"\"");  //For value <<BLANK>> it's doing value as = ""  ""
				sContent=sContent.replaceFirst("@\\b"+sKey+"\\b", "");	//It's working as expected
		}

		return sContent;
	}


	private  static String getFormatedContentStringForSOAPRequest(HashMap<String, String> phmTestData, String sContent) throws SAXException, IOException {
		//ArrayList<String> arrLstsContent= new ArrayList<>(); 
		for(Map.Entry<String, String> entry : phmTestData.entrySet()) {
			String sKey = entry.getKey();
			String sValue=phmTestData.get(sKey);
			if(!(null==sValue||sValue.equalsIgnoreCase(Constants.BLANKVALUE) || sValue.equals("")))
				//sContent=sContent.replaceAll("@"+sKey, phmTestData.get(sKey));
				sContent=sContent.replaceAll("@\\b"+sKey+"\\b", phmTestData.get(sKey));

			else
				//	sContent=sContent.replaceFirst("@"+sKey, "");
				sContent=sContent.replaceFirst("@\\b"+sKey+"\\b", "");

		}

		/* DO NOT DELETE THIS COMMENTED CODE Sumeet
		 * will be needing if the @value is mentioned in xml but not in testdata sheet
		 * In-Complete logic need to work on this. 
		 * 
		 * DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is;
		 try {
				builder = factory.newDocumentBuilder();

         is = new InputSource(new StringReader(sContent));
         Document doc = builder.parse(is);
         NodeList list = doc.getElementsByTagName("*");


         for(int i=0 ; i<list.getLength();i++)

         System.out.println(list.item(i).getTextContent());

	        } catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 *End of @Value logic
		 */

		return sContent;
	}

	public static ArrayList<String> getMultiSetRequestBody(String sFileName) throws Exception {
		ArrayList<String> arrLstsContent= new ArrayList<>(); 

		//TODO

		return arrLstsContent;
	} 	

	public static String getRequestFileRawData(String sFileName) throws IOException { 		  
		File file = new File(System.getProperty("user.dir")+"\\APITemplate\\"+sFileName+".txt");
		FileInputStream fis = new FileInputStream(file);
		InputStreamReader isr = new InputStreamReader(fis);
		BufferedReader br = new BufferedReader(isr);

		String sLine,sContent="";
		while((sLine = br.readLine()) != null){
			sContent=sContent+sLine;	
		}
		br.close();

		return sContent;
	}

	public  static String getRequestBodyForSOAPRequest(String sFileName, HashMap<String, String> phmTestData,boolean bIsMultiSet, String sExcelTestData) throws Exception {

		String sRequestBody=null;

		if(bIsMultiSet) {
			sRequestBody = getMultiSetRequestBodyForSOAP(sFileName, phmTestData, sExcelTestData);
		}else {
			sRequestBody = getRequestBodyForDefinedTestDataSOAP(sFileName,phmTestData);
		}

		return sRequestBody;
	}

	// Return multiple set request body of type SOAP 
	public static String getMultiSetRequestBodyForSOAP(String sFileName, HashMap<String, String> phmTestData, String sTestDataFileName) throws Exception {
		String sRequestPayload = null;
		String sPart1 , sMultisetBody , sSet , sPart2 ;
		String sContentOfRawFile = getRequestFileRawData(sFileName);	

		try {
			List<HashMap<String,String>> multisetDataDrivenMap = getMultiSetDataAsPerCategory(phmTestData, sTestDataFileName);

			String sRequestBodyContent = sContentOfRawFile;
			if(sRequestBodyContent.contains(START) ) {
				int iCount = StringUtils.countMatches(sRequestBodyContent, START);

				// To Update data for all the sets of particular category
				for(int i=0; i<iCount; i++) {
					int startIndex = sRequestBodyContent.indexOf(START);
					int endIndex = sRequestBodyContent.indexOf(END);

					sPart1 = sRequestBodyContent.substring(0, startIndex);
					sPart2 = sRequestBodyContent.substring(endIndex+4);
					sSet = sRequestBodyContent.substring(startIndex + 6, endIndex);

					// Fetching multiple set request with given test data category 
					sMultisetBody = getPayloadOfMultisetDataOfSoap(multisetDataDrivenMap, sSet);

					if(sMultisetBody!=null)
						sRequestBodyContent = sPart1 + sMultisetBody+ sPart2;
					else
						throw new Exception("Test data return from the multiple set is null");
				}					

				// Update common data in request body			
				sRequestPayload = getFormatedContentStringForSOAPRequest(phmTestData, sRequestBodyContent);
			}

			else
				throw new Exception("Request body is incorrect. Tagging for multiset is missing");
		}
		catch(Exception e) {
			e.printStackTrace();
		}


		return sRequestPayload;
	} 	

	// returns the data for the given set
	public static String getPayloadOfMultisetDataOfSoap(List<HashMap<String, String>> lstHashMapValidData, String sSet) throws Exception {
		String sBodyWithData = null, sContent1="";
		try {

			if(lstHashMapValidData.size()==0) { System.out.println("[DEBUG]: Valida data is not present for the category in multiple set sheet"); return null;}
			for(HashMap<String,String> hmapApplicableMultiset: lstHashMapValidData)
			{	
				sBodyWithData = getFormatedContentStringForSOAPRequest(hmapApplicableMultiset, sSet);
				sContent1 = sContent1 + sBodyWithData;
			}		
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return sContent1;
	}

	private static String getRequestBodyForDefinedTestDataSOAP(String sFileName, HashMap<String, String> phmTestData) throws Exception {

		String sContent = getRequestFileRawData(sFileName);	
		String sRequestBody=null;
		sRequestBody=getFormatedContentStringForSOAPRequest(phmTestData, sContent);
		return sRequestBody;
	}	



	public static synchronized String getRequestBodyForRestRequest(String sFileName,
			HashMap<String, String> phmTestData, boolean ismultiset, String sTestDataFileName) throws IOException {

		String sRequestBody=null;
		if(ismultiset) {
			sRequestBody = getRequestBodyForMultisetDataREST(sFileName,phmTestData, sTestDataFileName);
		}else {
			sRequestBody = getRequestBodyForDefinedTestDataREST(sFileName,phmTestData);
		}
		System.out.println("******* Request For ["+phmTestData.get(Constants.ExcelHeaderRunConfig)+"] *******\n"+sRequestBody);
		return sRequestBody;
	}

	public static synchronized String getRequestBodyWithoutNullValues(String text) {
		// JSONObject objJson = new JSONObject(text);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(text);
		String prettyJsonString = gson.toJson(je);
		String[] arr = prettyJsonString.toString().split("\n"); // every arr items is a line now.
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= arr.length - 1; i++) {
			if (arr[i].contains("REMOVEKEY")) {
				if (arr[i + 1].contains("}")) {
					if (String.valueOf(sb.toString().charAt(sb.toString().length() - 1)).equals(",")) {
						String newStr = sb.toString().substring(0, sb.toString().length() - 1);
						sb = new StringBuilder(newStr);
					}
				}
			} else {
				sb.append(arr[i]);
			}
		}
		return sb.toString(); // new file that does not contains that lines.
	}
	
	public static String getRequestBodyForMultisetDataREST(String sFileName, HashMap<String, String> phmTestData, String sTestDataFileName) {
		String sRequestPayload = null;
		String sPart1 , sMultisetBody , sSet , sPart2 ;
		try {
			List<HashMap<String,String>> multisetDataDrivenMap = getMultiSetDataAsPerCategory(phmTestData, sTestDataFileName);
			String sRequestBodyContent = getRequestFileRawData(sFileName);
			if(sRequestBodyContent.contains(START) ) {
				int iCount = StringUtils.countMatches(sRequestBodyContent, START);

				// To Update data for all the sets of particular category
				for(int i=0; i<iCount; i++) {
					int startIndex = sRequestBodyContent.indexOf(START);
					int endIndex = sRequestBodyContent.indexOf(END);

					sPart1 = sRequestBodyContent.substring(0, startIndex);
					sPart2 = sRequestBodyContent.substring(endIndex+4);
					sSet = sRequestBodyContent.substring(startIndex + 6, endIndex);

					// Fetching multiple set request with given test data category 
					sMultisetBody = getPayloadWithMultisetDataOfREST(multisetDataDrivenMap, sSet);

					if(sMultisetBody!=null)
						sRequestBodyContent = sPart1 + sMultisetBody+ sPart2;
					else
						throw new Exception("Test data return from tghe multiple set is null");
				}					

				// Update common data in request body			
				sRequestPayload = getFormatedContentStringForRestRequest(phmTestData, sRequestBodyContent);
			}

			else
				throw new Exception("Request body is incorrect. Tagging for multiset is missing");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return sRequestPayload;
	}

	public static String getPayloadWithMultisetDataOfREST(List<HashMap<String, String>> lstHashMapValidData, String sSet) throws Exception {
		String sBodyWithData = "", sContent1=sSet, setWithData="";
		try {
			if(lstHashMapValidData.size()==0) { System.out.println("[DEBUG]: Valida data is not present for the category in multiple set sheet"); return null;}
			int index=0;
			for(HashMap<String,String> hmapApplicableMultiset: lstHashMapValidData)
			{	
				if(index!=0) 
					sBodyWithData= sBodyWithData+",";
				setWithData = getFormatedContentStringForRestRequest(hmapApplicableMultiset, sContent1);
				sBodyWithData = sBodyWithData + setWithData;
				index++;
			}		
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return sBodyWithData;
	}

	private static String getRequestBodyForDefinedTestDataREST(String sFileName, HashMap<String, String> phmTestData) throws IOException {
		String sContent = getRequestFileRawData(sFileName);	
		String sRequestBody=null;
		sRequestBody=getFormatedContentStringForRestRequest(phmTestData, sContent);
		return sRequestBody;
	}


	public synchronized static List<HashMap<String, String>> getMultiSetDataAsPerCategory(HashMap<String, String> hSingleSetData, String sTestDataFileName) throws Exception {
		String sEnvrnName = RunConfig.getProperty("Environment");		
		List<HashMap<String, String>> mapOfMultiSetData = new ArrayList<>();
		String sCountry = hSingleSetData.get(Constants.ExcelHeaderRunConfig);
		String sTestDataCategory = hSingleSetData.get(Constants.EXCELHEADER_TESTDATACATEGORY);
		try {
			List<HashMap<String,String>> mapMultipleSetOfTestData = new ReadExcelFile().readExcelDataToListofHashMap(Constants.getCurrentProjectPath()
					+"\\TestData\\"+sTestDataFileName, sEnvrnName+MUTISET );


			for(HashMap<String,String> hmapApplicableMultiset: mapMultipleSetOfTestData) {			
				if((sCountry.trim().equalsIgnoreCase(hmapApplicableMultiset.get(Constants.ExcelHeaderRunConfig).trim()) &&
						hmapApplicableMultiset.get(Constants.EXCELHEADER_TESTDATACATEGORY).trim().equalsIgnoreCase(sTestDataCategory)))					
					mapOfMultiSetData.add(hmapApplicableMultiset);}
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
		return mapOfMultiSetData;
	}

	/**
	 * This function will be useful to generate config data from the QA sheet of repository
	 * @param hmTestData
	 * @ Added by Prashant Navkudkar
	 * Sumeet : Added logic to get the Environment from POM.xl when ran through jenkins 
	 */
	public static synchronized HashMap<String, String> getCredentialData(HashMap<String, String> hmTestData) {
		HashMap<String, String> mapForConfigData = new HashMap<>();
		try {
			String sEnvironment=System.getProperty("Environment").trim();
			sEnvironment = (!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sEnvironment:RunConfig.getProperty(Constants.Environment);

			String sUrlKey = Constants.EXCELHEADEREndPointURL+"_"+sEnvironment;
			String sUserNameKey = "UserName"+"_"+sEnvironment;
			String sPasswqordKey = "Password"+"_"+sEnvironment;

			mapForConfigData.put(sUrlKey, hmTestData.get(sUrlKey));
			mapForConfigData.put(sUserNameKey, hmTestData.get(sUserNameKey));
			mapForConfigData.put("Password"+"_"+sEnvironment, hmTestData.get(sPasswqordKey));
			mapForConfigData.put(Constants.ExcelHeaderRunConfig,hmTestData.get(Constants.ExcelHeaderRunConfig));

		} catch (Exception e) {
			logger.error("Error in  the function getCredentialData of AppUtil ");
			e.printStackTrace();
		}
		return mapForConfigData;		
	}

	/**
	 * This function will add the attributes not present is given map but exist in Request Body with <<BLANK>> Value
	 * @param hmTestData of excel data
	 * @return HashMap  Adding <<BLANK>> Value with attribute tag not present in given map
	 * @author usumas00
	 * @param isMultiSet --> This logic is not yet developed, need to enhance this for multiset request
	 */
	public static synchronized HashMap<String, String> getMapWithBlankDataWhichIsNotPresentInTestDataExcel(String pRequestTemplateNm,HashMap<String, String> mapReqDataForTemplate, boolean isMultiSet) {

		HashMap<String, String> mapToBeReturn = new HashMap<String,String>();
		int indexOfAT = 0,indexToBeConsidered=0;
		String sNewTempString = null;
		long Count = 0;
		try {
			String sRequestBody = getRequestFileRawData(pRequestTemplateNm);		
			mapToBeReturn.putAll(mapReqDataForTemplate);

			Set<String> sKeySet = mapToBeReturn.keySet(); //get all the columns from test data excel		
			Count =	sRequestBody.chars().filter(c -> c == '@').count();		
			indexOfAT = sRequestBody.indexOf("@"); //Search the first Index of @ from request body
			
			for(int i=0;i<Count ; i++) {						
						
				indexToBeConsidered = indexToBeConsider(sRequestBody,indexOfAT);						
																		
				sNewTempString = sRequestBody.substring(indexOfAT+1, indexToBeConsidered); // get the attribute name from request body like "@invoicenumber" returns invoicenumber
				indexOfAT = sRequestBody.indexOf("@",indexOfAT+1); //Gets the next index of @ as the first took from outside the loop

				if(!sKeySet.contains(sNewTempString)) // Checking if the attribute name passed from excel if not present add it to map with value as <<BLANK>>
					mapToBeReturn.put(sNewTempString, Constants.BLANKVALUE);

			}

		} catch (Exception e) {
			logger.error("Error in  the function getMapWithBlankDataWhichIsNotPresentInTestDataExcel of AppUtil ");
			e.printStackTrace();
		}

		return mapToBeReturn;

	}
	
	/**
	 * This Method will analyze " ] } indexes of get close to each @ attribute and return its index 
	 * -1 index is handled for all the 3 characters 
	 * @param sRequestBody
	 * @param indexOfAT
	 * @return Next index of " ] } which ever is earliest 
	 * @author usumas00
	 */
	
	private static int indexToBeConsider(String sRequestBody, int indexOfAT) {
		int indexOfInvertedComma=0,indexOfSquareBracket=0,indexOfCurlyBracket=0;
		List<Integer> lstRetunValue = new ArrayList<>();
		
		try {
		indexOfInvertedComma = sRequestBody.indexOf("\"", indexOfAT+1);
		indexOfSquareBracket = sRequestBody.indexOf("]", indexOfAT+1);
		indexOfCurlyBracket = sRequestBody.indexOf("}", indexOfAT+1);
		
		if(indexOfInvertedComma > 0)
			lstRetunValue.add(indexOfInvertedComma);
		
		if(indexOfSquareBracket > 0)
			lstRetunValue.add(indexOfSquareBracket);
		
		if(indexOfCurlyBracket > 0)
			lstRetunValue.add(indexOfCurlyBracket);
		
		Collections.sort(lstRetunValue);
		}catch (Exception e) {
			logger.error("Error in  the function indexToBeConsider of AppUtil ");
			e.printStackTrace();
		}
		
		return lstRetunValue.get(0);
	}

	/**
	 * @author usumas00
	 * @param sCountryCode
	 * @param sFileName
	 * @return CountryValue as Object
	 * @throws FileNotFoundException
	 */
	private static synchronized Object getTestdataFor(String sCountryCode, String sFileName) {
		try {
			JsonReader jsonReader = Json.createReader(new FileReader(Constants.BASEPATH+"\\DynamicTestData\\"+sFileName+".json"));
			JsonStructure jsonStructure = jsonReader.read();
			JsonPointer jsonPointer1 = Json.createPointer(sCountryCode);
			Object jsonString = (Object)jsonPointer1.getValue(jsonStructure);
			return jsonString;
		}catch(JsonException e) {
			logger.error("Given Country["+sCountryCode+"] is not present in Testdatafile["+sFileName+"] change the value of ExecuteWithOfflineData to \"No\" to capture data for ["+sCountryCode+"]");
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			logger.error("Given File["+sFileName+"] is not found in Testdata Folder[DynamicTestData]");
			e.printStackTrace();
		}
		catch(Exception e) {
			logger.error("Error in function getTestdataFor of AppUtil.java");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @author usumas00
	 * @param sCountryCode
	 * @param pTestDataUtilClassName
	 * @return TestDataUtilObject
	 * @throws FileNotFoundException
	 * 
	 * Example of calling :
	 * (OrderSearchTestDataUtil) AppUtil.getObjectFor(pExcelHMap.get(Constants.ExcelHeaderRunConfig), OrderSearchTestDataUtil.class);
	 */
	
	public static synchronized <T> Object getObjectFor(String sCountryCode, Class<T> pTestDataUtilClassName) throws FileNotFoundException {
		
		Gson gson =  new GsonBuilder().setPrettyPrinting().create();	
		String s = getTestdataFor("/"+sCountryCode,pTestDataUtilClassName.getSimpleName()).toString();
		Object returnObj = gson.fromJson(s, pTestDataUtilClassName);
		return returnObj;
	}
	
	/**
	 * @author usumas00
	 * @param sFileName
	 * @param pTestDataUtilClassName
	 * @return
	 * @throws FileNotFoundException
	 */
	public static synchronized <T> HashMap<String, Object> getEntireTestdataForFile(String sFileName,Class<T> pTestDataUtilClassName) throws FileNotFoundException{
		
		HashMap<String, Object> hmReturnValue = new HashMap<>();
		JsonReader jsonReader = Json.createReader(new FileReader(sFileName));
		JsonStructure jsonStructure = jsonReader.read();
		System.out.println(jsonStructure.toString());
		JSONObject jsonObj = new JSONObject(jsonStructure.toString());
		List<String> lstCountryCodeKeys = new ArrayList<String>(jsonObj.keySet());
		
		for(String sCountryCode : lstCountryCodeKeys) {
			hmReturnValue.put(sCountryCode, getObjectFor(sCountryCode,pTestDataUtilClassName));
		}
		
		return hmReturnValue;
	}
	
	public static void main(String[] args) throws Exception {
		
		/*HashMap<String,Object> hm = getEntireTestdataForFile("D:\\FrameworkIM\\SeleniumFramework\\IngramAutoAPITest\\DynamicTestData\\OrderSearchTestDataUtil.json",OrderSearchTestDataUtil.class);
		
		System.out.println(hm);*/

		/*OrderSearchTestDataUtil s =	(OrderSearchTestDataUtil)AppUtil.getTestdataFor("/CA");
	
		System.out.println(s.getListOfIngramPartNumber());*/
		HashMap<String, String> inputMap=new HashMap<>();
		inputMap.put("gsaflag", "Y");
		inputMap.put("enduserid", "6");
		System.out.println(getAttributeNameAndAttributeValueStructureForMapEntries(inputMap));
		
	}

	
	
	/*takes List<List<HashMap<String, String>>> as the last parameter. 
	the size of this list is equal to number of multiset structures in the request template
	if the list element corresponding to ith multiset structure is empty, that part is eliminated*/
	public static synchronized String getRequestBodyForMultiSetRestRequestWithTestDataUtil(String sFileName,
			HashMap<String, String> phmTestData, List<List<HashMap<String, String>>> mapMultipleSetOfSKUData) {
		String sRequestPayload = null;
		String sPart1 , sMultisetBody , sSet , sPart2 ;
		try {
			List<List<HashMap<String,String>>> multisetDataDrivenMap = mapMultipleSetOfSKUData;
			String sRequestBodyContent = getRequestFileRawData(sFileName);
			if(sRequestBodyContent.contains(START) ) {
				int iCount = StringUtils.countMatches(sRequestBodyContent, START);
				// To Update data for all the sets of particular category
				for(int i=0; i<iCount; i++) {
					int startIndex = sRequestBodyContent.indexOf(START);
					int endIndex = sRequestBodyContent.indexOf(END);

					sPart1 = sRequestBodyContent.substring(0, startIndex);
					sPart2 = sRequestBodyContent.substring(endIndex+4);
					sSet = sRequestBodyContent.substring(startIndex + 6, endIndex);

					// Fetching multiple set request from the passed List for the ith structure in the request
					if(multisetDataDrivenMap.get(i)!=null && !multisetDataDrivenMap.get(i).isEmpty())
						sMultisetBody = getPayloadWithMultisetDataOfREST(multisetDataDrivenMap.get(i), sSet);
					else
						sMultisetBody="";

					sRequestBodyContent = sPart1 + sMultisetBody+ sPart2;
				}					

				// Update common data in request body			
				sRequestPayload = getFormatedContentStringForRestRequest(phmTestData, sRequestBodyContent);
				sRequestPayload=sRequestPayload.replaceAll("\\s+","");
				
				sRequestPayload = sRequestPayload.replace("[,", "[");
				//sRequestPayload = sRequestPayload.contains(",,")?sRequestPayload.replaceAll(",+",","):sRequestPayload;
				sRequestPayload = sRequestPayload.contains(",,")?sRequestPayload.replaceAll(",{2,}", ","):sRequestPayload;
				sRequestPayload = sRequestPayload.contains(",]")?sRequestPayload.replace(",]","]"):sRequestPayload;
			}
			else
				throw new Exception("Request body is incorrect. Tagging for multiset is missing");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return sRequestPayload;
	}
	
	
	/**
	 * @author Vinita Lakhwani
	 * @param inputMap
	 * @return jsonString
	 * @throws Exception
	 *
	 */
	
	public static String getJSONStringForMap(HashMap<String, String> inputMap) throws Exception {
		Gson gsonObj = new Gson();
		String jsonStr = gsonObj.toJson(inputMap);
		return jsonStr;
	}
	
	/**
	 * @author Vinita Lakhwani
	 * @param inputMap
	 * @return comma seperated jsonStrings of the format {"attributename":"key from hashmap","attributevalue":"value against that key"}
	 * @throws Exception
	 *
	 */
	
	public static String getAttributeNameAndAttributeValueStructureForMapEntries(HashMap<String, String> inputMap) throws Exception {
		//HashMap<String, String> outputMap
		String strReturn="";
		
		//List<String> attributeNames= new ArrayList<String>();
		Set<String> attributeNames = inputMap.keySet();
		
		for(String attrName : attributeNames)
		{
			strReturn=strReturn+"{\"attributename\": \""+ attrName+ "\",\"attributevalue\": \""
					+ inputMap.get(attrName)+ "\"},";
		}
		
		return strReturn;
	}
	
	public static String generateAndStorePONumber(String sCountryCode) throws Exception {
		String sPONum = null;
		//String sCountryName = testData.get(HTTPSmokeAPIConstants.RunConfig);
		String sTimeStamp = new SimpleDateFormat("ddHHmmss").format(Calendar.getInstance().getTime());
		String sRandomNo = String.valueOf(Util.getRandomNumberInRange(1, 100));
		sPONum = sCountryCode + sTimeStamp + sRandomNo;
		//Thread.sleep(120000);
		return sPONum;
	}
}//End of Class
