package com.im.b2b.api.business;

public class APIEnumerations {

	public enum ConnectToDatabase{ODS}

	/**
	 * Enum for database name
	 * 
	 * @author Priyesh Thakur
	 */
	public static enum dataBaseName {
		ODS("ODSD01");

		public String value;

		private dataBaseName(String value) {
			this.value = value;
		}
	}
}
