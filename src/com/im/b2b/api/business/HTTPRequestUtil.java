package com.im.b2b.api.business;

import static io.restassured.path.xml.XmlPath.CompatibilityMode.HTML;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.im.api.common.Util;
import com.im.wrapper.api.APIAssertion;
import com.jayway.jsonpath.JsonPath;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class HTTPRequestUtil {
	Logger logger = Logger.getLogger("RestAssuredUtil"); 
	RequestSpecification httpRequest1=null;
	Response objResponse = null;
	Map<String,String> mapCookies1 = null;
	APIAssertion objAPIAssertion = null;
	
	
	public HTTPRequestUtil(RequestSpecification httpRequest, String sBaseURI, Map<String,String> mapCookies) {		
		httpRequest1 = httpRequest;
		 RestAssured.baseURI = sBaseURI;
		 mapCookies1 = mapCookies;
	}
	
	public Response hitGetRequest(Map<String,String> mapHeader, Map<String,String> mapQueryParam) {
		 httpRequest1 = RestAssured.given();
		 if(mapCookies1!=null) {
		 httpRequest1.cookies(mapCookies1);
		 }
		 if(mapHeader!=null) {
			 httpRequest1.headers(mapHeader);
		 }
		 if(mapQueryParam!=null) {
			 httpRequest1.queryParams(mapQueryParam);
		 }
		 
		 objResponse = httpRequest1.get(RestAssured.baseURI)
									.then()
									//.contentType(ContentType.HTML)
									.extract()
									.response();
		 return objResponse;
	}
	
	public Response hitPostRequest(Map<String,String> mapHeader, Map<String,String> mapQueryParam, Map<String,String> mapFormParam, Map<String,String> mapMultiPart, String sBody) {
		 httpRequest1 = RestAssured.given();
		 if(mapCookies1!=null) {
		 httpRequest1.cookies(mapCookies1);
		 }
		 if(mapHeader!=null) {
			 httpRequest1.headers(mapHeader);
		 }
		 if(mapQueryParam!=null) {
			 httpRequest1.queryParams(mapQueryParam);
		 }
		 if(mapFormParam!=null) {
			 httpRequest1.formParams(mapFormParam);
		 }
		if(mapMultiPart!=null) {
			 setMultiParam(httpRequest1,mapMultiPart);
		 }
		 if(sBody!=null) {
			 httpRequest1.body(sBody);
		 }
		 
		 objResponse = httpRequest1.post(RestAssured.baseURI)
									.then()
									//.contentType(ContentType.HTML)
									.extract()
									.response();
		 return objResponse;
	}
	
	public Response hitGetRequestWithRedirect(Map<String,String> mapHeader, Map<String,String> mapQueryParam) {
		 httpRequest1 = RestAssured.given();
		 if(mapCookies1!=null) {
		 httpRequest1.cookies(mapCookies1);
		 }
		 if(mapHeader!=null) {
			 httpRequest1.headers(mapHeader);
		 }
		 if(mapQueryParam!=null) {
			 httpRequest1.queryParams(mapQueryParam);
		 }
		 
		objResponse = httpRequest1.when().redirects().follow(false)
								.get(RestAssured.baseURI)
								.then().extract()
								.response();
		
		return objResponse;
	}
	
	private void setMultiParam(RequestSpecification httpRequest1, Map<String,String> mapMultiPart)
	{
		/*String[] sSplittedMultiPart = sMPart.split(",");
		for(String s:sSplittedMultiPart)
		{
			String[] keyValues = s.split(":");
			httpRequest1.multiPart(keyValues[0], keyValues[1]);
		}*/
		
		for(Map.Entry<String, String> pair : mapMultiPart.entrySet())
		{
			httpRequest1.multiPart(pair.getKey(), pair.getValue());
		}
	}
	
	public String getHTMLXpathValue(Response objResponse2, String sXpath) throws Exception{
		String sHTMLXpathValue = null;
		try {
			XmlPath htmlPath = new XmlPath(HTML, objResponse2.getBody().asString());
			sHTMLXpathValue = htmlPath.getString(sXpath);
		}catch (Exception e) {
			String sErrMessage="FAIL: getXpathValue() method of BasePage: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		return sHTMLXpathValue;
	}
	
	public String getJSONXpathValue(Response objResponse2, String sXpath) throws Exception{
		String sJSONXpathValue = null;
		try {
			String xPathRule ="$.."+sXpath;
			String sresponseString = objResponse.asString();
			List<String> arrList = JsonPath.read(sresponseString, xPathRule);
			sJSONXpathValue = arrList.get(0);
		}catch (Exception e) {
			String sErrMessage="FAIL: getJSONXpathValue() method of BasePage: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		return sJSONXpathValue;
	}
	
	public Double getJSONXpathValueDouble(Response objResponse2, String sXpath) throws Exception{
		Double sJSONXpathValue = null;
		try {
			String xPathRule ="$.."+sXpath;
			String sresponseString = objResponse.asString();
			List<Double> arrList = JsonPath.read(sresponseString, xPathRule);
			sJSONXpathValue = arrList.get(0);
		}catch (Exception e) {
			String sErrMessage="FAIL: getJSONXpathValueDouble() method of BasePage: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		return sJSONXpathValue;
	}
}
