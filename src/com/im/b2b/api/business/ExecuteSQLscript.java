package com.im.b2b.api.business;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.im.api.common.Constants;
import com.im.b2b.api.business.APIEnumerations.ConnectToDatabase;
import com.im.b2b.api.business.APIEnumerations.dataBaseName;
import com.im.utility.DatabaseConnect;

/**
 * This class will help you to create connect to database and execute SQL Query
 * Common methods create to execute and capture data in collections
 * @author Prashant Navkudkar
 */
public class ExecuteSQLscript {
	Logger logger = Logger.getLogger("ExecuteSQLscript");

	private DatabaseConnect objDC = null;	
	private DataSource ds = null;
	private Connection con = null;
	ResultSet rs = null; Statement stmt =null;

	public ExecuteSQLscript(ConnectToDatabase databaseType, dataBaseName ods, String sUsername, String sPassword) throws SQLException  {
		objDC = new DatabaseConnect(databaseType, ods.value, sUsername, sPassword);
		ds = objDC.getDatsSource();	
		con = objDC.getConnection();
	}


	/**
	 * This function will help to execute query which will return single row of data/column
	 * @param sQuery: SQL Query to be executed
	 * @return This will return hashmap of data
	 * @author prashant navkudkar
	 * @throws SQLException 
	 */
	public List<Map<String, String>> executrScript(String sQuery) throws SQLException {
		List<Map<String, String>> lstMap = new ArrayList<Map<String,String>>();
		Map<String, String> mapOfData = null;
		Connection con = ds.getConnection();
		try {
			String[] sColumnsToBeRetrived = sQuery.replace(" ","").replace("SELECT", "").split("FROM")[0].split(",");
			stmt = con.createStatement();  
			rs = stmt.executeQuery(sQuery);  

			while(rs.next()) {	
				mapOfData = new HashMap<String, String>();
				for(String sKey: sColumnsToBeRetrived) {
					mapOfData.put(sKey, rs.getString(sKey));	
				}
				lstMap.add(mapOfData);
			}			
			System.out.println("executrScript: Data Captured for given requset: "+lstMap);

		}catch(Exception e) {
			e.printStackTrace();
		}
		return lstMap;
	}

	/**
	 * This function will help to execute query which will return multiple row of data/column
	 * @param sQuery: SQL Query to be executed
	 * @return This will return List of hashmap of data
	 * @author prashant navkudkar
	 */
	public Map<String, String> executrScriptToCaptureTwoColumnPairData(String sQuery) {
		Map<String, String> mapOfData = null;		
		try {
			Connection con = ds.getConnection();
			String[] sColumnsToBeRetrived = sQuery.replace(" ","").replace("SELECT", "").split("FROM")[0].split(",");

			stmt=con.createStatement();  
			rs=stmt.executeQuery(sQuery);  

			mapOfData = new HashMap<String, String>();	

			while(rs.next()) {	
				String sKey = rs.getString(sColumnsToBeRetrived[0]).trim();
				String sValue = rs.getString(sColumnsToBeRetrived[1]).trim();
				if(mapOfData.containsKey(sKey)) {
					String sExistingValue = mapOfData.get(sKey);
					mapOfData.put(sKey, sExistingValue+","+sValue);
				}else
					mapOfData.put(sKey, sValue);	
			}			
			System.out.println("executrScriptToCaptureTwoColumnPairData: Data Captured for given request: "+mapOfData);

		}catch(Exception e) {
			e.printStackTrace();
		}
		return mapOfData;
	}

	/**
	 * This function will help to execute query which will return multiple row of data/column
	 * @param sQuery: SQL Query to be executed
	 * @return This will return hashmultimap of data
	 * @author Chandra
	 */
	public Multimap<String, String> executrScriptToCaptureMultimapData(String sQuery) {
		Multimap<String, String> mapOfData = null;		
		try {
			Connection con = ds.getConnection();
			String[] sColumnsToBeRetrived = sQuery.replace(" ","").replace("SELECT", "").split("FROM")[0].split(",");

			stmt=con.createStatement();  
			rs=stmt.executeQuery(sQuery);  

			mapOfData = HashMultimap.create();	

			while(rs.next()) {	
				String sKey = rs.getString(sColumnsToBeRetrived[0]).trim();
				String sValue = rs.getString(sColumnsToBeRetrived[1]).trim();
				mapOfData.put(sKey, sValue);	
			}			
			System.out.println("executrScriptToCaptureMultimapData: Data Captured for given request: "+mapOfData);

		}catch(Exception e) {
			e.printStackTrace();
		}
		return mapOfData;
	}

	/**
	 * This function will help to get column all column data in list format
	 * @param sQuery: SQL Query to be executed
	 * @return This will return hashmap of data
	 * @author prashant navkudkar
	 */
	public Map<String, Set<String>> executeScriptToCaptureSetOfData(String sQuery) {
		Set<String> setOfData = new HashSet<String>();
		Map<String, Set<String>> mapOfData = new HashMap<>();
		try {
			Connection con = ds.getConnection();
			String[] sColumnsToBeRetrived = sQuery.replace(" ","").replace("SELECT", "").split("FROM")[0].split(",");
			stmt = con.createStatement();  
			rs = stmt.executeQuery(sQuery);  

			while(rs.next()) {				
				setOfData = new HashSet<String>();
				for(String sKey: sColumnsToBeRetrived) {
					if(mapOfData.containsKey(sKey))
						mapOfData.get(sKey).add(rs.getString(sKey));
					else {
						setOfData.add(rs.getString(sKey));
						mapOfData.put(sKey, setOfData);}	
				}
			}			
			System.out.println("executeScriptToCaptureSetOfData: Data Captured for given requset: "+mapOfData);

		}catch(Exception e) {
			e.printStackTrace();
		}
		return mapOfData;
	}	

	public void closeConnection() throws SQLException {

		if(rs != null) rs.close();
		if(stmt != null) stmt.close();
		if(ds.getConnection()!= null) { ds.getConnection().close();
		System.out.println("<============= SUCCESS: Connection closed successfully ===============>");
		}

	}

	public DataSource getDataSource() {
		return ds;
	}
	
	/**
	 * To execute select query on DB
	 * 
	 * @param query query to be executed
	 * @return Resultset results returned by the query
	 * 
	 * @author Priyesh Thakur
	 */
	public ResultSet executeQuery(String query) {
		logger.info("executeQuery: " + query);
		ResultSet rs = null;

		Statement stmt = null;
		try {
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(query);

			return rs;
		} catch (SQLException e) {
			logger.warn("SQL exception in executeQuery");
			Assert.assertTrue(false, "SQL exception in executeQuery " + e.getMessage());
			return rs;
		}
	}

	/**
	 * To execute update or delete query on DB
	 * 
	 * @param query query to be executed
	 * @return no of rows updated
	 * 
	 * @author Priyesh Thakur
	 */
	public int executeUpdate(String query) {
		logger.info("executeUpdate: " + query);

		Statement stmt = null;
		int res = 0;
		try {
			stmt = con.createStatement();
			res = stmt.executeUpdate(query);
			return res;
		} catch (SQLException e) {
			logger.warn("SQL exception in executeUpdate");
			Assert.assertTrue(false, "SQL exception in executeUpdate");
			return res;
		}
	}
	
	/**
	 * Function to get dbConnectionString, username, password based on the
	 * environment for database connection
	 * 
	 * @param env - Environment for DB connection
	 * @return dbConnectionString, username, password based on the environment
	 *         passed
	 * @throws SQLException
	 * 
	 * @author Priyesh Thakur
	 */
	public HashMap<String, String> getDBConnectionDetails(String env) throws SQLException {
		logger.info("enter DataBase.getDBConnectionDetails");

		String dbConnectionString;
		String dbUsername = Constants.DBUSERNAME;
		String dbPassword = Constants.DBPASSWORD;

		switch (env.toUpperCase()) {
		case "QA":
			dbConnectionString = Constants.DBQACONNECTIONSTRING;
			break;
		case "DEV":
			dbConnectionString = Constants.DBDEVCONNECTIONSTRING;
			break;
		default:
			dbConnectionString = Constants.DBQACONNECTIONSTRING;
		}
		HashMap<String, String> connectionDetailsDB = new HashMap<String, String>();

		// add all 3 parameter values to hashmap for the column-PATID and PAT_NAME
		connectionDetailsDB.put("connectionString", dbConnectionString);
		connectionDetailsDB.put("username", dbUsername);
		connectionDetailsDB.put("password", dbPassword);

		return connectionDetailsDB;
	}

	/**
	 * Function to verify whether a particular column exists in the table or not
	 * 
	 * @param rs - resultSet
	 * @param columnName - name of the column
	 * 
	 * @return true if column is present else false
	 * 
	 * @author Priyesh Thakur
	 */
	public boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			if (columnName.equals(rsmd.getColumnName(x))) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Function to get all columns from resultset
	 * 
	 * @param rs - resultSet
	 * 
	 * @return list of all columns
	 * 
	 * @author Priyesh Thakur
	 */
	public List<String> returnAllColumns(ResultSet rs) throws SQLException{
		List<String> colList = new ArrayList<String>();

		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			colList.add(rsmd.getColumnName(x));
		}
		return colList;
	}

	/**
	 * Function to get all values for a specific column
	 * 
	 * @param rs - resultSet
	 * @param columnName - name of the column
	 * 
	 * @return list of all values available for given column
	 * 
	 * @author Priyesh Thakur
	 */
	public List<String> returnColData(ResultSet rs, String colName) {
		List<String> arrTemp = new ArrayList<String>();
		try {
			while (rs.next()) {
				String str = rs.getObject(colName) + "";
				arrTemp.add(str);
			}
			rs.beforeFirst();
		} catch (Exception e) {
			System.out.println(arrTemp.isEmpty());
		}
		return arrTemp;
	}
}
