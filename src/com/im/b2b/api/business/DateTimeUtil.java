package com.im.b2b.api.business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateTimeUtil {

	/*** 
	 * @param sCountryName
	 * @return Date format for the given country
	 * @author Prashant Navkudkar
	 */
	public static synchronized SimpleDateFormat getDefaultCountryDateFormat(String sCountryName) {
		SimpleDateFormat objSimpleDateFormat = null;

		switch(sCountryName){

		case "US":
			objSimpleDateFormat = new SimpleDateFormat("M/d/yyyy");
			break;

		case "HK":		
		case "MI":	
			objSimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			break;

		case "NL":
			objSimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			break;	

		case "CL":
			objSimpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
			break;

		case "SE":
		case "CO":
		case "CA":
			objSimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			break;

		case "DE":
		case "AT":
			objSimpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
			break;

		default:
			objSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		}

		return objSimpleDateFormat;		
	}
	//TODO to make ordersearch use verifyGivenDateFallsBetween2DatesInMentionedFormat and delete the below given method

	public static synchronized <E> boolean verifyGivenDateFallsBetween2Dates(List<Date> dActualOrderCreateDate,Date dExpectedFromDate,Date dExpectedToDate) {		
		boolean bResult= false;

		for(Date date:dActualOrderCreateDate) {
			bResult=date.after(dExpectedFromDate)&&date.before(dExpectedToDate);
			if(bResult==false)
				break;
		}
		if(dActualOrderCreateDate.size()==0)
			bResult=false;
		return bResult;

	}
	public static synchronized <E> boolean verifyGivenDateFallsBetween2DatesInMentionedFormat(List<String> sActualDate,String sExpectedFromDate,String sExpectedToDate, String fdateFormat) throws ParseException {		
		boolean bResult= false;
		if(sActualDate.size()==0)
			return false;

		DateFormat sdf= new SimpleDateFormat(fdateFormat);

		Date dExpectedFromDate=sdf.parse(sExpectedFromDate);  
		String sOneDayBeforeFromDay=getDatePlusNoOfDays(dExpectedFromDate,-1);
		Date dOneDayBeforeFromDate=sdf.parse(sOneDayBeforeFromDay);

		Date dExpectedToDate=sdf.parse(sExpectedToDate); 
		String sOneDayAfterToDay=getDatePlusNoOfDays(dExpectedToDate,1);
		Date dOneDayAfterToDate=sdf.parse(sOneDayAfterToDay);


		List<Date> lstDActualDate= new ArrayList<Date>();
		for(String  sdate: sActualDate) {
			String sStringDate=sdate.toString();
			lstDActualDate.add(sdf.parse(sStringDate));
		}
		for(Date date:lstDActualDate) {
			bResult=date.after(dOneDayBeforeFromDate)&&date.before(dOneDayAfterToDate);
			if(bResult==false)
				break;
		}
		return bResult;
	}
	public static synchronized String getDatePlusNoOfDays(Date date,int iNoOfDays) {

		DateFormat dateFormat =   new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, iNoOfDays);
		Date toDate = cal.getTime(); 
		return dateFormat.format(toDate);
	}
	public static synchronized String getDateMinusNoOfDays(Date date,int iNoOfDays) {

		DateFormat dateFormat =   new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, iNoOfDays*-1);
		Date toDate = cal.getTime(); 
		return dateFormat.format(toDate);

	}
	public static synchronized String getTodaysDateInFormat_YYYY_MM_DD() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
		return sdf.format(new Date());
	}
	public static synchronized String getSystemDatePlusNoOfDays(String ddateFormat, int iNoOfDays) {
		DateFormat dateFormat = null;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, iNoOfDays);
		Date toDate = cal.getTime(); 
		dateFormat = new SimpleDateFormat(ddateFormat);
		return dateFormat.format(toDate);
	}
	public static synchronized String getDatePlusOrMinusNoOfDaysForStringDate(String date,String sdateFormat,int iNoOfDays) throws ParseException {

		DateFormat dateFormat =   new SimpleDateFormat(sdateFormat);
		Date dDate=dateFormat.parse(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dDate);
		cal.add(Calendar.DATE, iNoOfDays*1);
		Date toDate = cal.getTime(); 
		return dateFormat.format(toDate);

	}

	public static <E> boolean verifyTheListOfDateFitBetweenTheMentionedRange(List<E> sActualDate,
			String sExpectedFromDate, String sExpectedToDate, String fdateFormat) throws ParseException {
		boolean bCheck= false;
		boolean bResult= false;
		if(sActualDate.size()==0)
			return false;

		DateFormat sdf= new SimpleDateFormat(fdateFormat);

		Date dExpectedFromDate=sdf.parse(sExpectedFromDate);  
		String sOneDayBeforeFromDay=getDatePlusNoOfDays(dExpectedFromDate,-1);
		Date dOneDayBeforeFromDate=sdf.parse(sOneDayBeforeFromDay);

		Date dExpectedToDate=sdf.parse(sExpectedToDate); 
		String sOneDayAfterToDay=getDatePlusNoOfDays(dExpectedToDate,1);
		Date dOneDayAfterToDate=sdf.parse(sOneDayAfterToDay);

		List <Boolean> lstbResult= new ArrayList<Boolean>();

		for(E strActual : sActualDate) {
			List<E> lstinEFormatOfActualDate= (List<E>) strActual;

			for(E  sdate: lstinEFormatOfActualDate) {
				Date dStringDate=sdf.parse(sdate.toString());
				
				if((dStringDate).after(dOneDayBeforeFromDate)&&(dStringDate).before(dOneDayAfterToDate)) {
					bCheck=true;
					break;
				}
				
			lstbResult.add(bCheck);
		}

		if((lstbResult.contains(false))) {
			bResult= false;
		}else
			bResult= true;
	}
		return bResult;
		}

	public static synchronized String getTodaysDateInFormat_MM_DD_YYYY_AndUpdateFormat() {
		LocalDate date = LocalDate.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String sDate = date.format(myFormatObj).replace("/", "%2F");
		
		return sDate;
	}
}



