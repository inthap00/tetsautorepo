package com.im.b2b.api.business;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.wrapper.api.APIAssertion;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class XMLUtil {

	RequestSpecification httpRequest=null;
	Response objResponse = null;
	HashMap<String, String> excelHMap=null;
	HashMap<String, String> configHMap=null;
	String sRequestBody = null;
	int iStatusCode=0;



	public XMLUtil(String pRequestBody, HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap) {
		sRequestBody=pRequestBody;
		excelHMap=pExcelHMap;
		configHMap = pConfigHMap;
	}

	public void postRequest() {
		String sEnvironment=System.getProperty("Environment").trim();	
		sEnvironment =(!sEnvironment.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sEnvironment:RunConfig.getProperty(Constants.Environment);
		
		RestAssured.baseURI = configHMap.get(Constants.EXCELHEADEREndPointURL+"_"+sEnvironment);

		httpRequest = RestAssured.given();
		httpRequest.header("Content-Type", "application/xml");
		//httpRequest.auth().basic(excelHMap.get("UserName"), excelHMap.get("Password")); 	
		httpRequest.body(sRequestBody);
		objResponse=httpRequest.post();

		iStatusCode = objResponse.getStatusCode();
	}


	public int getStatusCode() {
		return iStatusCode;
	}
	
	public Response getResponse() {
		return objResponse;
	}


	public synchronized String getNodeValueByTagName(String sNodeName) throws ParserConfigurationException, SAXException, IOException {

		String sActualValue="";
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		InputSource is;
		builder = factory.newDocumentBuilder();

		is = new InputSource(new StringReader(objResponse.asString()));
		Document doc = builder.parse(is);
		NodeList list = doc.getElementsByTagName(sNodeName);
		if(!(list.getLength()==0))
			sActualValue = list.item(0).getTextContent() ;  

		return sActualValue;

	}


	public synchronized NodeList getListOfNodeForTagName(String sNodeName) throws SAXException, IOException, ParserConfigurationException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		InputSource is;
		builder = factory.newDocumentBuilder();

		is = new InputSource(new StringReader(objResponse.asString()));
		Document doc = builder.parse(is);
		NodeList list = doc.getElementsByTagName(sNodeName);

		return list;

	}

	public synchronized String getXMLNodeValByXPath(String sNodeXpath) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		String retVal="";

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		InputSource is;
		builder = factory.newDocumentBuilder();
		is = new InputSource(new StringReader(objResponse.asString()));
		Document doc = builder.parse(is);
		XPath xPath =  XPathFactory.newInstance().newXPath();
		NodeList nList = (NodeList) xPath.compile(sNodeXpath).evaluate(doc, XPathConstants.NODESET);
		if(!(nList.getLength()==0))
			retVal=nList.item(0).getTextContent();

		return retVal;

	}

	//TODO: Parameter should be List of HashMap of response 
	public void validateMultiSetDetails(APIAssertion objAPIAssertion, NodeList nlstToBeVerified, String[] sToBeVerify, String sTestDataFilenm) throws Exception {
		try {
			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(excelHMap, sTestDataFilenm);
			List<HashMap<String, String>> lstOfTestDataFromResponse = new ArrayList<HashMap<String, String>>();
			lstOfTestDataFromResponse = XMLUtil.getResponseDataofMultipleSet(nlstToBeVerified);

			for(int i=0; i<multisetDataDrivenMap.size(); i++)
			{
				for(int j=0; j<sToBeVerify.length; j++) {
					String sActual = lstOfTestDataFromResponse.get(i+1).get(sToBeVerify[j]).toUpperCase();
					String sExpected = multisetDataDrivenMap.get(i).get(sToBeVerify[j]).toUpperCase();
					objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sToBeVerify[j].toUpperCase());
					System.out.println("Actual: "+sActual+" Expected :"+sExpected);
				}
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @param objOfNodelstToBeTraversed:
	 * @return This function returns you the hash map of tage name and respective values of multiple set in the response
	 */
	public synchronized static List<HashMap<String,String>> getResponseDataofMultipleSet(NodeList objOfNodelstToBeTraversed){

		ArrayList<HashMap<String, String>> lstHmapMultiSetResponse = new ArrayList<HashMap<String, String>>();

		if(objOfNodelstToBeTraversed != null && objOfNodelstToBeTraversed.getLength() > 0) {
			for(int i = 0 ; i < objOfNodelstToBeTraversed.getLength();i++) {
				Element el = (Element)objOfNodelstToBeTraversed.item(i);
				NodeList nl = el.getChildNodes();
				HashMap< String, String> hmapNodeData = new HashMap<>();
				if(nl != null && nl.getLength() > 0) {
					for(int j=0; j<nl.getLength(); j++) {
						Element el1 = (Element)nl.item(j);					
						hmapNodeData.put(el1.getTagName(), el1.getTextContent());
					}
					lstHmapMultiSetResponse.add(hmapNodeData);
				}			
			}					
		}
		return lstHmapMultiSetResponse;

	}


}//end of class
