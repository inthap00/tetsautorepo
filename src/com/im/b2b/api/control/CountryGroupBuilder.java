package com.im.b2b.api.control;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D., Sumeet U, Prashant N.*/
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.im.api.common.Constants;
import com.im.api.common.RunConfig;
import com.im.api.common.Util;
import com.im.b2b.api.business.MapTCForNations;

public class CountryGroupBuilder {
	private final Logger logger = Logger.getLogger(CountryGroupBuilder.class.getName());
	public  static final String CountrynameSeparator="~", ALLEXCELCOUNTRIES="ALL", sGroupStart="[", sGroupEnd="]", sGroupSeparator=",", sCountryStart="{", sCountryEnd="}";
	private static boolean bPropLoaded=false;

	private HashMap<String, String> hmCountryMap= new  HashMap<String, String>();

	public static final String GroupList[]= {"IGNORECALCULATIONCHECK","MVC_ROLLEDOUT","IMONLINE", "SAP", "IMPULSE" ,"PAYMENT_TYPE_BASKETDETAILS",
			"SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED","CREDITCARD_ELIGIBLE", "QUOTES_APPLICABLE", "DIRECT_SHIP_SKU" ,"SOFTWARE_LICENSE_PRODUCTS",
			"JTYPE_SKU","END_USER", "MULTILINGUAL", "SBO_ELIGIBLE" ,"FREE_ITEM_ELIGIBLE",
			"SPECIAL_BID_ELIGIBLE","BUNDLE_ELIGIBLE", "CLICK_2_LICENSE", "SELECT_DIFFERENT_BILL_TO" ,"KENTICO_CMS_ROLLOUT_STATUS",
			"KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES","CH_DC", "SG_DC", "RRP", "INVOICEGROUP","CARRIERELIGIBLECHECK","DISABLE_SHIPPING_ADDRESS", 
			"WITHOUT_ENGLISH_LANG_DROPDOWN","FF_DC"

	};

	public CountryGroupBuilder(){
		if(!bPropLoaded)
			loadProperties();
	}

	private synchronized void loadProperties() {
		Properties properties = new Properties();

		try {
			properties.load(Util.class.getResourceAsStream("/" + Constants.COUNTRYGROUPPROP));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int i=0;i<GroupList.length;i++) {
			String sCountryList=properties.getProperty(GroupList[i]);
			hmCountryMap.put(GroupList[i], sCountryList);
		}
	}



	public CountryGroups getCountryGroupsForCountry(String sCountryName2CharInCAPS) throws Exception {
		if(null==sCountryName2CharInCAPS) {
			throw new Exception("CountryGroupBuilder: The country name received from the excel test data is null!!"); 
		}
		CountryGroups oCG=new CountryGroups(sCountryName2CharInCAPS);

		for(int i=0;i<GroupList.length;i++) {
			String sCurrentGroup=GroupList[i];
			String sCountryList=hmCountryMap.get(sCurrentGroup);
			if(null==sCurrentGroup||null==sCountryList)
				logger.error("Either the CurrentGroup["+sCurrentGroup+"] or the respective CountryList["+sCountryList+"] is invalid/not supported.");
			else if(sCountryList.contains(sCountryName2CharInCAPS)) {

				switch(sCurrentGroup) {
				case "MVC_ROLLEDOUT": oCG.setMVC_ROLLEDOUT(true);break;
				case "IMONLINE":  oCG.setIMONLINE(true);break;
				case "SAP": oCG.setSAP(true);break;
				case "IMPULSE":  oCG.setIMPULSE(true);break;
				case "PAYMENT_TYPE_BASKETDETAILS": oCG.setPAYMENT_TYPE_BASKETDETAILS(true);break;
				case "SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED":  oCG.setSEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED(true);break;

				case "CREDITCARD_ELIGIBLE": oCG.setCREDITCARD_ELIGIBLE(true);break;
				case "QUOTES_APPLICABLE":  oCG.setQUOTES_APPLICABLE(true);break;
				case "DIRECT_SHIP_SKU": oCG.setDIRECT_SHIP_SKU(true);break;
				case "SOFTWARE_LICENSE_PRODUCTS":  oCG.setSOFTWARE_LICENSE_PRODUCTS(true);break;
				case "JTYPE_SKU": oCG.setJTYPE_SKU(true);break;
				case "END_USER":  oCG.setEND_USER(true);break;
				case "MULTILINGUAL":  oCG.setMULTILINGUAL(true);break;
				case "SBO_ELIGIBLE": oCG.setSBO_ELIGIBLE(true);break;
				case "FREE_ITEM_ELIGIBLE":  oCG.setFREE_ITEM_ELIGIBLE(true);break;

				case "SPECIAL_BID_ELIGIBLE": oCG.setSPECIAL_BID_ELIGIBLE(true);break;
				case "BUNDLE_ELIGIBLE":  oCG.setBUNDLE_ELIGIBLE(true);break;
				case "CLICK_2_LICENSE": oCG.setCLICK_2_LICENSE(true);break;
				case "SELECT_DIFFERENT_BILL_TO":  oCG.setSELECT_DIFFERENT_BILL_TO(true);break;	

				case "KENTICO_CMS_ROLLOUT_STATUS": oCG.setKENTICO_CMS_ROLLOUT_STATUS(true);break;
				case "KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES":  oCG.setKENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES(true);break;
				case "CH_DC": oCG.setCH_DC(true);break;
				case "SG_DC":  oCG.setSG_DC(true);break;
				case "RRP":  oCG.setRRP(true);break;
				case "INVOICEGROUP" : oCG.setInvoiceGroup(true);break;
				case "IGNORECALCULATIONCHECK" : oCG.setIGNORECALCULATIONCHECK(true);break;
				case "CARRIERELIGIBLECHECK" : oCG.setCARRIERELIGIBLECHECK(true);break;
				case "DISABLE_SHIPPING_ADDRESS": oCG.setDISABLE_SHIPPING_ADDRESS(true);
				case "WITHOUT_ENGLISH_LANG_DROPDOWN": oCG.setWITHOUT_ENGLISH_LANG_DROPDOWN(true);
				case "FF_DC": oCG.setFF_DC(true); break;
				default: logger.error("The given group name["+sCurrentGroup+"] is not yet supported. Please check the :["+Constants.COUNTRYGROUPPROP+"] file.");
				}//Switch ends

			}//else ends
		}//for ends
		return oCG;
	}

	public ArrayList<String> getCountrysForGivenGroup(String sGroupName) {
		String sCountryDecodedList=hmCountryMap.get(sGroupName);
		if(null==sCountryDecodedList||null==sGroupName) {
			logger.error("The countrygroup given["+sGroupName+"] is null or value for it is null.");
			return null;
		}
		return getSeparateTokensFromTheDecodedList(sCountryDecodedList, sGroupStart, sGroupEnd, CountrynameSeparator);
	}

	public ArrayList<String> getSeparateTokensFromTheDecodedList(String sCountryDecodedList,String sStartChar, String sEndsChar, String sSeparator ) {
		ArrayList<String> listTokens=new ArrayList<String>();
		int iStart=sCountryDecodedList.indexOf(sStartChar);
		int iEnds=sCountryDecodedList.indexOf(sEndsChar);

		String sActualCombinedTokensCountries=sCountryDecodedList.substring(iStart+1, iEnds);
		listTokens.addAll(Util.tokenizeString(sActualCombinedTokensCountries, sSeparator));
		return listTokens;
	}

	public List<HashMap<String, String>>  processRunCountryEnvVariable(List<HashMap<String, String>> listExcelRowsList){
		ArrayList<String> listRuntimeCountryList=getAllThecountriesAsPerTheCountryEnvVariable();
		List<HashMap<String, String>> listFilteredExcelRow=new ArrayList<HashMap<String, String>>();

		if((null==listRuntimeCountryList||listRuntimeCountryList.size()==0)) {
			//No changes from the runtime country selection
			listFilteredExcelRow=listExcelRowsList;
		}else {
			ArrayList<String> listRuntimeCountryListNotInExcelCountry= new ArrayList<String>(listRuntimeCountryList);
			ArrayList<String> listRuntimeListInExcelCountryList= new ArrayList<String>();
			for(HashMap<String, String> hmExcelRow : listExcelRowsList ) {

				try {
					String sCountryName=hmExcelRow.get(Constants.ExcelHeaderRunConfig).trim();
					if(listRuntimeCountryList.contains(sCountryName)) {
						listFilteredExcelRow.add(hmExcelRow);
						listRuntimeListInExcelCountryList.add(sCountryName);
					}else logger.debug("The given entry from Excel testdata input is ignored as not given in the RuntimeCountry: ["+sCountryName+"]");


				} catch (Exception e) {
					logger.error("Jackson or file related issue : "+e.getMessage()+"]");
				}
			}

			listRuntimeCountryListNotInExcelCountry.removeAll(listRuntimeListInExcelCountryList);
			if(null!=listRuntimeCountryListNotInExcelCountry||listRuntimeCountryListNotInExcelCountry.size()>0)
				printRuntimeCountriesWithoutTestDataInExcel(listRuntimeCountryListNotInExcelCountry);

		}
		return listFilteredExcelRow;


	}
	
	
	public List<HashMap<String, String>>  processRunCountryEnvVariableWithApplicabilityTrueCountries(List<HashMap<String, String>> listExcelRowsList,
			HashMap<String,MapTCForNations>  pListTCMappingToCountry){
		
		ArrayList<String> listRuntimeCountryList=getAllThecountriesAsPerTheCountryEnvVariable();
		List<HashMap<String, String>> listFilteredExcelRow=new ArrayList<HashMap<String, String>>();

		if((null==listRuntimeCountryList||listRuntimeCountryList.size()==0)) {
			//No changes from the runtime country selection
			listFilteredExcelRow=listExcelRowsList;
		}else {
			ArrayList<String> listRuntimeCountryListNotInExcelCountry= new ArrayList<String>(listRuntimeCountryList);
			ArrayList<String> listRuntimeListInExcelCountryList= new ArrayList<String>();
			
			Set<String> setOfCountryTrueInApplicability = new HashSet<>();
			for( Entry<String, MapTCForNations> mapElement :pListTCMappingToCountry.entrySet()) {
				MapTCForNations oMap =  mapElement.getValue();
				for(String sCurrentCountry: CountryGroups.countryListArray) {
					if(oMap.isCountryEabledForExecution(sCurrentCountry))
						setOfCountryTrueInApplicability.add(sCurrentCountry);
				}
			}
			
			for(HashMap<String, String> hmExcelRow : listExcelRowsList ) {

				try {
					String sCountryName=hmExcelRow.get(Constants.ExcelHeaderRunConfig).trim();
					if(listRuntimeCountryList.contains(sCountryName) && setOfCountryTrueInApplicability.contains(sCountryName) ) {
						listFilteredExcelRow.add(hmExcelRow);
						listRuntimeListInExcelCountryList.add(sCountryName);
					}else logger.debug("The given entry from Excel testdata input is ignored as not given in the RuntimeCountry: ["+sCountryName+"]");


				} catch (Exception e) {
					logger.error("Jackson or file related issue : "+e.getMessage()+"]");
				}
			}

			listRuntimeCountryListNotInExcelCountry.removeAll(listRuntimeListInExcelCountryList);
			if(null!=listRuntimeCountryListNotInExcelCountry||listRuntimeCountryListNotInExcelCountry.size()>0)
				printRuntimeCountriesWithoutTestDataInExcel(listRuntimeCountryListNotInExcelCountry);

		}
		return listFilteredExcelRow;
	}
	
	
	
	
	private void printRuntimeCountriesWithoutTestDataInExcel(
			ArrayList<String> listRuntimeCountryListNotInExcelCountry) {
		StringBuilder sb = new StringBuilder();
		for(String sWithoutExcelEntryCountryOfRuntime : listRuntimeCountryListNotInExcelCountry ) {
			sb.append(sWithoutExcelEntryCountryOfRuntime+", ");

		}
		logger.info("*** Following countries which were part of Runtime country selection were not enable/present in the Excel Data provider:["+sb+"] ****");
	}
	
	
	

	private ArrayList<String> decodeTheCroupToGetUniqueCountries(String sCountryGroupName){
		ArrayList<String> listCountrysOfGivenGroup=null;
		String sCountrysSeparatedList=null;
		//Delete this code after 15 Dec 2018
		/*switch(sCountryGroupName) { 
		//case ALLEXCELCOUNTRIES: //Do not change, and process all the excel countries.
		case "MVC_ROLLEDOUT": 
		case "IMONLINE":  
		case "SAP": 
		case "IMPULSE":  
		case "PAYMENT_TYPE_BASKETDETAILS": 
		case "SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED":
		case "CREDITCARD_ELIGIBLE": 
		case "QUOTES_APPLICABLE": 
		case "DIRECT_SHIP_SKU": 
		case "SOFTWARE_LICENSE_PRODUCTS": 
		case "JTYPE_SKU": 
		case "END_USER": 
		case "MULTILINGUAL": 
		case "SBO_ELIGIBLE": 
		case "FREE_ITEM_ELIGIBLE": 
		case "SPECIAL_BID_ELIGIBLE": 
		case "BUNDLE_ELIGIBLE": 
		case "CLICK_2_LICENSE": 
		case "SELECT_DIFFERENT_BILL_TO": 
		case "KENTICO_CMS_ROLLOUT_STATUS": 
		case "KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES": 
		case "CH_DC": 
		case "SG_DC": 
		case "RRP": 
		case "INVOICEGROUP" : 
		case "IGNORECALCULATIONCHECK" : 
		case "CARRIERELIGIBLECHECK" : 
		case "DISABLE_SHIPPING_ADDRESS":
		case "WITHOUT_ENGLISH_LANG_DROPDOWN": sCountrysSeparatedList=hmCountryMap.get(sCountryGroupName); break;
		default: logger.error("The given group name["+sCountryGroupName+"] is not yet supported. Please check the :["+Constants.COUNTRYGROUPPROP+"] file.");
		}*/
		
		sCountrysSeparatedList=hmCountryMap.get(sCountryGroupName);
		if (sCountrysSeparatedList==null)
			 logger.error("The given group name["+sCountryGroupName+"] is not yet supported. Please check the :["+Constants.COUNTRYGROUPPROP+"] file.");
		
		if(null!=sCountrysSeparatedList){
			sCountrysSeparatedList=sCountrysSeparatedList.replaceAll("\"", "");
			listCountrysOfGivenGroup = Util.tokenizeString(sCountrysSeparatedList, CountrynameSeparator);
			System.out.println("Added countries form Group["+sCountryGroupName+"] countries["+listCountrysOfGivenGroup+"]");
		}
		return listCountrysOfGivenGroup;
	}
	public ArrayList<String> getCountryForDifferentCountryGroups(ArrayList<String> listCountryGroups){
		//Get the countries from the given group
		ArrayList<String> listCountriesFromTheGroups=new ArrayList<String>();
		for(String sGroupName : listCountryGroups) {
			listCountriesFromTheGroups.addAll(decodeTheCroupToGetUniqueCountries(sGroupName.trim()));
		}
		return listCountriesFromTheGroups;
	}
	
	//RUNFORCOUNTRIES={~~~}[,,]
	//RUNFORCOUNTRIES={}
	//RUNFORCOUNTRIES=[]
	public ArrayList<String> getAllUniqueCountriesFromTheGivenRunForCountriesProperties(String sCountryGroupEnvValue){
		ArrayList<String> listCountry=new ArrayList<String>();
		ArrayList<String> listCountryGroups=new ArrayList<String>();
		boolean bGroupPresent = sCountryGroupEnvValue.contains(sGroupStart);
		boolean bDirectCountryPresent = sCountryGroupEnvValue.contains(sCountryStart);
		boolean bCountrySepPresent = sCountryGroupEnvValue.contains(CountrynameSeparator);
		boolean bCountryGroupSepPresent = sCountryGroupEnvValue.contains(sGroupSeparator);
		
		if(bDirectCountryPresent) //only {~~~}
			listCountry.addAll(getSeparateTokensFromTheDecodedList(sCountryGroupEnvValue, sCountryStart, sCountryEnd, CountrynameSeparator));

		if(bGroupPresent) {//only [,,]
			listCountryGroups.addAll(getSeparateTokensFromTheDecodedList(sCountryGroupEnvValue, sGroupStart, sGroupEnd, sGroupSeparator));

			//Get the countries from the given group
			ArrayList<String> listCountriesFromTheGroups=getCountryForDifferentCountryGroups(listCountryGroups);

			//Add the countries which are specified from the country groupss
			listCountry.addAll(listCountriesFromTheGroups);
		}

		if(!(bGroupPresent||bDirectCountryPresent)&bCountrySepPresent) {
			//Only "~~~"
			listCountry.addAll(Util.tokenizeString(sCountryGroupEnvValue, CountrynameSeparator));
		}
		if(!(bGroupPresent||bDirectCountryPresent||bCountrySepPresent)) {
			ArrayList<String> listSeparatedCountryGroups=new ArrayList<String> ();
			//Get the countries from the given group
			if(bCountryGroupSepPresent){
				listSeparatedCountryGroups.addAll(Util.tokenizeString(sCountryGroupEnvValue, sGroupSeparator));
			}else listSeparatedCountryGroups.add(sCountryGroupEnvValue);
			ArrayList<String> listCountriesFromTheGroups=getCountryForDifferentCountryGroups(listSeparatedCountryGroups);
			listCountry.addAll(listCountriesFromTheGroups);
		}
		//Remove duplicate elements
		Set<String> countrySet = new HashSet<String>(listCountry);
		listCountry.clear();
		listCountry.addAll(countrySet);
		for(String stoken : listCountry) {
			System.out.println(stoken+", ");
		}
		return listCountry;
	}

	public ArrayList<String> getAllThecountriesAsPerTheCountryEnvVariable(){
		String sValueRunConfig = null;
		if (!System.getProperty("CountriesValue").trim().equalsIgnoreCase("READ_FROM_PROPERTIES_FILE")) 
			sValueRunConfig = System.getProperty("CountriesValue").trim().toUpperCase();
		else 
		    sValueRunConfig=RunConfig.getProperty(Constants.RUNFORCOUNTRIES).trim();
	
		System.out.println("****************["+sValueRunConfig+"]*******");
		
		ArrayList<String> listCountry=new ArrayList<String>();

		if(!sValueRunConfig.startsWith(ALLEXCELCOUNTRIES)) {
			if(sValueRunConfig.length()==2) {
				listCountry.add(sValueRunConfig);
			}else {
				listCountry.addAll(getAllUniqueCountriesFromTheGivenRunForCountriesProperties(sValueRunConfig));

			}
		}else
			listCountry.addAll(Arrays.asList(CountryGroups.countryListArray));
		return listCountry;

	}
	public ArrayList<String> temp(String sValueRunConfig){
		ArrayList<String> listCountry=new ArrayList<String>();

		if(!sValueRunConfig.startsWith(ALLEXCELCOUNTRIES)) {
			if(sValueRunConfig.length()==2) {
				listCountry.add(sValueRunConfig);
			}else {
				listCountry.addAll(getAllUniqueCountriesFromTheGivenRunForCountriesProperties(sValueRunConfig));

			}
		}
		System.out.println("+++++++++++"+ listCountry);
		return listCountry;

	}

	public static void main(String args[]) throws Exception {
/*				CountryGroups oAU_CG = getCountryGroupsForCountry("MX");
		
				if(oAU_CG.isCARRIER_ELIGIBLE()) {
					System.out.println("Inside IF condition");
				}
				System.out.println(oAU_CG);*/
		//EUNFORCOUNTRIES={~~~}[,,]
		//EUNFORCOUNTRIES={}
		//EUNFORCOUNTRIES=AU
		//EUNFORCOUNTRIES={AU~MX~MX}
		//{ZA~ZB~IT}[JTYPE_SKU]
		CountryGroupBuilder cgb = new CountryGroupBuilder();
		cgb.temp("JTYPE_SKU, FREE_ITEM_ELIGIBLE");
		cgb.temp("JTYPE_SKU");
		cgb.temp("[JTYPE_SKU]");
		cgb.temp("[JTYPE_SKU, FREE_ITEM_ELIGIBLE]");
		cgb.temp("AU~WO");
		cgb.temp("AU");
		cgb.temp("ALL");
		cgb.temp("{SE,RT,FE}");
		cgb.temp("{SE,RT,FE}[JTYPE_SKU, FREE_ITEM_ELIGIBLE]");
		cgb.temp("{SE}[JTYPE_SKU, FREE_ITEM_ELIGIBLE]");

	}





}
