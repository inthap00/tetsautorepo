package com.im.b2b.api.control;
/** @author Sambodhan D. (Designer/Architect) Developer: Sambodhan D., Sumeet U, Prashant N.*/
import java.util.ArrayList;
import java.util.Arrays;

public class CountryGroups {

	public static final String countryListArray[]= {"NZ","BE","CL","SG","NL","CA","SE","AU","IT","FR","UK","MY","ES","ID","US","MI","HK","BR","CH","CO","DE","MX","HU","PE","AT","IN","PT"};
	public static ArrayList<String> countryList = null;
	static {
		countryList=new ArrayList<String>(Arrays.asList(countryListArray));

	}
	String Name="";
	CountryGroups(String pName) {
		Name=pName.trim();
	}
	boolean MVC_ROLLEDOUT=false,IMONLINE=false, SAP=false, IMPULSE =false,PAYMENT_TYPE_BASKETDETAILS=false,
			SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED=false,CREDITCARD_ELIGIBLE=false, QUOTES_APPLICABLE=false, DIRECT_SHIP_SKU =false,SOFTWARE_LICENSE_PRODUCTS=false,
			JTYPE_SKU=false,END_USER=false, MULTILINGUAL=false, SBO_ELIGIBLE =false,FREE_ITEM_ELIGIBLE=false,
			SPECIAL_BID_ELIGIBLE=false,BUNDLE_ELIGIBLE=false, CLICK_2_LICENSE=false, SELECT_DIFFERENT_BILL_TO =false,KENTICO_CMS_ROLLOUT_STATUS=false,
			KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES=false,CH_DC=false, SG_DC=false, RRP=false, InvoiceGroup=false,
			IGNORECALCULATIONCHECK=false,CARRIERELIGIBLECHECK=false,DISABLE_SHIPPING_ADDRESS=false, WITHOUT_ENGLISH_LANG_DROPDOWN=false, FF_DC=false;


	@Override
	public String toString() {
		return "CountryGroups [Name=" + Name + ", MVC_ROLLEDOUT=" + MVC_ROLLEDOUT + ", IMONLINE=" + IMONLINE + ", SAP="
				+ SAP + ", IMPULSE=" + IMPULSE + ", PAYMENT_TYPE_BASKETDETAILS=" + PAYMENT_TYPE_BASKETDETAILS
				+ ", SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED=" + SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED
				+ ", CREDITCARD_ELIGIBLE=" + CREDITCARD_ELIGIBLE + ", QUOTES_APPLICABLE=" + QUOTES_APPLICABLE
				+ ", DIRECT_SHIP_SKU=" + DIRECT_SHIP_SKU + ", SOFTWARE_LICENSE_PRODUCTS=" + SOFTWARE_LICENSE_PRODUCTS
				+ ", JTYPE_SKU=" + JTYPE_SKU + ", END_USER=" + END_USER + ", MULTILINGUAL=" + MULTILINGUAL
				+ ", SBO_ELIGIBLE=" + SBO_ELIGIBLE + ", FREE_ITEM_ELIGIBLE=" + FREE_ITEM_ELIGIBLE
				+ ", SPECIAL_BID_ELIGIBLE=" + SPECIAL_BID_ELIGIBLE + ", BUNDLE_ELIGIBLE=" + BUNDLE_ELIGIBLE
				+ ", CLICK_2_LICENSE=" + CLICK_2_LICENSE + ", SELECT_DIFFERENT_BILL_TO=" + SELECT_DIFFERENT_BILL_TO
				+ ", KENTICO_CMS_ROLLOUT_STATUS=" + KENTICO_CMS_ROLLOUT_STATUS
				+ ", KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES=" + KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES + ", CH_DC="
				+ CH_DC + ", SG_DC=" + SG_DC + ", RRP=" + RRP + ", InvoiceGroup=" + InvoiceGroup + ",CARRIERELIGIBLECHECK=" 
				+ CARRIERELIGIBLECHECK + ", IGNORECALCULATIONCHECK="+ IGNORECALCULATIONCHECK+",DISABLE_SHIPPING_ADDRESS="
				+ DISABLE_SHIPPING_ADDRESS+"], WITHOUT_ENGLISH_LANG_DROPDOWN=" + WITHOUT_ENGLISH_LANG_DROPDOWN + ", FF_DC="+FF_DC+"]";
	}

	public boolean isWITHOUT_ENGLISH_LANG_DROPDOWN() {
		return WITHOUT_ENGLISH_LANG_DROPDOWN;
	}

	public void setWITHOUT_ENGLISH_LANG_DROPDOWN(boolean wITHOUT_ENGLISH_LANG_DROPDOWN) {
		WITHOUT_ENGLISH_LANG_DROPDOWN = wITHOUT_ENGLISH_LANG_DROPDOWN;
	}

	public boolean isInvoiceGroup() {
		return InvoiceGroup;
	}
	public void setInvoiceGroup(boolean invoiceGroup) {
		InvoiceGroup = invoiceGroup;
	}
	public boolean isRRP() {
		return RRP;
	}
	public void setRRP(boolean rRP) {
		RRP = rRP;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name.trim();
	}
	public boolean isMVC_ROLLEDOUT() {
		return MVC_ROLLEDOUT;
	}
	public void setMVC_ROLLEDOUT(boolean mVC_ROLLEDOUT) {
		MVC_ROLLEDOUT = mVC_ROLLEDOUT;
	}
	public boolean isIMONLINE() {
		return IMONLINE;
	}
	public void setIMONLINE(boolean iMONLINE) {
		IMONLINE = iMONLINE;
	}
	public boolean isSAP() {
		return SAP;
	}
	public void setSAP(boolean sAP) {
		SAP = sAP;
	}
	public boolean isIMPULSE() {
		return IMPULSE;
	}
	public void setIMPULSE(boolean iMPULSE) {
		IMPULSE = iMPULSE;
	}
	public boolean isPAYMENT_TYPE_BASKETDETAILS() {
		return PAYMENT_TYPE_BASKETDETAILS;
	}
	public void setPAYMENT_TYPE_BASKETDETAILS(boolean pAYMENT_TYPE_BASKETDETAILS) {
		PAYMENT_TYPE_BASKETDETAILS = pAYMENT_TYPE_BASKETDETAILS;
	}
	public boolean isSEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED() {
		return SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED;
	}
	public void setSEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED(boolean sEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED) {
		SEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED = sEARCH_RESULT_QUANTITY_MOUSE_HOVER_DISABLED;
	}
	public boolean isCREDITCARD_ELIGIBLE() {
		return CREDITCARD_ELIGIBLE;
	}
	public void setCREDITCARD_ELIGIBLE(boolean cREDITCARD_ELIGIBLE) {
		CREDITCARD_ELIGIBLE = cREDITCARD_ELIGIBLE;
	}
	public boolean isQUOTES_APPLICABLE() {
		return QUOTES_APPLICABLE;
	}
	public void setQUOTES_APPLICABLE(boolean qUOTES_APPLICABLE) {
		QUOTES_APPLICABLE = qUOTES_APPLICABLE;
	}
	public boolean isDIRECT_SHIP_SKU() {
		return DIRECT_SHIP_SKU;
	}
	public void setDIRECT_SHIP_SKU(boolean dIRECT_SHIP_SKU) {
		DIRECT_SHIP_SKU = dIRECT_SHIP_SKU;
	}
	public boolean isSOFTWARE_LICENSE_PRODUCTS() {
		return SOFTWARE_LICENSE_PRODUCTS;
	}
	public void setSOFTWARE_LICENSE_PRODUCTS(boolean sOFTWARE_LICENSE_PRODUCTS) {
		SOFTWARE_LICENSE_PRODUCTS = sOFTWARE_LICENSE_PRODUCTS;
	}
	public boolean isJTYPE_SKU() {
		return JTYPE_SKU;
	}
	public void setJTYPE_SKU(boolean jTYPE_SKU) {
		JTYPE_SKU = jTYPE_SKU;
	}
	public boolean isEND_USER() {
		return END_USER;
	}
	public void setEND_USER(boolean eND_USER) {
		END_USER = eND_USER;
	}
	public boolean isMULTILINGUAL() {
		return MULTILINGUAL;
	}
	public void setMULTILINGUAL(boolean mULTILINGUAL) {
		MULTILINGUAL = mULTILINGUAL;
	}
	public boolean isSBO_ELIGIBLE() {
		return SBO_ELIGIBLE;
	}
	public void setSBO_ELIGIBLE(boolean sBO_ELIGIBLE) {
		SBO_ELIGIBLE = sBO_ELIGIBLE;
	}
	public boolean isFREE_ITEM_ELIGIBLE() {
		return FREE_ITEM_ELIGIBLE;
	}
	public void setFREE_ITEM_ELIGIBLE(boolean fREE_ITEM_ELIGIBLE) {
		FREE_ITEM_ELIGIBLE = fREE_ITEM_ELIGIBLE;
	}
	public boolean isSPECIAL_BID_ELIGIBLE() {
		return SPECIAL_BID_ELIGIBLE;
	}
	public void setSPECIAL_BID_ELIGIBLE(boolean sPECIAL_BID_ELIGIBLE) {
		SPECIAL_BID_ELIGIBLE = sPECIAL_BID_ELIGIBLE;
	}
	public boolean isBUNDLE_ELIGIBLE() {
		return BUNDLE_ELIGIBLE;
	}
	public void setBUNDLE_ELIGIBLE(boolean bUNDLE_ELIGIBLE) {
		BUNDLE_ELIGIBLE = bUNDLE_ELIGIBLE;
	}
	public boolean isCLICK_2_LICENSE() {
		return CLICK_2_LICENSE;
	}
	public void setCLICK_2_LICENSE(boolean cLICK_2_LICENSE) {
		CLICK_2_LICENSE = cLICK_2_LICENSE;
	}
	public boolean isSELECT_DIFFERENT_BILL_TO() {
		return SELECT_DIFFERENT_BILL_TO;
	}
	public void setSELECT_DIFFERENT_BILL_TO(boolean sELECT_DIFFERENT_BILL_TO) {
		SELECT_DIFFERENT_BILL_TO = sELECT_DIFFERENT_BILL_TO;
	}
	public boolean isKENTICO_CMS_ROLLOUT_STATUS() {
		return KENTICO_CMS_ROLLOUT_STATUS;
	}
	public void setKENTICO_CMS_ROLLOUT_STATUS(boolean kENTICO_CMS_ROLLOUT_STATUS) {
		KENTICO_CMS_ROLLOUT_STATUS = kENTICO_CMS_ROLLOUT_STATUS;
	}
	public boolean isKENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES() {
		return KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES;
	}
	public void setKENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES(boolean kENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES) {
		KENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES = kENTICO_MULTILINGUAL_ROLLEDOUT_COUNTRIES;
	}
	public boolean isCH_DC() {
		return CH_DC;
	}
	public void setCH_DC(boolean cH_DC) {
		CH_DC = cH_DC;
	}
	public boolean isSG_DC() {
		return SG_DC;
	}
	public void setSG_DC(boolean sG_DC) {
		SG_DC = sG_DC;
	}
	public void setIGNORECALCULATIONCHECK(boolean iGNORE_CALCULATION_CHECK) {
		IGNORECALCULATIONCHECK = iGNORE_CALCULATION_CHECK;
	}
	public boolean isIGNORECALCULATIONCHECK() {
		return IGNORECALCULATIONCHECK;
	} 

	public void setCARRIERELIGIBLECHECK(boolean CARRIER_ELIGIBLE) {
		CARRIERELIGIBLECHECK = CARRIER_ELIGIBLE;
	}

	public boolean isCARRIER_ELIGIBLE() {
		return CARRIERELIGIBLECHECK;
	}
	//TODO 
	public void setDISABLE_SHIPPING_ADDRESS(boolean DISABLE_SHIPPING_ADDR) {
		DISABLE_SHIPPING_ADDRESS = DISABLE_SHIPPING_ADDR;
	}

	public boolean isDISABLESHIPPINGADDRESS() {
		return DISABLE_SHIPPING_ADDRESS;
	}		

	public boolean isFF_DC() {
		return FF_DC;
	}

	public void setFF_DC(boolean fF_DC) {
		FF_DC = fF_DC;
	}

	public static String getCountryName(String sCountryCode2Letters) {
		switch(sCountryCode2Letters) {
		case "AT": return "Austria"; 
		case "AU": return "Australia"; 
		case "CA": return "Canada"; 
		case "CH": return "Switzerland";  
		case "FR": return "France"; 
		case "ES": return "Spain"; 
		case "CL": return "Chile"; 
		case "IT": return "Italy"; 
		case "BE": return "Belgium"; 
		case "ID": return "Indonesia"; 
		case "SG": return "Singapore"; 
		case "NZ": return "New Zealand"; 
		case "NL": return "Netherland"; 
		case "MY": return "Malaysia"; 
		case "CO": return "Colombia"; 
		case "SE": return "Sweden"; 
		case "US": return "United States"; 
		case "PE": return "Peru";
		case "UK": return "United Kingdom"; 
		case "DE": return "Germany"; 
		case "MX": return "Mexico"; 
		case "HK": return "Hong Kong"; 
		case "HU": return "Hungary"; 
		case "MI": return "Miami"; 
		case "BR": return "Brazil";
		case "IN": return "INDIA";
		case "PT": return "PORTUGAL";
		default : 
			return sCountryCode2Letters;
			//sumeet commented below line for API framework
			//ExtentTestManager.log(Status.FAIL, "CountryGroup.getCountryName received invalid country name:["+sCountryCode2Letters+"]", "Invalid:"+sCountryCode2Letters);return "null";

		}

	}
}
